#!/bin/bash

if [ $# -ne 6 ]; then
	echo "Usage: ${0} [vidfn.mp4] [x] [y] [width] [top] [0|1:add frame]"
	exit 0
fi

vidfn=${1}
x=${2}
y=${3}
w=${4}
h=${5}
a=${6}

osx_top=-22
osx_left=0
osx_width=0
osx_height=22

if [ ${a} -eq 1 ]; then
	x=$(( x + osx_left ))
	y=$(( y + osx_top ))
	w=$(( w + osx_width ))
	h=$(( h + osx_height ))
fi

# make sure w and h are even for h264
w=$(( w + (w % 2) ))
h=$(( h + (h % 2) ))

echo "Capturing: ${x} ${y} ${w} ${h}"

rm -f tmp.mp4
rm -f ${vidfn}

# records great!  do not touch
/Applications/VLC.app/Contents/MacOS/VLC screen:// --quiet --sout-x264-quiet \
--screen-left ${x} --screen-top ${y} --screen-width ${w} --screen-height ${h} --screen-fps 30 --screen-caching 100 \
--sout '#transcode{venc=x264{bframes=0,nocabac,ref=1,nf,level=13,crf=30,partitions=none},vcodec=mp4v,vb=3072,fps=30}:standard{mux=mp4,dst=tmp.mp4,access=file}'

mv tmp.mp4 ${vidfn}

# from http://forrst.com/posts/Command_line_screen_capture_using_VLC-COl
# could not get to work
#--sout '#transcode{venc=x264{bframes=0,nocabac,ref=1,nf,level=13,crf=24,partitions=none},vcodec=h264{fps=30,vb=3000,width=1272,height=716,acodec=none}:duplicate{mux=mp4,dst=test.mp4,access=file}' \

# compression artifacts
#--sout '#transcode{vcodec=mp4v,vb=3072,fps=30}:standard{mux=mp4,dst=test.mp4,access=file}'

# choppy
#--sout '#transcode{vcodec=h264,vb=3072,fps=30}:standard{mux=mp4,dst=test.mp4,access=file}'

