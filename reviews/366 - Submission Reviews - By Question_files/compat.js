/***************************************************************************
 file:         compat.js

 author:       Kevin Jacobs (jacobs@theopalgroup.com)

 created:      December 29, 2000

 purpose:      Implements browser detection and compatibility code

 compatiblity: Should work with any Javascript >1.1 implementation Tested
               against Microsoft Internet Explorer 5.0 and 5.5, Netscape 6.0
               and Mozilla (Seamonkey).

 requires:     nothing

 History:
          1.0  2000-12-29  [kbj]  Initial version

 Copyright (c) 2000 The OPAL Group.  All rights reserved.

 ***************************************************************************/


// Global variables -- ugly, but necessary for older browsers
NS   = false
NS6  = false
IE   = false
IE5  = false
IE55 = false

switch(navigator.appName)
{
  case 'Netscape':
    NS  = true
    NS6 = (navigator.product == 'Gecko')? true : false
    break
  case 'Microsoft Internet Explorer':
    IE   = true
    IE4  = (navigator.appVersion.indexOf('MSIE 4.')  >= 0)? true : false
    IE5  = (navigator.appVersion.indexOf('MSIE 5.')  >= 0)? true : false
    IE55 = (navigator.appVersion.indexOf('MSIE 5.5') >= 0)? true : false
    break
}

if(IE && !IE55)
{
  var x = new Object()
  undefined = x.y
}

function isDef(x)
{
  return (x==undefined)? false : true
}

function isUndef(x)
{
  return (x!=undefined)? false : true
}

function merge(obj, newobj)
{
  if( obj == null )
    return obj

  for (var i in obj)
    newobj[i] = obj[i]

  return newobj
}

function copy(obj)
{
  return merge(obj, new Object())
}

function merge_list(objlist)
{
  var newobj = new Object()
  for(var i = 0; i < objlist.length; ++i)
    merge(objlist[i], newobj)
  return newobj
}

if( isUndef(Array.prototype.push) )
  Array.prototype.push = function(item1)
  {
    var l = arguments.length
    var n = this.length

    for(var i = 0; i < l; ++i,++n)
      this[n] = arguments[i]

    return n
  }

if( isUndef(Array.prototype.splice) )
  Array.prototype.splice = function(start, deleteCount)
  {
    var A = new Array()
    var len = this.length

    if(start < 0)
      start = Math.max(0,start+length)
    else
      start = Math.min(start, len)

    deleteCount = Math.min( Math.max(deleteCount,0), len - start )

    for(var k = 0; k < deleteCount; ++k)
      A[k] = this[start + k]

    var alen = Math.max(arguments.length,2)
    var shift = alen - 2 - deleteCount
    var shiftStart = start + deleteCount

    if(shift > 0)
    {
      for(var k = len - 1; k >= shiftStart; --k)
        this[ k + shift ] = this[ k ]
    }
    else if(shift < 0)
    {
      for(var k = shiftStart; k < len; ++k)
        this[ k + shift ] = this[ k ]
    }

    for(var l = 2; l < alen; ++l,++start)
      this[start] = arguments[l]

    this.length = len + shift

    return A
  }

function inherit(parent, child)
{
  for (var p in parent.prototype)
  {
    child.prototype[p] = parent.prototype[p]
    child.prototype['base_'+p] = parent.prototype[p]
  }
  child.prototype.parent = parent.prototype
}

function dump_props(obj, obj_name, recurse)
{
  if( obj == null || obj_name.length > 20 )
    return ""

  var result = ""
  for (var i in obj)
  {
    result += obj_name + "." + i + " = " + obj[i] + "<BR>"
    if(recurse && typeof(obj[i]) == 'object' && obj[i] != null)
      result += dump_props(obj, obj_name + '.' + i)
  }
  return result
}
