using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public partial class SnapshotScene
	{
		/*public IndexedViewableAlpha GetViewable() { return GetViewable( null, true ); }
		public IndexedViewableAlpha GetViewable( bool applymodifiers ) { return GetViewable( null, applymodifiers ); }
		public IndexedViewableAlpha GetViewable( IndexedViewableAlpha matchVerts ) { return GetViewable( matchVerts, true ); }
		public IndexedViewableAlpha GetViewable( IndexedViewableAlpha matchVerts, bool applymodifiers )
		{
			IndexedViewableAlpha viewable = null;
			
			List<Vec3f> verts = new List<Vec3f>();
			List<int> vertuids = new List<int>();
			List<bool> selected = new List<bool>();
			
			Vec4f[][] colors = new Vec4f[models.Length * 4][];
			int[][] groups = new int[models.Length * 4][];
			
			float[] pointsizes = new float[models.Length * 4];
			float[] linewidths = new float[models.Length * 4];
			int[] groupsizes = new int[models.Length * 4];
			
			int nverts = 0;
			
			IndexedViewableAlpha[] viewables = new IndexedViewableAlpha[ models.Length ];
			for( int iobj = 0; iobj < models.Length; iobj++ )
				viewables[iobj] = models[iobj].GetViewable( matchVerts, applymodifiers );
			
			for( int iobj = 0; iobj < models.Length; iobj++ )
			{
				viewable = viewables[iobj];
				
				verts.AddRange( viewable.Verts );
				vertuids.AddRange( viewable.VertUIDs );
				selected.AddRange( viewable.Selected );
				
				for( int i = 0; i < 4; i++ )
				{
					groups[iobj * 4 + i] = viewable.Indices[i].Select( (int ind) => ind + nverts ).ToArray();
					
					pointsizes[iobj * 4 + i] = viewable.PointSizes[i];
					linewidths[iobj * 4 + i] = viewable.LineWidths[i];
					groupsizes[iobj * 4 + i] = viewable.GroupSizes[i];
				}
				
				nverts += viewable.nVerts;
			}
			
			int ivertstart = 0;
			for( int iobj = 0; iobj < models.Length; iobj++ )
			{
				viewable = viewables[iobj];
				
				for( int igrp = 0; igrp < 4; igrp++ )
				{
					int igroup = iobj * 4 + igrp;
					colors[igroup] = new Vec4f[nverts];
					int i = 0;
					for( ; i < ivertstart; i++ ) colors[igroup][i] = new Vec4f( 0.0f, 0.0f, 0.0f, 0.0f );
					for( int ic = 0; ic < viewable.nVerts; ic++, i++ ) colors[igroup][i] = viewable.Colors[igrp][ic];
					for( ; i < nverts; i++ ) colors[igroup][i] = new Vec4f( 0.0f, 0.0f, 0.0f, 0.0f );
				}
				ivertstart += viewable.nVerts;
			}
			
			return new IndexedViewableAlpha( verts.ToArray(), colors, groups, pointsizes, linewidths, groupsizes, vertuids.ToArray(), selected.ToArray() );
		}*/
		
		public IndexedViewableAlpha GetViewables() { return GetViewables( null, true ); }
		public IndexedViewableAlpha GetViewables( bool applymodifiers ) { return GetViewables( null, applymodifiers ); }
		public IndexedViewableAlpha GetViewables( Vec3f[] match ) { return GetViewables( match, true ); }
		public IndexedViewableAlpha GetViewables( Vec3f[] match, bool applymodifiers )
		{
			IndexedViewableAlpha viewable = null;
			foreach( SnapshotModel model in Models )
			{
				IndexedViewableAlpha viewadd = null;
				//Timer.PrintTimeToExecute( "getviewable", delegate {
					viewadd = model.GetViewable( match, applymodifiers );
				//} );
				//Timer.PrintTimeToExecute( "adding", delegate {
					viewable = IndexedViewableAlpha.CombineFat( viewable, viewadd );
					//viewable += viewadd;
				//} );
			}
			return viewable;
		}
		
		public IndexedViewableAlpha GetViewablesAttached( Vec3f[] match, bool applymods )
		{
			IndexedViewableAlpha viewable = null;
			foreach( SnapshotModel model in Models )
			{
				viewable = IndexedViewableAlpha.Attach( viewable, model.GetViewable( match, applymods ) );
			}
			return viewable;
		}
		
		public IndexedViewableAlpha GetSelection( Vec3f[] match, bool applymodifiers )
		{
			SnapshotModel modeledit = GetEditModel();
			
			if( modeledit != null ) return modeledit.GetSelection( match, applymodifiers );
			
			IndexedViewableAlpha viewable = null;
			foreach( SnapshotModel model in Models )
			{
				if( !model.objselected || !model.objvisible ) continue;
				IndexedViewableAlpha nview = model.GetSelection( match, applymodifiers, true );
				viewable = IndexedViewableAlpha.Attach( viewable, nview );
			}
			return viewable;
			
			/*IndexedViewableAlpha viewable = null;
			foreach( SnapshotModel model in Models )
			{
				IndexedViewableAlpha viewadd = model.GetSelection( match, applymodifiers );
				viewable = IndexedViewableAlpha.CombineFat( viewable, viewadd );
			}
			return viewable;*/
		}
		
		public static IndexedViewableAlpha MeshDiff( SnapshotScene snapshot0, SnapshotScene snapshot1, Vec3f[] verts, bool applymods )
		{
			SnapshotModel[] models0 = snapshot0.Models;
			SnapshotModel[] models1 = snapshot1.Models;
			
			IndexedViewableAlpha viewable = null;
			
			int i;
			int c = models1.Length;
			foreach( SnapshotModel model0 in models0 )
			{
				int uid = model0.objuid;
				for( i = 0; i < c; i++ ) if( models1[i].objuid == uid ) break;
				if( i == c ) { viewable += model0.GetViewable( verts, applymods ); continue; }
				SnapshotModel model1 = models1[i];
				
				if( model0.GetEditCount() == model1.GetEditCount() ) continue;
				IndexedViewableAlpha diff = SnapshotModel.MeshDiff( model0, model1, verts, applymods );
				viewable += diff;
				
				//IndexedViewableAlpha view0 = model0.GetViewable( verts, applymods );
				//IndexedViewableAlpha view1 = model1.GetViewable( verts, applymods );
				//viewable += view0 - view1;
			}
			
			return viewable;
		}
		
		public static IndexedViewableAlpha MeshDiffAttach( SnapshotScene snapshot0, SnapshotScene snapshot1, Vec3f[] verts, bool applymods )
		{
			SnapshotModel[] models0 = snapshot0.Models;
			SnapshotModel[] models1 = snapshot1.Models;
			
			IndexedViewableAlpha viewable = null;
			
			int i;
			int c = models1.Length;
			foreach( SnapshotModel model0 in models0 )
			{
				int uid = model0.objuid;
				for( i = 0; i < c; i++ ) if( models1[i].objuid == uid ) break;
				if( i == c ) {
					//viewable += model0.GetViewable( verts, applymods );
					viewable = IndexedViewableAlpha.Attach( viewable, model0.GetViewable( verts, applymods ) );
					continue;
				}
				SnapshotModel model1 = models1[i];
				
				if( model0.GetEditCount() == model1.GetEditCount() ) continue;
				IndexedViewableAlpha diff = SnapshotModel.MeshDiff( model0, model1, verts, applymods );
				//viewable += diff;
				viewable = IndexedViewableAlpha.Attach( viewable, diff );
			}
			
			return viewable;
		}
		
		public static IndexedViewableAlpha MeshIntersect( SnapshotScene snapshot0, SnapshotScene snapshot1, Vec3f[] verts, bool applymods )
		{
			SnapshotModel[] models0 = snapshot0.Models;
			SnapshotModel[] models1 = snapshot1.Models;
			
			IndexedViewableAlpha viewable = null;
			
			int i;
			int c = models1.Length;
			foreach( SnapshotModel model0 in models0 )
			{
				int uid = model0.objuid;
				for( i = 0; i < c; i++ ) if( models1[i].objuid == uid ) break;
				if( i == c ) continue;
				SnapshotModel model1 = models1[i];
				
				if( model0.GetEditCount() == model1.GetEditCount() ) {
					viewable += model0.GetViewable( verts, applymods );
				} else {
					viewable += SnapshotModel.MeshIntersect( model0, model1, verts, applymods );
				}
			}
			
			return viewable;
		}
		public static IndexedViewableAlpha MeshIntersectAttach( SnapshotScene snapshot0, SnapshotScene snapshot1, Vec3f[] verts, bool applymods )
		{
			SnapshotModel[] models0 = snapshot0.Models;
			SnapshotModel[] models1 = snapshot1.Models;
			
			IndexedViewableAlpha viewable = null;
			
			int i;
			int c = models1.Length;
			foreach( SnapshotModel model0 in models0 )
			{
				int uid = model0.objuid;
				for( i = 0; i < c; i++ ) if( models1[i].objuid == uid ) break;
				if( i == c ) continue;
				SnapshotModel model1 = models1[i];
				
				if( model0.GetEditCount() == model1.GetEditCount() ) {
					viewable = IndexedViewableAlpha.Attach( viewable, model0.GetViewable( verts, applymods ) );
				} else {
					viewable = IndexedViewableAlpha.Attach( viewable, SnapshotModel.MeshIntersect( model0, model1, verts, applymods ) );
				}
			}
			
			return viewable;
		}
	}
}

