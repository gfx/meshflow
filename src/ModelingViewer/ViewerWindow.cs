using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
using Common.Libs.VMath;
using Common.Libs.Image;
using Common.Libs.MiscFunctions;


namespace ModelingViewer
{
	public class ViewerWindow : Form
	{
		public static ViewerWindow viewwin = null;
		
		ModelViewerControl viewer;
		ModelViewerControl viewerwindow;
		ModelingHistory hist;
		//ModelTreeRoot root;
		//int curicind;
		//ModelTree mtree;
		
		public static int PlayDelay = 500;
		public static int PlayDelayFast = 100;
		public static float EaseSpeed = 0.03f;
		public static float EaseSpeedFast = 0.1f;
		protected int deltai = 0;
		protected Thread currentmover;
		
		//protected System.Timers.Timer playtimer = new System.Timers.Timer( 100 );
		//protected System.Timers.Timer deltatimer = new System.Timers.Timer( 100 );
		
		protected enum PlayStates { Stop, Play, PlayFast, PlayRev, PlayFastRev }
		protected PlayStates PlayState;
		
		// play
		protected ToolStripButton tsbPlayFastRevSteps;
		protected ToolStripButton tsbPlayRevSteps;
		protected ToolStripButton tsbPlaySteps;
		protected ToolStripButton tsbPlayFastSteps;
		protected ToolStripButton tsbPauseSteps;
		
		// jump
		protected ToolStripButton tsbFirstStep;
		protected ToolStripButton tsbPrevStep;
		protected ToolStripButton tsbNextStep;
		protected ToolStripButton tsbLastStep;
		
		// filters, clusters
		protected ToolStripDropDownButton tsbUnfilter;
		protected ToolStripDropDownButton tsbFilter;
		protected ToolStripDropDownButton tsbCluster;
		protected ToolStripDropDownButton tsbLoD;
		protected ToolStripDropDownButton tsbTag;
		
		protected ToolStripButton tsbFilterCommands_ViewGUI;
		protected ToolStripButton tsbFilter_NonHighlighted;
		protected ToolStripButton tsbFilter_Undos;
		
		protected ToolStripButton tsbCluster_RepeatedCommands;
		protected ToolStripButton tsbClusterCommands_SelectTranslate;
		
		protected ToolStripButton tsbShowVTags;
		protected ToolStripButton tsbShowTTags;
		
		// information
		
		protected ToolStripDropDownButton tsbInfo;
		protected ToolStripDropDown tsddInfo;
		
		protected ToolStripButton tsbInfo_tuples;
		
		// other
		
		protected bool ignoreinfochange = false;
		protected Cluster currentcluster = null;
		
		protected TableLayoutPanel tlp;
		protected TableLayoutPanel tlpViewerCluster;
		protected TableLayoutPanel tlpClusterInfo;
		protected TextBox txtName;
		protected ComboBox cboComposition;
		protected TextBox txtAnnotation;
		
		protected ToolStripButton tsbDiff;
		
		protected SplitContainer splitViewerTimeline;
		protected SplitContainer splitViewerClusterInfo;
		
		//protected RowStyle rsTrackBar;
		
		//protected ListView lstSteps;
		//protected ScrubBar sbIndex;
		protected Timeline timeline;
		
		public Filtering FilterHighlightedVerts = new FilteringVertexHighlightedTag( "Filter Clusters that alter highlighted verts", false );
		public Filtering UnFilterHighlightedVerts = new UnFilteringVertexHighlightedTag( "Filter Clusters that do not alter highlighted verts", false );
		
		public Filtering FilterNameView = new FilteringNameStartsWith( "Filter View Operations", "view", false );
		public Filtering FilterNameSelect = new FilteringNameStartsWith( "Filter Select Operations", "select", false );
		public Filtering FilterNameVisibility = new FilteringNameStartsWith( "Filter Visibility Operations", new string[] { "visible", "modifier" }, false );
		//public Filtering FilterNameViewVisSel = new FilteringNameStartsWith( "Filter View, Select, and Visibility Operations", new string[] { "view", "select", "visible", "modifier", "viewvis" }, false );
		public Filtering FilterNameTransform = new FilteringNameStartsWith( "Filter Transform Operations", "transform", false );
		public Filtering FilterUndos = new FilteringName( "Filter Undo Operations", "undo.undo", false );
		
		//protected Filtering FilterVTagTest = new UnFilteringVertexTag( "test", false );
		
		public ViewerWindow( ModelingHistory hist )
		{
			this.hist = hist;
			viewwin = this;
			
			hist.Filters.AddFilters( FilterHighlightedVerts, UnFilterHighlightedVerts );
			hist.Filters.AddFilters( FilterNameView, FilterNameVisibility, FilterNameSelect );
			hist.Filters.AddFilters( FilterNameTransform, FilterUndos );
			
			hist.HighlightedChanged += delegate {
				viewer.RefreshControl();
				viewerwindow.RefreshControl();
				if( FilterHighlightedVerts.Enabled ) FilterHighlightedVerts.FireReevalutadeHandler();
				if( UnFilterHighlightedVerts.Enabled ) UnFilterHighlightedVerts.FireReevalutadeHandler();
			};
			
			hist.TagsChanged += delegate {
				//if( FilterVTagTest.Enabled ) FilterVTagTest.FireReevalutadeHandler();
			};
			hist.Layers.Reevaluated += delegate {
				FillLoDDropDown();
			};
			hist.Filters.Reevaluated += delegate { ViewWindowChange(); ClusterChange(); };
			hist.CurrentLevel.PropertyChanged += delegate {
				SelectLoDInDropDown();
				ViewWindowChange();
				ClusterChange();
			};
			
			Initialize();
			UpdatePlayState( PlayStates.Stop );
			
			currentmover = new Thread( (ThreadStart) MoveCurrent );
			currentmover.Start();
			currentmover.Priority = ThreadPriority.Normal;
			
			//playtimer.Elapsed += delegate {
			//	if( viewer.IsRendering ) return;
			//	if( !viewer.CameraReady ) return;
			//	if( !viewer.HasRendered ) return;
			//	
			//	if( PlayState == ViewerWindow.PlayStates.Play || PlayState == ViewerWindow.PlayStates.PlayFast ) Next();
			//	else Prev();
			//};
			//playtimer.Enabled = false;
			
			//deltatimer.Elapsed += delegate {
			//	if( viewer.IsRendering ) return; //|| sbIndex.IsRendering ) return;
			//	
			//	if( deltai < 0 ) Prev();
			//	if( deltai > 0 ) Next();
			//	deltai = 0;
			//	deltatimer.Enabled = false;
			//};
			
			
			
			//sbIndex.Index0Changed += delegate( bool click ) { ClusterChange(); };
			//sbIndex.LevelChanged += delegate { ClusterChange(); };
			
			timeline.Index0CurrentChanged += delegate(bool click) { ClusterChange(); };
			timeline.ViewWindowProperties.PropertyChanged += delegate { ViewWindowChange(); };
			ViewWindowChange();
			
			this.KeyPreview = true;
			
			viewer.RenderOptVTags.PropertyChanged += delegate { tsbShowVTags.Checked = viewer.RenderOptVTags; };
			viewer.RenderOptTTags.PropertyChanged += delegate { tsbShowTTags.Checked = viewer.RenderOptTTags; };
			
			First();
		}
		
		private void MoveCurrent()
		{
			while( !ModelingHistory.ENDING )
			{
				while( PlayState == ViewerWindow.PlayStates.Stop && deltai == 0 && !ModelingHistory.ENDING ) { Thread.Sleep( 10 ); }
				if( ModelingHistory.ENDING ) break;
				
				if( viewer.IsRendering ) continue;
				if( !viewer.CameraReady ) continue;
				if( !viewer.HasRendered ) continue;
				
				if( PlayState == ViewerWindow.PlayStates.Play || PlayState == ViewerWindow.PlayStates.PlayFast ) deltai = 1;
				if( PlayState == ViewerWindow.PlayStates.PlayRev || PlayState == ViewerWindow.PlayStates.PlayFastRev ) deltai = -1;
				
				if( deltai < 0 ) Prev();
				else Next();
				
				deltai = 0;
				
				while( ( !viewer.CameraReady || !viewer.HasRendered || viewer.IsRendering ) && !ModelingHistory.ENDING ) Thread.Sleep( 10 );
				
				if( PlayState == ViewerWindow.PlayStates.Play || PlayState == ViewerWindow.PlayStates.PlayRev ) if( PlayDelay > 0 ) Thread.Sleep( PlayDelay );
				else if( PlayState == ViewerWindow.PlayStates.PlayFast || PlayState == ViewerWindow.PlayStates.PlayFastRev ) if( PlayDelayFast > 0 ) Thread.Sleep( PlayDelayFast );
				else Thread.Sleep( 100 );
			}
		}
		
		private void SelectLoDInDropDown()
		{
			int l = hist.CurrentClusteringLayerLevel_Get(); //hist.CurrentLevel.Val.Level;
			foreach( ToolStripItem tsItem in tsbLoD.DropDownItems )
			{
				((ToolStripButton)tsItem).Checked = ( Int32.Parse( tsItem.Text.SplitOnce(':')[0] ) == l );
			}
		}
		
		protected override void OnClosing( CancelEventArgs e )
		{
			base.OnClosing( e );
			ModelingHistory.ENDING = true;
		}
		
		protected void ViewWindowChange()
		{
			//Cluster cluster = new Cluster( timeline.GetIndex0ViewWindowStart(), timeline.GetIndex0ViewWindowEnd(), "Intervals", Composition.GetPreset( CompositionPresets.Intervals ) );
			
			new Thread( (ThreadStart) delegate {
				Cluster cluster = timeline.GetViewWindowCluster();
				viewerwindow.SetCluster( timeline.GetCurrentLayer(), cluster );
			} ).Start();
		}
		
		protected void ClusterChange()
		{
			currentcluster = timeline.GetCurrentCluster(); // sbIndex.GetCurrentCluster();
			Composition comp = currentcluster.composition;
			
			if( this.IsHandleCreated )
			{
				this.Invoke( (MethodInvoker) delegate {
					ignoreinfochange = true;
					//txtName.Text = ( currentcluster.name == null ? "" : currentcluster.name );
					//cboComposition.SelectedItem = currentcluster.composition;
					//txtAnnotation.Text = ( currentcluster.annotation == null ? "" : currentcluster.annotation );
					txtAnnotation.Text =
						"Mode: " +
						( comp.Mode == ComparisonModes.OnlyCurrent ? "Only Current\n" : "" ) +
						( comp.Mode == ComparisonModes.BeforeAfter ? "Before After\n" : "" ) +
						( comp.Mode == ComparisonModes.BeforeCurrentAfter ? "Before, Current, After\n" : "" ) +
						( comp.SeparateViewports_Use ? "Separate Viewports\n" : "" ) +
						( comp.Intervals_Use ? "Intervals: " + comp.Intervals_Count + "\n" : "" ) +
						( "Offset: " + comp.Compare_Offset.ToStringFormatted() + "\n" ) +
						( comp.Show_After ? "After\n" : "" ) +
						( comp.Show_Diff_AfterBefore ? "After - Before\n" : "" ) +
						( comp.Show_Diff_BeforeAfter ? "Before - After\n" : "" ) +
						( comp.Show_End ? "End\n" : "" ) +
						( comp.Show_Intersect_BeforeAfter ? "Before intersect After\n" : "" ) +
						( comp.Show_Provenance ? "Provenance\n" : "" ) +
						"";
					ignoreinfochange = false;
				} );
			}
			
			while( viewer.CurrentlyRendering ) { Thread.Sleep( 10 ); }
			viewer.SetCluster( timeline.GetCurrentLayer(), currentcluster ); //sbIndex.CurrentLayer, currentcluster );
		}
		
		protected void UpdateClusterInfo()
		{
			if( ignoreinfochange || currentcluster == null ) return;
			//currentcluster.name = txtName.Text;
			//currentcluster.composition = (ClusterCompositions)cboComposition.SelectedValue;
			//currentcluster.annotation = txtAnnotation.Text;
			
			if( cboComposition.SelectedValue != null )
			{
				//currentcluster.CompositionPreset( (CompositionPresets)cboComposition.SelectedValue );
				cboComposition.Text = "Composition Presets";
			}
		}
		
		protected override bool ProcessCmdKey (ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;
			
			if( ( msg.Msg == WM_KEYDOWN ) || ( msg.Msg == WM_SYSKEYDOWN ) )
			{
				switch( keyData ) {
					
				case Keys.Left:
					deltai = -1;
					//deltatimer.Enabled = true;
					break;
					
				case Keys.Right:
					deltai = 1;
					//deltatimer.Enabled = true;
					break;
					
				case Keys.Space:
					if( PlayState == ViewerWindow.PlayStates.Stop ) UpdatePlayState( PlayStates.Play );
					else UpdatePlayState( PlayStates.Stop );
					break;
					
				case Keys.F3:
					int w,h;
					string sw = "" + viewer.Width;
					string sh = "" + viewer.Height;
					
					if( InputBox.Show( "Enter Size", "Width", ref sw ) == DialogResult.Cancel ) break;
					if( !Int32.TryParse( sw, out w ) ) break;
					if( InputBox.Show( "Enter Size", "Height", ref sh ) == DialogResult.Cancel ) break;
					if( !Int32.TryParse( sh, out h ) ) break;
					
					this.Size = new Size( Width + (w - viewer.Width), Height + (h - viewer.Height ) );
					
					break;
					
				case Keys.P:
					if( InputBox.Show( "Easing Speed Normal", "Easing Speed Normal", ref EaseSpeed ) == DialogResult.Cancel ) break;
					if( InputBox.Show( "Easing Speed Fast", "Easing Speed Fast", ref EaseSpeedFast ) == DialogResult.Cancel ) break;
					if( InputBox.Show( "MS Delay Normal", "MS Delay Normal", ref PlayDelay ) == DialogResult.Cancel ) break;
					if( InputBox.Show( "MS Delay Fast", "MS Delay Fast", ref PlayDelayFast ) == DialogResult.Cancel ) break;
					break;
					
				case Keys.F4:
					SnapshotSequence();
					break;
					
				case Keys.F5:
					timeline.GetCurrentLayer().CacheViewables( 0 );
					break;
					
				}
			}
			return base.ProcessCmdKey( ref msg, keyData );
		}
		
		private bool snapshotting = false;
		private string snapfilename = "snap";
		private void SnapshotSequence()
		{
			if( InputBox.Show( "Enter filename prefix", "Enter filename prefix", ref snapfilename ) == DialogResult.Cancel ) return;
			switch( MessageBox.Show( "Reset snapshot numbering to 0?", "Reset Numbering?", MessageBoxButtons.YesNoCancel ) ) {
			case DialogResult.Cancel: return;
			case DialogResult.Yes: viewer.ResetSnapshotCount(); break;
			}
			
			if( snapshotting ) { System.Console.WriteLine( "already going!" ); return; }
			snapshotting = true;
			
			System.Console.WriteLine( "caching..." );
			int i0start = timeline.GetIndex0ViewWindowStart();
			int i0end = timeline.GetIndex0ViewWindowEnd();
			int indstart = hist.CurrentLevel.Val.GetClusterIndex( i0start );
			int indend = hist.CurrentLevel.Val.GetClusterIndex( i0end );
			timeline.GetCurrentLayer().CacheViewables( 200, indstart, indend );
			System.Console.WriteLine( "done" );
			
			new Thread( (ThreadStart) delegate {
				bool easing = viewer.ViewEasing.Get();
				//Cluster current = timeline.GetCurrentCluster();
				viewer.ViewEasing.Set( false );
				
				timeline.CurrentToFirst();
				while( !viewer.HasRendered || viewer.IsRendering || !viewer.CameraReady ) Thread.Sleep( 10 );
				
				//int i0c = 0;
				//int i0l = -1;
				Cluster cur = null;
				Cluster last = null;
				
				System.Console.WriteLine( "capturing..." );
				//while( ( i0c = timeline.GetIndex0Current() ) != i0l )
				while( ( cur = timeline.GetCurrentCluster() ) != last )
				{
					viewer.SaveSnapshot( snapfilename );
					
					//i0l = i0c;
					last = cur;
					timeline.CurrentToNext();
					Thread.Sleep( 50 );
					while( !viewer.HasRendered || viewer.IsRendering || !viewer.CameraReady ) Thread.Sleep( 10 );
				}
				System.Console.WriteLine( "done with capture" );
				
				snapshotting = false;
				viewer.ViewEasing.Set( easing );
				
			} ).Start();
			
		}
		
		
		/*protected override bool ProcessKeyPreview( ref Message m )
		{
			switch( m.WParam.ToInt32() ) {
				
			case Keys.Space: // space
				if( PlayState == ViewerWindow.PlayStates.Stop ) PlayState = ViewerWindow.PlayStates.Play;
				else PlayState = ViewerWindow.PlayStates.Stop;
				return false;
				
			case 37: // <-
				deltai = -1; deltatimer.Enabled = true;
				return false;
				
			case 38: // /\
				break;
				
			case 39: // ->
				deltai = 1; deltatimer.Enabled = true;
				return false;
				
			case 40: // \/
				break;
				
			}
			
			return base.ProcessKeyPreview( ref m );
		}*/
		
		protected override void OnKeyPress( System.Windows.Forms.KeyPressEventArgs e )
		{
			if( !e.Handled ) base.OnKeyPress( e );
		}
		
		private void First() { timeline.CurrentToFirst(); } // sbIndex.MoveFirstIndex( true ); }
		private void Last() { timeline.CurrentToLast(); } // sbIndex.MoveLastIndex( true ); }
		private void Next() { timeline.CurrentToNext(); } // sbIndex.MoveNextIndex( true ); }
		private void Prev() { timeline.CurrentToPrev(); } // sbIndex.MovePrevIndex( true ); }
		
		private void Initialize( )
		{
			this.Text = "Modeling Viewer";
			this.Size = new Size( 1280, 720 );
			this.Icon = MiscFileIO.LoadIconResource( "model.ico" );
			
			tlp = new TableLayoutPanel() {
				Dock = DockStyle.Fill,
				Padding = new Padding( 0 ),
				Margin = new Padding( 0 ),
				AutoSize = false,
				BackColor = Color.Black,
				ForeColor = Color.Black,
				ColumnCount = 1,
				RowCount = 4,
				CellBorderStyle = TableLayoutPanelCellBorderStyle.Single,
			};
			tlp.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 100.0f ) );
			tlp.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
			tlp.RowStyles.Add( new RowStyle( SizeType.Percent, 100.0f ) );
			tlp.RowStyles.Add( new RowStyle( SizeType.Absolute, 100.0f ) );
			tlp.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
			
			tlpViewerCluster = new TableLayoutPanel() {
				Dock = DockStyle.Fill,
				Padding = new Padding( 0 ),
				Margin = new Padding( 0 ),
				AutoSize = false,
				BackColor = Color.Black,
				ColumnCount = 2,
				RowCount = 1,
			};
			tlpViewerCluster.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 100.0f ) );
			tlpViewerCluster.ColumnStyles.Add( new ColumnStyle( SizeType.AutoSize ) );
			tlpViewerCluster.RowStyles.Add( new RowStyle( SizeType.Percent, 100.0f ) );
			
			tlpClusterInfo = new TableLayoutPanel() {
				Dock = DockStyle.Fill,
				Padding = new Padding( 1, 0, 0, 0 ),
				Margin = new Padding( 0 ),
				AutoSize = false,
				BackColor = Color.Black,
				ColumnCount = 1,
				RowCount = 3,
				Width = 0 //( ModelingHistory.DEBUGMODE ? 200 : 0 ),
			};
			tlpClusterInfo.ColumnStyles.Add( new ColumnStyle( SizeType.Percent, 100.0f ) );
			tlpClusterInfo.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
			tlpClusterInfo.RowStyles.Add( new RowStyle( SizeType.AutoSize ) );
			tlpClusterInfo.RowStyles.Add( new RowStyle( SizeType.Percent, 100.0f ) );
			
			ToolStrip ts = new ToolStrip();
			ts.GripStyle = ToolStripGripStyle.Hidden;
			ts.Dock = DockStyle.Fill;
			//ts.BackColor = Color.Black;
			ts.RenderMode = ToolStripRenderMode.Professional;
			//ts.RenderMode = ToolStripRenderMode.Custom;
			
			viewer = new ModelViewerControl( hist, true ) {
				Dock = DockStyle.Fill,
				Padding = new Padding( 0 ),
				Margin = new Padding( 0 ),
			};
			viewer.SetToolStrip( ts );
			
			viewerwindow = new ModelViewerControl( hist, false ) {
				Dock = DockStyle.Fill,
				Padding = new Padding( 0 ),
				Margin = new Padding( 0 ),
			};
			viewerwindow.RenderOptGrid.Set( false );
			viewerwindow.RenderOptClusterInfo.Set( false );
			viewerwindow.RenderOptTTags.Set( false );
			viewerwindow.RenderOptRotationGizmoOpt.Set( false );
			viewerwindow.RenderOptSeparateViewports.Set( true );
			viewerwindow.RenderOptSelectedVerts.Set( false );
			viewerwindow.SmoothView( new Vec3f(), new Quatf( 0.73907f, -0.53697f, -0.23907f, -0.32906f ), 10.53924f, true );
			
			tsbFirstStep = CreateToolStrip_Image( "first.png", "First", delegate { First(); } );
			tsbPrevStep = CreateToolStrip_Image( "prev.png", "Prev", delegate { Prev(); } );
			tsbNextStep = CreateToolStrip_Image( "next.png", "Next", delegate { Next(); } );
			tsbLastStep = CreateToolStrip_Image( "last.png", "Last", delegate { Last(); } );
			
			tsbPlayFastRevSteps = CreateToolStrip_Image( "playfastrev.png", "Play Fast Reverse", delegate { UpdatePlayState( PlayStates.PlayFastRev ); } );
			tsbPlayRevSteps = CreateToolStrip_Image( "playrev.png", "", delegate { UpdatePlayState( PlayStates.PlayRev ); } );
			tsbPauseSteps = CreateToolStrip_Image( "pause.png", "", delegate { UpdatePlayState( PlayStates.Stop ); } );
			tsbPlaySteps = CreateToolStrip_Image( "play.png", "", delegate { UpdatePlayState( PlayStates.Play ); } );
			tsbPlayFastSteps = CreateToolStrip_Image( "playfast.png", "", delegate { UpdatePlayState( PlayStates.PlayFast ); } );
			
			//////////////////////////////////////
			// filters, clustering
			
			tsbUnfilter = CreateToolStripDropDownButton_Image( "unfilter.png", "Unfilter", new ToolStripItem[] { } );
			
			tsbFilter = CreateToolStripDropDownButton_Image( "filter.png", "Filter", new ToolStripButton[] { } );
			FillFilterDropDown();
			
			tsbShowVTags = CreateToolStrip_Text( "Show Selected Vertices VTags", delegate { viewer.RenderOptVTags.Toggle(); } );
			tsbShowVTags.Checked = viewer.RenderOptVTags;
			tsbShowTTags = CreateToolStrip_Text( "Show Time Tags", delegate { viewer.RenderOptTTags.Toggle(); } );
			tsbShowTTags.Checked = viewer.RenderOptTTags;
			
			tsbTag = CreateToolStripDropDownButton_Image( "tag.png", "Tags", new ToolStripItem[] {
				tsbShowVTags,
				tsbShowTTags,
				CreateToolStrip_Text( "Highlight by VTag", delegate { //, Filter Non-Highlighted
					string txt = "Enter VTag";
					if( InputBox.Show( "Highlight by VTag", "Enter VTag", ref txt ) == DialogResult.Cancel ) return;
					hist.HighlightByVTag( txt, HighlightColors.Red );
					//UnFilterHighlightedVerts.Enabled.Set( true );
				} ),
				new ToolStripSeparator(),
				CreateToolStrip_Text( "Add VTag to Highlighted Vertices", delegate {
					string txt = "New VTag";
					if( InputBox.Show( "Add VTag: Highlighted Vertices", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
					hist.AddVTag( null, txt, AddVTagCriterion.Highlighted );
				} ),
				CreateToolStripDropDownButton_Text( "Add TTag", new ToolStripItem[] {
					CreateToolStrip_Text( "Current View", delegate {
						string txt = "New TTag";
						if( InputBox.Show( "Add TTag: Current View", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
						hist.AddTTag( timeline.GetCurrentCluster(), txt );
					} ),
					CreateToolStrip_Text( "Viewing Window", delegate {
						string txt = "New TTag";
						if( InputBox.Show( "Add TTag: Viewing Window", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
						hist.AddTTag( timeline.GetIndex0ViewWindowStart(), timeline.GetIndex0ViewWindowEnd(), txt );
					} ),
				} ),
				new ToolStripSeparator(),
				CreateToolStrip_Text( "Save VTags", delegate {
					hist.SaveVTags( "vtags.txt" );
				} ),
				CreateToolStrip_Text( "Load VTags", delegate {
					hist.LoadVTags( "vtags.txt" );
				} ),
				/*CreateToolStripDropDownButton_Text( "Add VTag", new ToolStripItem[] {
					CreateToolStrip_Text( "Selected Vertices", delegate {
						//MessageBox.Show( "Rewrite!" );
						//string txt = "New VTag";
						//if( InputBox.Show( "Add VTag: Selected Vertices", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
						//hist.AddVTag( viewer.GetViewableSum(), txt, AddVTagCriterion.Selected );
					} ),
					CreateToolStrip_Text( "Highlighted Vertices", delegate {
						MessageBox.Show( "Rewrite!" );
						//string txt = "New VTag";
						//if( InputBox.Show( "Add VTag: Highlighted Vertices", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
						//hist.AddVTag( viewer.GetViewableSum(), txt, AddVTagCriterion.Highlighted );
					} ),
					CreateToolStrip_Text( "Visible Vertices", delegate {
						MessageBox.Show( "Rewrite!" );
						//string txt = "New VTag";
						//if( InputBox.Show( "Add VTag: Visible Vertices", "Enter Tag", ref txt ) == DialogResult.Cancel ) return;
						//hist.AddVTag( viewer.GetViewableSum(), txt, AddVTagCriterion.Visible );
					} ),
				} ),*/
			} );
			
			tsbCluster = CreateToolStripDropDownButton_Image( "cluster.png", "Cluster", new ToolStripItem[] {
			} );
			
			tsbLoD = CreateToolStripDropDownButton_Image( "lod.png", "Level of Detail", new ToolStripItem[] { } );
			FillLoDDropDown();
			
			//////////////////////////////////////
			// information
			
			tsbInfo = CreateToolStripDropDownButton_Image( "info.png", "Information", new ToolStripItem[] {
				CreateToolStrip_Text( "N-Grams", delegate { hist.PrintTuplesInfo( hist.CurrentLevel.Val.GetClusters() ); } ),
				CreateToolStrip_Text( "Camera", delegate {
					CameraProperties cam = viewer.GetCamera();
					System.Console.WriteLine( "Camera: {0} {1} {2} {3}", cam.GetTarget().ToStringFormatted(), cam.GetRotation().ToStringWXYZ(), cam.GetDistance(), cam.GetOrtho() );
				} ),
				CreateToolStrip_Text( "Cluster Info", delegate {
					Cluster cluster = timeline.GetCurrentCluster();
					int i0 = cluster.snapshots[0];
					hist.PrintSnapshotInfo( i0 );
				} ),
				CreateToolStrip_Text( "Statistics Dump", delegate {
					hist.StatisticsDump();
				} ),
				CreateToolStrip_Text( "Print OTags in View Window", delegate {
					int i0min = timeline.GetIndex0ViewWindowStart();
					int i0max = timeline.GetIndex0ViewWindowEnd();
					int indmin = hist.CurrentLevel.Val.GetClusterIndex( i0min );
					int indmax = hist.CurrentLevel.Val.GetClusterIndex( i0max );
					System.Console.WriteLine( "{0}, {1}-{2}", hist.CurrentLevel.Val.Level, i0min, i0max );
					List<Cluster> clusters = hist.CurrentLevel.Val.GetClusters();
					for( int i = indmin; i < indmax; i++ )
						System.Console.WriteLine( "{0}", clusters[i].GetOTag() );
				} ),
				CreateToolStrip_Text( "Print Level Info", delegate {
					System.Console.WriteLine( "Level: " + timeline.GetCurrentLayer().Level );
					System.Console.WriteLine( "Clusters: " + timeline.GetCurrentLayer().GetClusters().Count );
				} ),
				CreateToolStrip_Text( "Element Count", delegate {
					IndexedViewableAlpha[] vs = hist.GetViewables( timeline.GetCurrentCluster(), false, false, true, false );
					IndexedViewableAlpha v = vs[0];
					HashSet<int> uids = new HashSet<int>();
					while( v != null )
					{
						foreach( int uid in v.VertUIDs ) if( uid >= 0 ) uids.Add( uid );
						v = v.attached;
					};
					System.Console.WriteLine( "Verts: " + uids.Count );
				} ),
//				CreateToolStrip_Text( "Root info", delegate { hist.PrintModelTreeRootInformation( root ); } ),
//				CreateToolStrip_Text( "ModelTree info", delegate {
//					hist.PrintModelTreeInformation( mtree ); System.Console.WriteLine();
//					hist.PrintModelTreeInformationDetailed( mtree );
//				} ),
//				CreateToolStrip_Text( "Show Vert Trails", delegate {
//					foreach( ModelTree t in root.lst ) {
//						if( t is ModelTreeSummary ) {
//							ModelTreeSummary n = (ModelTreeSummary)t;
//							n.composition = SummaryCompositions.VertexTrails;
//						}
//					}
//					UpdateRoot( root );
//					viewer.Invalidate();
//				} ),
			} );
			
			/////////////////////////////
			
			tsbDiff = CreateToolStrip_Text( "Diff", delegate {
				Cluster cluster = new Cluster( timeline.GetIndex0ViewWindowStart(), timeline.GetIndex0ViewWindowEnd(), "Diff", Composition.GetPreset( CompositionPresets.CompareBeforeAfter ) );
				//viewer.RenderOptSeparateViewports.Set( true );
				viewer.SetCluster( timeline.GetCurrentLayer(), cluster );
			} );
			
			timeline = Timeline.timeline;
			timeline.InitializeControl();
			timeline.Dock = DockStyle.Fill;
			timeline.MouseEnter += delegate { timeline.Focus(); };
			timeline.Padding = timeline.Margin = new Padding( 0, 0, 0, 0 );
			
			
			txtName = new TextBox();
			txtName.Dock = DockStyle.Fill;
			txtName.BorderStyle = BorderStyle.FixedSingle;
			txtName.TextChanged += delegate { UpdateClusterInfo(); };
			txtName.Padding = txtName.Margin = new Padding( 0, 0, 0, 1 );
			
			cboComposition = new ComboBox();
			cboComposition.Dock = DockStyle.Fill;
			cboComposition.FlatStyle = FlatStyle.Flat;
			//cboComposition.DropDownStyle = ComboBoxStyle.DropDownList;
			cboComposition.Text = "Composition Presets";
			cboComposition.DataSource = Enum.GetValues( typeof( CompositionPresets ) );
			cboComposition.Padding = cboComposition.Margin = new Padding( 1, 0, 1, 1 );
			cboComposition.SelectedValueChanged += delegate { UpdateClusterInfo(); };
			
			txtAnnotation = new TextBox();
			txtAnnotation.Dock = DockStyle.Fill;
			txtAnnotation.Multiline = true;
			txtAnnotation.WordWrap = true;
			txtAnnotation.ScrollBars = ScrollBars.Vertical;
			txtAnnotation.BorderStyle = BorderStyle.FixedSingle;
			txtAnnotation.TextChanged += delegate { UpdateClusterInfo(); };
			txtAnnotation.Padding = txtAnnotation.Margin = new Padding( 0, 0, 0, 0 );
			
			
			ts.Items.Add( new ToolStripSeparator() );
			ts.Items.Add( tsbPlayFastRevSteps );
			ts.Items.Add( tsbPlayRevSteps );
			ts.Items.Add( tsbPauseSteps );
			ts.Items.Add( tsbPlaySteps );
			ts.Items.Add( tsbPlayFastSteps );
			
			//if( ModelingHistory.DEBUGMODE )
			//{
				ts.Items.Add( new ToolStripSeparator() );
				ts.Items.Add( tsbFirstStep );
				ts.Items.Add( tsbPrevStep );
				ts.Items.Add( tsbNextStep );
				ts.Items.Add( tsbLastStep );
			//}
			
			ts.Items.Add( new ToolStripSeparator() );
			//ts.Items.Add( tsbUnfilter );
			ts.Items.Add( tsbFilter );
			ts.Items.Add( tsbTag );
			//ts.Items.Add( tsbCluster );
			ts.Items.Add( tsbLoD );
			
			if( ModelingHistory.DEBUGMODE ) ts.Items.Add( tsbDiff );
			
			ts.Items.Add( new ToolStripSeparator() );
			
			if( ModelingHistory.DEBUGMODE ) ts.Items.Add( tsbInfo );
			
			
			tlpClusterInfo.Controls.Add( txtName );
			tlpClusterInfo.Controls.Add( cboComposition );
			tlpClusterInfo.Controls.Add( txtAnnotation );
			
			tlpViewerCluster.Controls.Add( viewer );
			tlpViewerCluster.Controls.Add( tlpClusterInfo );
			
			tlp.Controls.Add( ts );
			tlp.Controls.Add( tlpViewerCluster );
			tlp.Controls.Add( viewerwindow );
			tlp.Controls.Add( timeline );
			
			this.Controls.Add( tlp );
		}
		
		/*private void UpdateRoot( ModelTreeRoot nroot )
		{
			if( nroot.lst.Count == 0 ) throw new Exception( "bad clustering" );
			
			System.Console.WriteLine( "Updating root..." );
			System.Console.WriteLine( "    root.lst.Count  = " + root.lst.Count );
			System.Console.WriteLine( "    nroot.lst.Count = " + nroot.lst.Count );
			
			root = nroot;
			sbIndex.Root = root;
			sbIndex.SetIndexClosest( mtree.GetRepIndex() );
			SetModelTree( root.lst[sbIndex.Index] );
		}*/
		
		#region ToolStrip Helper Functions
		
		private ToolStripDropDown CreateToolStripDropDown( ToolStripItem[] dropdownitems )
		{
			ToolStripDropDown tsdd = new ToolStripDropDown();
			tsdd.Items.AddRange( dropdownitems );
			return tsdd;
		}
		
		private ToolStripDropDownButton CreateToolStripDropDownButton_Image( string image, string label, ToolStripDropDown dropdown )
		{
			ToolStripDropDownButton tsb = new ToolStripDropDownButton( label, MiscFileIO.LoadBitmapResource( image ) );
			tsb.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsb.DropDown = dropdown;
			return tsb;
		}
		private ToolStripDropDownButton CreateToolStripDropDownButton_Image( string image, string label, ToolStripItem[] dropdownitems )
		{
			ToolStripDropDownButton tsb = new ToolStripDropDownButton( label, MiscFileIO.LoadBitmapResource( image ) );
			tsb.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsb.DropDown = CreateToolStripDropDown( dropdownitems );
			return tsb;
		}
		
		private ToolStripDropDownButton CreateToolStripDropDownButton_Text( string label, ToolStripItem[] dropdownitems )
		{
			ToolStripDropDownButton tsb = new ToolStripDropDownButton( label );
			tsb.DisplayStyle = ToolStripItemDisplayStyle.Text;
			tsb.DropDown = CreateToolStripDropDown( dropdownitems );
			return tsb;
		}
			
		private ToolStripButton CreateToolStrip_Image( string image, string label, EventHandler clickaction )
		{
			ToolStripButton tsb = new ToolStripButton( label, MiscFileIO.LoadBitmapResource( image ) );
			tsb.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsb.Click += clickaction;
			return tsb;
		}
		
		private ToolStripButton CreateToggleToolStrip_Text( string label, Property<bool> ToggleProperty )
		{
			ToolStripButton tsb = new ToolStripButton( label );
			tsb.Checked = ToggleProperty.Val;
			tsb.Click += delegate {
				ToggleProperty.Set( !ToggleProperty );
			};
			ToggleProperty.PropertyChanged += delegate(object sender, PropertyChangedEventArgs e) {
				Property<bool> prop = (Property<bool>) sender;
				if( prop.Val != tsb.Checked ) tsb.Checked = prop.Val;
			};
			return tsb;
		}
		
		private ToolStripButton CreateToolStrip_Text( string label, EventHandler clickaction ) { return CreateToolStrip_Text( label, clickaction, false, false ); }
		private ToolStripButton CreateToolStrip_Text( string label, EventHandler clickaction, bool checkonclick, bool ischecked )
		{
			ToolStripButton tsb = new ToolStripButton( label );
			tsb.CheckOnClick = checkonclick;
			tsb.Checked = ischecked;
			tsb.Click += clickaction;
			return tsb;
		}
		
		#endregion
		
		private void FillFilterDropDown()
		{
			List<ToolStripItem> tsbfilters = new List<ToolStripItem>();
			foreach( Filtering filter in hist.Filters )
			{
				tsbfilters.Add( CreateToggleToolStrip_Text( filter.Label, filter.Enabled ) );
			}
			
			tsbFilter.DropDownItems.Clear();
			tsbFilter.DropDownItems.AddRange( tsbfilters.ToArray() );
			
			// CreateToggleToolStrip_Text( "view", FilterNameView.Enabled ),
			// CreateToggleToolStrip_Text( "view*", FilterNameStartsWithView.Enabled ),
			// CreateToggleToolStrip_Text( "select", FilterNameSelect.Enabled ),
			// CreateToggleToolStrip_Text( "select*", FilterNameStartsWithSelect.Enabled ),
			// CreateToggleToolStrip_Text( "transform", FilterNameTransform.Enabled ),
			// CreateToggleToolStrip_Text( "transform*", FilterNameStartsWithTransform.Enabled ),
			// CreateToggleToolStrip_Text( "gui*", FilterNameStartsWithGUI.Enabled ),
			// CreateToggleToolStrip_Text( "undo", FilterUndos.Enabled ),
			// CreateToggleToolStrip_Text( "vtag test", FilterVTagTest.Enabled ),
			// CreateToggleToolStrip_Text( "highlighted verts", FilterHighlightedVerts.Enabled ),
			// CreateToggleToolStrip_Text( "(un) highlighted verts", UnFilterHighlightedVerts.Enabled ),
		}
		
		private void FillLoDDropDown()
		{
			tsbLoD.DropDownItems.Clear();
			/*for( int i = hist.Layers.nlevels - 1; i >= 0; i-- )
			{
				Clustering layer = hist.Layers.GetClusteringLayer( i );
				tsbLoD.DropDownItems.Add( CreateToolStrip_Text( "" + i + ":" + layer.Label, delegate( object o, EventArgs e ) {
					int ilevel = Int32.Parse( ((ToolStripButton)o).Text.SplitOnce( ':' )[0] );
					hist.CurrentLevel.Set( hist.Layers.GetClusteringLayer( ilevel ) );
				} ) );
			}*/
			for( int i = hist.ClusteringLayerLevels.Count - 1; i >= 0; i-- )
			{
				Clustering layer = hist.ClusteringLayerLevel_Get( i );
				string label = hist.ClusteringLayerNames[i];
				tsbLoD.DropDownItems.Add( CreateToolStrip_Text( "" + i + ":" + label, delegate( object o, EventArgs e ) {
					int ilevel = Int32.Parse( ((ToolStripButton)o).Text.SplitOnce( ':' )[0] );
					hist.CurrentClusteringLayerLevel_Set( ilevel );
					//hist.CurrentLevel.Set( hist.Layers.GetClusteringLayer( ilevel ) );
				} ) );
			}
			SelectLoDInDropDown();
		}
		
		private void UpdatePlayState( PlayStates newstate )
		{
			PlayState = newstate;
			
			//if( PlayState == ViewerWindow.PlayStates.Stop ) viewer.SmoothedCamera.Set( false );
			//else viewer.SmoothedCamera.Set( true );
			
			if( PlayState == ViewerWindow.PlayStates.PlayFast || PlayState == ViewerWindow.PlayStates.PlayFastRev ) viewer.EaseSpeed = EaseSpeedFast;
			else viewer.EaseSpeed = EaseSpeed;
			
			//playtimer.Interval =	( PlayState == ViewerWindow.PlayStates.Play || PlayState == ViewerWindow.PlayStates.PlayRev ? PlaySpeed : PlayFastSpeed );
			//playtimer.Enabled =		( PlayState != ViewerWindow.PlayStates.Stop );
			
			tsbPlaySteps.Checked =			( PlayState == ViewerWindow.PlayStates.Play );
			tsbPlayFastSteps.Checked =		( PlayState == ViewerWindow.PlayStates.PlayFast );
			tsbPlayRevSteps.Checked =		( PlayState == ViewerWindow.PlayStates.PlayRev );
			tsbPlayFastRevSteps.Checked =	( PlayState == ViewerWindow.PlayStates.PlayFastRev );
			tsbPauseSteps.Checked =			( PlayState == ViewerWindow.PlayStates.Stop );
		}
	}
}





