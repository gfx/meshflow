using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Libs.MiscFunctions;

namespace ModelingViewer
{
	public delegate bool CommandFunctionDelegate( String[] args );
	public delegate bool ReportingFunctionDelegate( String txt );

	public class Command
	{
		public Boolean IsRequired { get; set; }
		public String sCommand { get; set; }
		public String sUsageDescription { get; set; }
		public String sDescription { get; set; }
		public Int32 nRequiredArguments { get; set; }
		public CommandFunctionDelegate fnCommand { get; set; }
	}

	public class CommandException : Exception
	{
		public CommandException( String message ) : base(message) { }
	}

	public class CommandHandler
	{
		public String sCommandDelimiter = "--";
		public String sArgumentDelimiter = " ";
		public Boolean bCaseSensitive = false;

		public String sHelp = null;
		public ReportingFunctionDelegate DisplayHelpFunction;
		public ReportingFunctionDelegate UnknownCommandFunction;
		public ReportingFunctionDelegate CommandRequiresArgumentsFunction;
		public ReportingFunctionDelegate CommandRequiresNoArgumentsFunction;

		public List<Command> Commands = new List<Command>();
		public List<Boolean> CmdFulfilled;

		public void AddCommand( Command command )
		{
			Commands.Add( command );
		}

		public String GetHelpString()
		{
			String sHelp = "Commands Help";
			foreach ( Command cmd in Commands )
			{
				if ( cmd.sUsageDescription == null )
					sHelp += "\n" + sCommandDelimiter + cmd.sCommand + " : " + cmd.sDescription;
				else
					sHelp += "\n" + sCommandDelimiter + cmd.sCommand + sArgumentDelimiter + cmd.sUsageDescription + " : " + cmd.sDescription;
				if( cmd.IsRequired ) sHelp += "   * IS REQUIRED";
			}
			return sHelp;
		}

		public bool ParseCommands( string[] sArgs )
		{
			return ParseCommands( String.Join( sArgumentDelimiter, sArgs ) );
		}
		
		public bool ParseCommands( String cmdsline )
		{
			String[] cmds = cmdsline.Split( new String[] { sCommandDelimiter }, StringSplitOptions.RemoveEmptyEntries );
			
			CmdFulfilled = new List<Boolean>( Commands.Count );
			foreach( Command cmd in Commands ) CmdFulfilled.Add( !cmd.IsRequired );
			
			foreach ( String cmd in cmds )
			{
				Boolean ret = ParseCommand( cmd );
				if ( !ret ) return false;
			}
			
			Commands.Each( delegate( Command cmd, int i ) {
				if( !CmdFulfilled[i] ) throw new CommandException( "Command is required: " + cmd.sCommand );
			} );
			return true;
		}

		public bool ParseCommand( String cmdline )
		{
			String[] args = cmdline.Split( new String[] { sArgumentDelimiter }, StringSplitOptions.RemoveEmptyEntries );
			StringComparison scmp = (bCaseSensitive ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase);
			String cmd = args[0].Trim();

			if ( sHelp != null && cmd.Equals( sHelp, scmp ) )
			{
				if ( !DisplayHelpFunction( GetHelpString() ) ) return false;
				return true;
			}

			bool? retvalue = null;
			Commands.Each( delegate( Command command, int index ) {
				if( retvalue != null ) return;
				if ( !cmd.Equals( command.sCommand, scmp ) ) return;

				// check if argument count is correct
				if ( args.Length != command.nRequiredArguments + 1 )
				{
					if ( command.nRequiredArguments > 0 )
					{
						if ( CommandRequiresArgumentsFunction != null )
						{
							if ( !CommandRequiresArgumentsFunction( sCommandDelimiter + cmd ) ) { retvalue = false; return; }
							retvalue = false;
							return;
						}
						throw new CommandException( "Command Requires Arguments: " + cmd );
					}
					else
					{
						if ( CommandRequiresNoArgumentsFunction != null )
						{
							if ( !CommandRequiresNoArgumentsFunction( sCommandDelimiter + cmd ) ) { retvalue = false; return; }
							retvalue = false;
							return;
						}
						throw new CommandException( "Command Requires No Arguments: " + cmd );
					}
				}
				
				if ( command.fnCommand != null ) { CmdFulfilled[index] = true; retvalue = command.fnCommand( args ); return; }
				throw new CommandException( "No handling function for command: " + cmd );
			} );
			
			if( retvalue != null ) return (bool) retvalue;

			if ( UnknownCommandFunction != null )
			{
				if ( !UnknownCommandFunction( cmd ) ) return false;
				return false;
			}
			throw new CommandException( "Unhandled Command: " + cmd );
		}
	}
}
