using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public interface IDebugWritable
	{
		void DebugWrite();
	}
	
	public partial class DebugWindow : Form
	{
		public static DebugWindow dbg;
		
		public static Boolean bDeferRefresh = false;

		private static Boolean bUsingProgress = false;
		private static float perCurrentProgressBar = 0.0f;
		private static float perLastProgressBar = -1.0f;
		private static Int32 nCurrentProgressBars = 0;
		private static Int32 nMaxProgressBars = 100;
		private static String sProgressBar = "*";
		private static Timer tmrProgress;

		private static String sSectionIndent = "    ";
		private static int nSections = 0;
		private static Timer[] sectionTimers = new Timer[100];

		private static Boolean bWriteToGUI = true;
		public static Boolean bTextProgressBar = true;
		public static Boolean bNoEraseTextProgressBar = false;
		
		private static object writelock = new object();

		static public void WriteLine( String st )
		{
			lock( writelock )
			{
				if ( bWriteToGUI && dbg != null ) dbg._WriteLine( st );
				System.Console.Out.WriteLine( st );
			}
		}
		static public void Write( String st )
		{
			lock( writelock )
			{
				if ( bWriteToGUI && dbg != null ) dbg._Write( st );
				System.Console.Out.Write( st );
			}
		}
		static public void RefreshOutput()
		{
			if( dbg != null ) dbg._RefreshOutput();
		}

		#region Section Functions

		static private String SectionIndent( String st )
		{
			return StringFunctions.Repeat( sSectionIndent, nSections ) + st;
		}

		static public void SectionStart( String st )
		{
			SectionWriteLine( st );
			if ( nSections < sectionTimers.Length ) sectionTimers[nSections] = new Timer();
			nSections++;
		}

		static public void SectionWriteLine( String st )
		{
			if ( st == null ) return;
			WriteLine( SectionIndent( st ) );
		}
		
		static public void SectionWriteVars( params object[] objs )
		{
			int max = 0;
			string[] sts = new string[objs.Length];
			objs.Each( delegate( object o, int i ) { sts[i] = o.ToString(); } );
			for( int i = 0; i < sts.Length; i += 2 ) max = Math.Max( max, sts[i].Length );
			for( int i = 0; i < sts.Length; i += 2 )
			{
				String line = String.Format( "{0,-" + max + "}: {1}", sts[i], sts[i+1] );
				SectionWriteLine( line );
			}
		}
		
		static public void SectionWriteLines( String st )
		{
			if ( st == null ) return;
			String[] lines = st.Split( '\n' );
			foreach( String line in lines ) WriteLine( SectionIndent( line.Trim() ) ); 
		}
		
		static public void SectionWriteStackTrace( Exception e )
		{
			SectionStart( "Stack trace:" );
			SectionWriteLines( e.StackTrace );
			SectionEnd();
		}
		
		static public void SectionWriteException( Exception e )
		{
			SectionStart( "Exception dump:" );
			SectionWriteLine( "Message: " + e.Message );
			SectionWriteStackTrace( e );
			if( e.InnerException != null ) SectionWriteException( e.InnerException );
			SectionEnd();
		}

		static public void SectionWrite( String st )
		{
			if ( st == null ) st = "";
			Write( SectionIndent( st ) );
		}

		static public void SectionEnd() { SectionEnd( null ); }
		static public void SectionEnd( String st ) { SectionEnd( st, false); }
		static public void SectionEnd( Boolean bReportTime ) { SectionEnd( null, bReportTime ); }
		static public void SectionEnd( String st, Boolean bReportTime )
		{
			if( st != null ) SectionWriteLine( st );
			if ( bReportTime && nSections <= sectionTimers.Length && nSections > 0 ) SectionWriteLine( "Time: " + sectionTimers[nSections - 1].Elapsed() );
			if ( nSections > 0 ) nSections--;
		}
		
		static public int GetSectionCount() { return nSections; }
		static public void SetSectionCount( int nSections ) { DebugWindow.nSections = nSections; }
		static public void SectionFallBack( int toSection, String st, bool bReportTime )
		{
			if( nSections < toSection ) throw new ArgumentException( "DebugWindow.SectionFallBack: Cannot fall-back to " + toSection + " from " + nSections );
			
			while( nSections > toSection ) SectionEnd( st, bReportTime );
		}

		#endregion

		#region Text-based Progress Bar

		static public void ProgressBarTextStart() { ProgressBarTextStart( null ); }
		static public void ProgressBarTextStart( String st )
		{
			if ( !bTextProgressBar ) return;
			if ( bUsingProgress ) ProgressBarTextDone();

			bUsingProgress = true;
			nCurrentProgressBars = 0;
			perCurrentProgressBar = 0.0f;
			perLastProgressBar = 0.0f;
			tmrProgress = new Timer();

			if ( st == null ) st = "";
			if ( st.Length > 0 ) st += " ";
			st = SectionIndent( st );

			System.Console.Out.Write( st + GetProgressBarTextString() );
			if( bNoEraseTextProgressBar ) System.Console.Out.WriteLine();
		}

		static public void ProgressBarTextDone()
		{
			if ( !bTextProgressBar ) return;
			if ( !bUsingProgress ) return;

			ProgressBarTextUpdate( 1.0f );
			System.Console.Out.WriteLine( "" );
			bUsingProgress = false;
			tmrProgress = null;
		}

		static public void ProgressBarTextUpdate( float percent )
		{
			if ( !bTextProgressBar ) return;
			
			// lock(writelock)
			
			// rather than waiting on a lock to do something rather unimportant (like updating the progress bar),
			// just try to grab it, and continue if it's locked already
			
			if( Monitor.TryEnter( writelock ) )
			{
				try {
					percent = FMath.Clamp( percent, 0.0f, 1.0f );
					perCurrentProgressBar = percent;
					int nNewCurrent = (int) Math.Ceiling( (double) nMaxProgressBars * (double) percent );
					
					if ( bNoEraseTextProgressBar && nNewCurrent <= nCurrentProgressBars ) return;
					
					String sIndent = "";
					String sErase = "";
					String sBar = GetProgressBarTextString();
					String sEnding = "";
					
					if( bNoEraseTextProgressBar ) {
						sIndent = SectionIndent( "" );
						sEnding = "\n";
					} else {
						sErase = StringFunctions.Repeat( "\b", nMaxProgressBars + 2 );
					}
					
					System.Console.Out.Write( sIndent + sErase + sBar + sEnding );
					
					nCurrentProgressBars = nNewCurrent;
					perLastProgressBar = perCurrentProgressBar;
				} finally {
					Monitor.Exit( writelock );
				}
			}
		}

		static public String GetProgressBarTextString()
		{
			if ( !bUsingProgress ) return "";
			String bar = "[" + StringFunctions.Repeat( sProgressBar, nCurrentProgressBars ) + StringFunctions.Repeat( " ", nMaxProgressBars - nCurrentProgressBars ) + "]";
			
			String sElapsed = tmrProgress.ElapsedNoSubseconds();
			String sEstRemaining = tmrProgress.EstimateDone( perCurrentProgressBar );
			
			String insert = String.Format( " {0:0.0000} {1} {2} ", perCurrentProgressBar, sElapsed, sEstRemaining );
			
			int start = nMaxProgressBars/2 + 1 - insert.Length / 2;
			bar = bar.Substring(0, start) + insert + bar.Substring( start + insert.Length );
			return bar;
		}

		#endregion

		#region GUI Progress Bar -- Function Wrapper

		public delegate void ProgressBarFunction( ref float progress );
		public delegate void ProgressBarFunctionArgs( ref float progress, params object[] args );

		private static Form frmProgress;
		private static int countProgressBars = 0;
		public static void ProgressBarGUIShow( ProgressBarFunction fn ) { ProgressBarGUIShow( "Progress", fn ); }
		public static void ProgressBarGUIShow( String text, ProgressBarFunction fn )
		{
			ProgressBarGUIShow( text, delegate( ref float progress, object[] args ) { fn( ref progress ); } );
		}
		public static void ProgressBarGUIShow( String text, ProgressBarFunctionArgs fn, params object[] args )
		{
			if ( countProgressBars == 0 )
			{
				frmProgress = new Form();
				frmProgress.Text = "Progress Bar";
				frmProgress.MaximumSize = frmProgress.MinimumSize = frmProgress.Size = new Size( 450, 30 );
				frmProgress.FormBorderStyle = FormBorderStyle.None;
				frmProgress.StartPosition = FormStartPosition.CenterScreen;
				frmProgress.SizeChanged += new EventHandler( delegate( Object o, EventArgs e )
				{
					Rectangle screen = Screen.PrimaryScreen.WorkingArea;
					frmProgress.Location = new Point( ( screen.Width - frmProgress.Width ) / 2, ( screen.Height - frmProgress.Height ) / 2 );
				} );
				frmProgress.Show();
			}
			countProgressBars++;
			frmProgress.MaximumSize = frmProgress.MinimumSize = frmProgress.Size = new Size( 450, 30 * countProgressBars );

			Label lbl = new Label();
			lbl.Size = new Size( 200, 30 );
			lbl.Location = new Point( 0, 30 * ( countProgressBars - 1 ) );
			lbl.Margin = new Padding( 0, 0, 0, 0 );
			lbl.TextAlign = ContentAlignment.MiddleLeft;
			lbl.Text = text;

			ProgressBar pb = new ProgressBar();
			pb.Maximum = 100;
			pb.Size = new Size( 250, 30 );
			pb.Location = new Point( 200, 30 * ( countProgressBars - 1 ) );
			pb.Margin = new Padding( 0, 0, 0, 0 );

			frmProgress.Controls.Add( lbl );
			frmProgress.Controls.Add( pb );
			lbl.Refresh(); pb.Refresh(); frmProgress.Refresh();

			float progress = 0.0f;

			Thread thrd = new Thread( new ParameterizedThreadStart( delegate( object o ) { fn( ref progress, args ); } ) );
			thrd.Start();
			while ( !thrd.Join( 100 ) )
			{
				pb.Value = (int) ( progress * 100.0f );
				lbl.Refresh(); pb.Refresh(); frmProgress.Refresh();
			}

			frmProgress.Controls.Remove( lbl );
			frmProgress.Controls.Remove( pb );
			countProgressBars--;
			if ( countProgressBars == 0 ) { frmProgress.Dispose(); frmProgress = null; }
			else frmProgress.MaximumSize = frmProgress.MinimumSize = frmProgress.Size = new Size( 450, 30 * countProgressBars );
		}

		#endregion

		public DebugWindow()
		{
			InitializeComponent();
			//this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
		}

		public void ScrollToBottom()
		{
			try {
				lstOutput.SelectedIndex = lstOutput.Items.Count - 1;
			} catch( Exception ) {
			}
			//txtOutput.SelectionStart = txtOutput.Text.Length;
			//txtOutput.SelectionLength = 0;
			//txtOutput.ScrollToCaret();
		}
		public void _WriteLine( String st ) { lstOutput.Items.Add( st ); _RefreshOutput(); } //_Write( st + "\n" ); }
		public void _Write( String st )
		{
			//txtOutput.Text += st;
			//_RefreshOutput();
		}
		
		public void _RefreshOutput()
		{
			if( bDeferRefresh ) return;
			ScrollToBottom();
			this.Refresh();
		}

		// does not work :(
		public void WriteBoldLine( String st )
		{
			WriteLine( st );

			/*String rtf = txtOutput.Rtf;
			String rtf0 = rtf;
			rtf = rtf.Substring( 0, rtf.LastIndexOf( "\\par" ));
			String rtf1 = rtf;
			rtf += "\\ul " + st + " \\ul0 \\par\n\\par\n}";
			MessageBox.Show( rtf0 + "\n\n" + rtf1 + "\n\n" + rtf );
			txtOutput.Rtf = rtf;
			ScrollToBottom();*/

			/*txtOutput.Text += "blah\n";
			txtOutput.Find( "blah", RichTextBoxFinds.MatchCase );
			MessageBox.Show( txtOutput.SelectedText );
			txtOutput.SelectionFont = new Font("Liberation Mono", 16, FontStyle.Bold);
			txtOutput.SelectionColor = Color.Red;
			ScrollToBottom();*/

			/*txtOutput.Find( "[inserthere]", RichTextBoxFinds.MatchCase );
			Font fnt = txtOutput.Font;
			txtOutput.SelectionFont = new Font( fnt, FontStyle.Bold );
			txtOutput.SelectedText = st + "\n";
			ScrollToBottom();
			txtOutput.Font = fnt;*/
			//WriteLine( "{\\colortbl;\\red255\\green255\\blue255;\\red255\\green0\\blue0;}BOLD\\cf2" + st );
		}

		private void DebugWindow_Load( object sender, EventArgs e )
		{
			//this.WindowState = FormWindowState.Minimized;
			this.ControlBox = false;
		}

		private void DebugWindow_FormClosing( object sender, FormClosingEventArgs e )
		{
			if ( e.CloseReason == CloseReason.MdiFormClosing ) return;
			this.WindowState = FormWindowState.Minimized;
			e.Cancel = true;
		}
		
		private void InitializeComponent()
		{
			this.txtInput = new System.Windows.Forms.TextBox();
			//this.txtOutput = new System.Windows.Forms.RichTextBox();
			this.lstOutput = new System.Windows.Forms.ListBox();
			this.SuspendLayout();
			// 
			// txtInput
			// 
			this.txtInput.BackColor = System.Drawing.Color.Black;
			this.txtInput.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.txtInput.Enabled = false;
			this.txtInput.Font = new System.Drawing.Font( "Liberation Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
			this.txtInput.ForeColor = System.Drawing.Color.White;
			this.txtInput.Location = new System.Drawing.Point( 0, 358 );
			this.txtInput.Name = "txtInput";
			this.txtInput.Size = new System.Drawing.Size( 858, 20 );
			this.txtInput.TabIndex = 0;
			// 
			// txtOutput
			// 
			/*this.txtOutput.BackColor = System.Drawing.Color.Black;
			this.txtOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.txtOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.txtOutput.Font = new System.Drawing.Font( "Liberation Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
			this.txtOutput.ForeColor = System.Drawing.Color.White;
			this.txtOutput.Location = new System.Drawing.Point( 0, 0 );
			this.txtOutput.Name = "txtOutput";
			this.txtOutput.ReadOnly = true;
			this.txtOutput.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
			this.txtOutput.ShortcutsEnabled = false;
			this.txtOutput.ShowSelectionMargin = true;
			this.txtOutput.Size = new System.Drawing.Size( 858, 358 );
			this.txtOutput.TabIndex = 1;
			this.txtOutput.Text = "";
			this.txtOutput.WordWrap = false;*/
			
			this.lstOutput.ScrollAlwaysVisible = true;
			this.lstOutput.BackColor = System.Drawing.Color.Black;
			this.lstOutput.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.lstOutput.Dock = System.Windows.Forms.DockStyle.Fill;
			this.lstOutput.Font = new System.Drawing.Font( "Liberation Mono", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ( (byte) ( 0 ) ) );
			this.lstOutput.ForeColor = System.Drawing.Color.White;
			this.lstOutput.Location = new System.Drawing.Point( 0, 0 );
			this.lstOutput.Name = "lstOutput";
			this.lstOutput.Size = new System.Drawing.Size( 858, 358 );
			this.lstOutput.TabIndex = 1;
			this.lstOutput.IntegralHeight = false;
			this.lstOutput.HorizontalScrollbar = true;
			
			// 
			// DebugWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size( 858, 378 );
			this.Controls.Add( this.lstOutput );
			this.Controls.Add( this.txtInput );
			this.Name = "DebugWindow";
			this.Text = "DebugWindow";
			this.Load += new System.EventHandler( this.DebugWindow_Load );
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler( this.DebugWindow_FormClosing );
			this.ResumeLayout( false );
			this.PerformLayout();

		}

		private System.Windows.Forms.TextBox txtInput;
		private System.Windows.Forms.ListBox lstOutput;
		//private System.Windows.Forms.RichTextBox txtOutput;
	}
}
