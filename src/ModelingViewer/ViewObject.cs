using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Libs.MiscFunctions;
using Common.Libs.ParallelFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public enum HighlightViewingOptions
	{
		AllWithHighlights, NoHighlights, OnlyHighlights
	}
	
	public enum HighlightColors
	{
		None,
		Red,
		Orange,
		Yellow,
		Green,
		Cyan,
		Blue,
		Purple,
		Pink,
		White
	}
	
	public static class Highlight
	{
		public static readonly Vec3f?[] Colors = new Vec3f?[] {
			null,
			new Vec3f(1,0,0),
			new Vec3f(1,0.5f,0),
			new Vec3f(1,1,0),
			new Vec3f(0,1,0),
			new Vec3f(0,1,1),
			new Vec3f(0,0,1),
			new Vec3f(0.5f,0,0.5f),
			new Vec3f(1,0.5f,1),
			new Vec3f(1,1,1)
		};
		
		public static Vec3f? GetColor( HighlightColors color ) { return Colors[(int)color]; }
	}
	
	public class AppearanceData
	{
		public Vec3f Color { get; set; }
		public float ColorMult { get; set; }
		public bool IsMasked { get; set; }		// should be rendered?
		public HighlightColors Highlight { get; set; }
	}
	
	public class ViewPoint
	{
		public Vec3f coord { get; set; }
		public AppearanceData appearance {get;set;}
		public ViewPoint( Vec3f coord, AppearanceData appearance ) { this.coord = coord; this.appearance = appearance; }
		public ViewPoint( Vec3f coord, Vec3f color ) { this.coord = coord; this.appearance = new AppearanceData() { Color = color, IsMasked = false, Highlight = HighlightColors.None }; }
	}
	
	public class ViewEdge
	{
		public int i1 { get; set; }
		public int i2 {get;set;}
		public ViewEdge( int i1, int i2 ) { this.i1 = i1; this.i2 = i2; }
	}
	
	public class ViewPoly
	{
		public int i1 {get; set;}
		public int i2 {get; set;}
		public int i3 {get; set;}
		public ViewPoly( int i1, int i2, int i3 ) { this.i1 = i1; this.i2 = i2; this.i3 = i3; }
	}
	
	public delegate AppearanceData AppearanceFunctionIndexDelegate( int index );
	public delegate AppearanceData AppearanceFunctionXYDelegate( int x, int y );
	
	
	public class ViewObject : Viewable
	{
		public ViewObject() : base() { }
		public ViewObject( ViewPoint[] points, ViewEdge[] edges, ViewPoly[] polygons ) : base( points, edges, polygons ) { }
		
		public int AddPoint( ViewPoint vp )
		{
			int ip = points.Count;
			points.Add( vp );
			fireDataChangedEvent();
			return ip;
		}
		
		public int AddEdge( ViewEdge ve )
		{
			int ie = edges.Count;
			edges.Add( ve );
			fireDataChangedEvent();
			return ie;
		}
		
		public int AddEdge( ViewPoint vp1, ViewPoint vp2 )
		{
			deferDataChangedEvent = true;
			int ie = edges.Count;
			int i1 = AddPoint( vp1 );
			int i2 = AddPoint( vp2 );
			edges.Add( new ViewEdge( i1, i2 ) );
			deferDataChangedEvent = false;
			fireDataChangedEvent();
			return ie;
		}
		
		public int AddPolygon( ViewPoint vp1, ViewPoint vp2, ViewPoint vp3 )
		{
			deferDataChangedEvent = true;
			int ip = polygons.Count;
			int i1 = AddPoint( vp1 );
			int i2 = AddPoint( vp2 );
			int i3 = AddPoint( vp3 );
			polygons.Add( new ViewPoly( i1, i2, i3 ) );
			deferDataChangedEvent = false;
			fireDataChangedEvent();
			return ip;
		}
		
		public void AddObject( ViewObject obj )
		{
			if ( !obj.HasPoints ) return;
			
			deferDataChangedEvent = true;
			int ptadd = points.Count;
			points.AddRange( obj.points );
			foreach ( ViewEdge e in obj.edges ) edges.Add( new ViewEdge( e.i1 + ptadd, e.i2 + ptadd ) );
			foreach ( ViewPoly p in obj.polygons ) polygons.Add( new ViewPoly( p.i1 + ptadd, p.i2 + ptadd, p.i3 + ptadd ) );
			deferDataChangedEvent = false;
			fireDataChangedEvent();
		}
		
	}
	
	public class Viewable
	{
		protected List<ViewPoint> points = new List<ViewPoint>();
		protected List<ViewEdge> edges = new List<ViewEdge>();
		protected List<ViewPoly> polygons = new List<ViewPoly>();
		
		public FlattenedViewable vpointsCache = null;
		public FlattenedViewable vedgesCache = null;
		public FlattenedViewable vpolygonsCache = null;
		private Vec3f? min = null, max = null, center = null;
		private int[] polygonCacheIndex;
		
		private System.Boolean renderPoints = true;
		private System.Boolean renderEdges = true;
		private System.Boolean renderPolygons = true;
		
		private HighlightViewingOptions renderHighlights;
		
		private float exposure = 1.0f;
		private Vec3f lightdir = new Vec3f( 0.0f, 0.0f, 1.0f );
		
		#region Events
		
		public event EventHandler ViewOptionsChangedEvent;
		public event EventHandler DataChangedEvent;
		
		protected System.Boolean deferViewOptionsChangedEvent = false;
		protected System.Boolean deferDataChangedEvent = false;
		
		protected void fireViewOptionsChangedEvent()
		{
			min = max = center = null;
			if ( deferViewOptionsChangedEvent ) return;
			if ( ViewOptionsChangedEvent != null ) ViewOptionsChangedEvent( this, null );
		}
		
		protected void fireDataChangedEvent() { fireDataChangedEvent(true); }
		protected void fireDataChangedEvent( bool bClearCache )
		{
			if( bClearCache ) {
				vedgesCache = vpointsCache = vpolygonsCache = null;
				min = max = center = null;
			}
			if ( deferDataChangedEvent ) return;
			if ( DataChangedEvent != null ) DataChangedEvent( this, null );
		}
		
		#endregion
		
		#region Properties
		
		public System.Boolean HasPoints { get { return ( points.Count > 0 ); } }
		public System.Boolean HasEdges { get { return ( edges.Count > 0 ); } }
		public System.Boolean HasPolygons { get { return ( polygons.Count > 0 ); } }
		
		public float Exposure {
			get { return exposure; }
			set { exposure = value; fireDataChangedEvent(); }
		}
		public Vec3f LightingDirection {
			get { return lightdir; }
			set { lightdir = value; fireDataChangedEvent(); }
		}
		
		public int PointCount { get { return points.Count; } }
		public int EdgeCount { get { return edges.Count; } }
		public int PolygonCount { get { return polygons.Count; } }
		
		public Vec3f Min { get { if ( min == null ) UpdateMinMax(); if ( min == null ) return Vec3f.Zero; return (Vec3f) min; } }
		public Vec3f Max { get { if ( max == null ) UpdateMinMax(); if ( max == null ) return Vec3f.Zero; return (Vec3f) max; } }
		public Vec3f Center { get { if ( center == null ) UpdateMinMax(); if ( center == null ) return Vec3f.Zero; return (Vec3f) center; } }
		
		public System.Boolean RenderPoints
		{
			get { return renderPoints; }
			set
			{
				if ( renderPoints == value ) return;
				renderPoints = value;
				fireViewOptionsChangedEvent();
			}
		}
		public System.Boolean RenderEdges
		{
			get { return renderEdges; }
			set
			{
				if ( renderEdges == value ) return;
				renderEdges = value;
				fireViewOptionsChangedEvent();
			}
		}
		public System.Boolean RenderPolygons
		{
			get { return renderPolygons; }
			set
			{
				if ( renderPolygons == value ) return;
				renderPolygons = value;
				fireViewOptionsChangedEvent();
			}
		}
		public HighlightViewingOptions RenderHighlights
		{
			get { return renderHighlights; }
			set
			{
				if ( renderHighlights == value ) return;
				renderHighlights = value;
				fireViewOptionsChangedEvent();
			}
		}
		
		#endregion
		
		#region Get Flattened Data Functions
		
		// returns false if point is masked
		protected bool GetPointData( ViewPoint vp, ref Vec3f coord, ref Vec3f color, ref bool IsHighlight ) {
			if( vp.appearance.IsMasked ) return false;
			
			coord = vp.coord;
			
			Vec3f? hlcolor;
			
			switch(renderHighlights) {
				case HighlightViewingOptions.NoHighlights:
					color = vp.appearance.Color;
					IsHighlight = false;
					return true;
					
				case HighlightViewingOptions.AllWithHighlights:
					hlcolor = Highlight.GetColor(vp.appearance.Highlight );
					if(hlcolor == null) { color = vp.appearance.Color; IsHighlight = false; }
					else { color = (Vec3f) hlcolor; IsHighlight = true; }
					return true;
					
				case HighlightViewingOptions.OnlyHighlights:
					hlcolor = Highlight.GetColor(vp.appearance.Highlight );
					if(hlcolor == null) return false;
					color = (Vec3f) hlcolor;
					IsHighlight = true;
					return true;
					
				default:
					throw new Exception("Unimplemented HighlightViewingOptions");
			}
		}

		public FlattenedViewable GetFlattenedViewablePoints()
		{
			if ( vpointsCache != null ) return vpointsCache;
			
			int nMax = points.Count;
			List<Vec3f> coords = new List<Vec3f>(nMax);
			List<Vec3f> colors = new List<Vec3f>(nMax);
			List<bool> highlights = new List<bool>(nMax);
			
			Vec3f coord = Vec3f.Zero;
			Vec3f color = Vec3f.Zero;
			bool ishighlight = false;
			
			points.Each( delegate( ViewPoint vp, int index )
			{
				if ( GetPointData( vp, ref coord, ref color, ref ishighlight ) )
				{
					if( !ishighlight ) color *= exposure;
					coords.Add( coord );
					colors.Add( color );
					highlights.Add( ishighlight );
				}
			} );
			
			vpointsCache = new FlattenedViewable( coords.ToArray(), colors.ToArray(), 1 );
			return vpointsCache;
		}
		
		public FlattenedViewable GetFlattenedViewableEdges()
		{
			if ( vedgesCache != null ) return vedgesCache;
			
			int nMax = edges.Count * 2;
			List<Vec3f> coords = new List<Vec3f>( nMax );
			List<Vec3f> colors = new List<Vec3f>( nMax );
			List<bool> highlights = new List<bool>( nMax );
			
			Vec3f coord0 = Vec3f.Zero;
			Vec3f color0 = Vec3f.Zero;
			bool ishighlight0 = false;
			Vec3f coord1 = Vec3f.Zero;
			Vec3f color1 = Vec3f.Zero;
			bool ishighlight1 = false;
			
			edges.Each( delegate( ViewEdge ve, int i )
			{
				ViewPoint vp0 = points[ve.i1];
				ViewPoint vp1 = points[ve.i2];
				if ( GetPointData( vp0, ref coord0, ref color0, ref ishighlight0 ) && GetPointData( vp1, ref coord1, ref color1, ref ishighlight1 ) )
				{
					if( !ishighlight0 ) color0 *= exposure;
					if( !ishighlight1 ) color1 *= exposure;
					coords.Add( coord0 ); coords.Add( coord1 );
					colors.Add( color0 ); colors.Add( color1 );
					highlights.Add( ishighlight0 ); highlights.Add( ishighlight1 );
				}
			});
			
			vedgesCache = new FlattenedViewable( coords.ToArray(), colors.ToArray(), 2 );
			return vedgesCache;
		}
		
		// assumes that the flattened polygon array does not change
		// ex: unmasking / masking a polygon
		/*public void UpdateFlattenedViewablePolygon( int iPolygon )
		{
			ViewPoly vp = polygons[iPolygon];
			int icache = polygonCacheIndex[iPolygon];
			if( icache == -1 ) throw new Exception("Viewable.UpdateFlattenedViewablePolygon: Unimplemented feature: unmasking" );
			
			ViewPoint vp0 = points[vp.i1];
			ViewPoint vp1 = points[vp.i2];
			ViewPoint vp2 = points[vp.i3];
			if ( 
				GetPointData( vp0, ref coord0, ref color0, ref ishighlight0 ) &&
				GetPointData( vp1, ref coord1, ref color1, ref ishighlight1 ) &&
				GetPointData( vp2, ref coord2, ref color2, ref ishighlight2 ) )
			{
				vpolygonsCache.Verts
				coords.Add( coord0 ); coords.Add( coord1 ); coords.Add( coord2 );
				colors.Add( color0 ); colors.Add( color1 ); colors.Add( color2 );
				highlights.Add( ishighlight0 ); highlights.Add( ishighlight1 ); highlights.Add( ishighlight2 );
				if ( !ishighlight0 ) max = Vec3f.Max( max, color0 );
				if ( !ishighlight1 ) max = Vec3f.Max( max, color1 );
				if ( !ishighlight2 ) max = Vec3f.Max( max, color2 );
			}
		}*/
		
		public FlattenedViewable GetFlattenedViewablePolygons()
		{
			if ( vpolygonsCache != null ) return vpolygonsCache;
			
			int nMax = polygons.Count * 3;
			List<Vec3f> coords = new List<Vec3f>( nMax );
			List<Vec3f> colors = new List<Vec3f>( nMax );
			List<bool> highlights = new List<bool>( nMax );
			
			polygonCacheIndex = new int[nMax];
			
			Vec3f coord0 = Vec3f.Zero;
			Vec3f color0 = Vec3f.Zero;
			bool ishighlight0 = false;
			Vec3f coord1 = Vec3f.Zero;
			Vec3f color1 = Vec3f.Zero;
			bool ishighlight1 = false;
			Vec3f coord2 = Vec3f.Zero;
			Vec3f color2 = Vec3f.Zero;
			bool ishighlight2 = false;
			
			polygons.Each( delegate( ViewPoly vp, int i )
			{
				ViewPoint vp0 = points[vp.i1];
				ViewPoint vp1 = points[vp.i2];
				ViewPoint vp2 = points[vp.i3];
				if ( 
					GetPointData( vp0, ref coord0, ref color0, ref ishighlight0 ) &&
					GetPointData( vp1, ref coord1, ref color1, ref ishighlight1 ) &&
					GetPointData( vp2, ref coord2, ref color2, ref ishighlight2 ) )
				{
					if ( !ishighlight0 ) color0 *= exposure;
					if ( !ishighlight1 ) color1 *= exposure;
					if ( !ishighlight2 ) color2 *= exposure;
					polygonCacheIndex[i] = coords.Count();
					coords.Add( coord0 ); coords.Add( coord1 ); coords.Add( coord2 );
					colors.Add( color0 ); colors.Add( color1 ); colors.Add( color2 );
					highlights.Add( ishighlight0 ); highlights.Add( ishighlight1 ); highlights.Add( ishighlight2 );
				} else {
					polygonCacheIndex[i] = -1;
				}
			} );
			
			vpolygonsCache = new FlattenedViewable( coords.ToArray(), colors.ToArray(), 3 );
			return vpolygonsCache;
		}
		
		#endregion
		
		#region Get Properties for Point, Edge, Polygon
		
		public AppearanceData GetPointAppearance( int i ) { return points[i].appearance; }
		public Vec3f GetPointCoord( int i ) { return points[i].coord; }
		
		private List<ViewPoint> GetEdgePoints( int i ) { return GetEdgePoints( edges[i] ); }
		private List<ViewPoint> GetEdgePoints( ViewEdge e )
		{
			return new List<ViewPoint>( new ViewPoint[] { points[e.i1], points[e.i2] } );
		}
		public List<AppearanceData> GetEdgeAppearances( int i ) { return GetEdgeAppearances( edges[i] ); }
		private List<AppearanceData> GetEdgeAppearances( ViewEdge ve )
		{
			return new List<AppearanceData>( new AppearanceData[] { GetPointAppearance( ve.i1 ), GetPointAppearance( ve.i2 ) } );
		}
		public List<Vec3f> GetEdgeCoords( int i ) { return GetEdgeCoords( edges[i] ); }
		private List<Vec3f> GetEdgeCoords( ViewEdge ve )
		{
			return new List<Vec3f>( new Vec3f[] { GetPointCoord( ve.i1 ), GetPointCoord( ve.i2 ) } );
		}
		
		public List<ViewPoint> GetPolygonPoints( int i ) { return GetPolygonPoints( polygons[i] ); }
		private List<ViewPoint> GetPolygonPoints( ViewPoly p )
		{
			return new List<ViewPoint>( new ViewPoint[] { points[p.i1], points[p.i2], points[p.i3] } );
		}
		public List<AppearanceData> GetPolygonAppearances( int i ) { return GetPolygonAppearances( polygons[i] ); }
		private List<AppearanceData> GetPolygonAppearances( ViewPoly vp )
		{
			return new List<AppearanceData>( new AppearanceData[] { GetPointAppearance( vp.i1 ), GetPointAppearance( vp.i2 ), GetPointAppearance( vp.i3 ) } );
		}
		public List<Vec3f> GetPolygonCoords( int i ) { return GetPolygonCoords( polygons[i] ); }
		private List<Vec3f> GetPolygonCoords( ViewPoly vp )
		{
			return new List<Vec3f>( new Vec3f[] { GetPointCoord( vp.i1 ), GetPointCoord( vp.i2 ), GetPointCoord( vp.i3 ) } );
		}
		
		#endregion
		
		#region Update Min, Max, Center
		
		private void UpdateMinMax()
		{
			min = null; max = null;
			foreach ( ViewPoint vp in points )
			{
				if ( min == null ) { min = max = center = vp.coord; continue; }
				min = Vec3f.Min( (Vec3f) min, vp.coord );
				max = Vec3f.Max( (Vec3f) max, vp.coord );
			}
			center = ( min + max ) * 0.5f;
			//String sMin = "(" + ((Vec3f)min).x + ", " + ((Vec3f)min).y + ", " + ((Vec3f)min).z + ")";
			//String sMax = "(" + ((Vec3f)max).x + ", " + ((Vec3f)max).y + ", " + ((Vec3f)max).z + ")";
			//DebugWindow.WriteLine( "Bounding Box: " + sMin + " - " + sMax);
		}
		private void UpdateMinMax( ViewPoint np )
		{
			if ( min == null ) { min = max = center = np.coord; return; }
			min = Vec3f.Min( (Vec3f) min, np.coord );
			max = Vec3f.Max( (Vec3f) max, np.coord );
			center = ( min + max ) * 0.5f;
		}
		
		#endregion
		
		#region Constructors
		
		public Viewable() { }
		public Viewable( ViewPoint[] points, ViewEdge[] edges, ViewPoly[] polygons )
		{
			this.points = new List<ViewPoint>( points );
			
			if ( edges == null ) this.edges = new List<ViewEdge>();
			else this.edges = new List<ViewEdge>( edges );
			
			if ( polygons == null ) this.polygons = new List<ViewPoly>();
			else this.polygons = new List<ViewPoly>( polygons );
			
			fireDataChangedEvent();
		}
		public Viewable( List<ViewPoint> points, List<ViewEdge> edges, List<ViewPoly> polygons )
		{
			this.points = points; this.edges = edges; this.polygons = polygons;
			fireDataChangedEvent();
		}
		
		#endregion
		
	}
}
