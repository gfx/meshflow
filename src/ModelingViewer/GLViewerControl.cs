using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK.Graphics.OpenGL;
using OpenTK;
//using OpenTK.Graphics;
using Common.Libs.Image;
using Common.Libs.VMath;
using Common.Libs.MatrixMath;


namespace ModelingViewer
{
	public enum ViewerMouseMode
	{
		NONE,
		SELECT_POINT, SELECT_EDGE, SELECT_POLY,
		ROTATE_WORLD, ROTATE_CAMERA_AROUND_ORIGIN, ROTATE_CAMERA_AROUND_SELECTION,
		ZOOM,
		MOVE_CAMERA
	}
	
	public class GLViewer : GLControl
	{
		protected Vec3f cameraPosition = new Vec3f( 0, 0, 1 );
		private Vec3f cameraTarget = new Vec3f( 0, 0, 0 );
		private Vec3f cameraUp = Vec3f.Z;
		private bool cameraOrtho = true;						// not implemented
		
		private Vec3f worldTranslation = new Vec3f( 0, 0, 0 );	// not implemented
		private Vec3f worldRotation = new Vec3f( 0, 0, 0 );		// not implemented
		private float worldScale = 1.0f;
		
		private Matrix matModel;
		private Matrix matProjection;
		private int[] viewport;
		
		private System.Boolean renderNormalized = false;
		
		private System.Boolean renderAxis = true;
		private float axisLength = 1.0f;
		private float axisWidth = 1.0f;
		
		private System.Boolean renderGrid = true;
		private int gridCount = 20;
		private Color gridColor = Color.Gray;
		private Color gridFillColor = Color.Transparent;		// not implemented
		private float gridWidth = 0.5f;							// not implemented
		
		public GLViewer()
		{
			InitializeComponent();
		}
		
		protected override void OnPaint( PaintEventArgs e )
		{
			base.OnPaint( e );
			MakeCurrent();
			SetUpRender();

			RenderTheGrid();
			RenderTheAxis();

			//RenderViewObject( this.viewable );
			
			SwapBuffers();
		}

		private void SetUpRender()
		{
			GL.ClearColor( BackColor );
			GL.Enable( EnableCap.DepthTest );
			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );
			GL.Enable( EnableCap.Texture2D );
			GL.Hint( HintTarget.PerspectiveCorrectionHint, HintMode.Nicest );

			Matrix4 lookat = Matrix4.LookAt(
				cameraPosition.x, cameraPosition.y, cameraPosition.z,
				cameraTarget.x, cameraTarget.y, cameraTarget.z,
				cameraUp.x, cameraUp.y, cameraUp.z
				);

			GL.MatrixMode( MatrixMode.Projection );
			GL.LoadIdentity();
			double ratio = (double)this.Height / (double)this.Width;
			if ( cameraOrtho ) GL.Ortho( -1.0, 1.0, -ratio, ratio, 0.001, 1000.0 );
			else Perspective( 60.0f, (float)ratio, 0.01f, 1000.0f );
			
			GL.MatrixMode( MatrixMode.Modelview );
			GL.LoadIdentity();
			GL.Scale( worldScale, worldScale, worldScale );
			GL.MultMatrix( ref lookat );
			GL.Translate( worldTranslation.x, worldTranslation.y, worldTranslation.z );
			
			GL.Viewport( 0, 0, this.Width, this.Height );
			
			matModel = GetModelMatrix();
			matProjection = GetProjectionMatrix();
			viewport = GetViewport();
		}
		
		private Matrix4d DoubleArrayToMatrix4d( double[] m )
		{
			return new Matrix4d(
				m[00], m[01], m[02], m[03], m[04], m[05], m[06], m[07],
				m[08], m[09], m[10], m[11], m[12], m[13], m[14], m[15] );
		}
		private Matrix DoubleArrayToMatrix( double[] m )
		{
			return new Matrix( new double[,] { 
				{ m[00], m[04], m[08], m[12] },
				{ m[01], m[05], m[09], m[13] },
				{ m[02], m[06], m[10], m[14] },
				{ m[03], m[07], m[11], m[15] }
			} );
		}
		public Matrix GetModelMatrix()
		{
			double[] m = new double[16];
			GL.GetDouble( GetPName.ModelviewMatrix, m );
			return DoubleArrayToMatrix(m);
		}
		public Matrix GetProjectionMatrix()
		{
			double[] m = new double[16];
			GL.GetDouble( GetPName.ProjectionMatrix, m );
			return DoubleArrayToMatrix(m);
		}
		public int[] GetViewport()
		{
			int[] viewport = new int[4];
			GL.GetInteger( GetPName.Viewport, viewport );
			return viewport;
		}
		
		protected List<Vec3f> GetUnprojected( int mousex, int mousey )
		{
			float x = (float) ( mousex - viewport[0] ) / (float) viewport[2] * 2.0f - 1.0f;
			float y = (float) ( mousey - viewport[1] ) / (float) viewport[3] * -2.0f + 1.0f;
			
			Matrix inv = Matrix.Invert( matProjection * matModel );
			
			Matrix mnear0 = new Matrix( new double[] { x, y, 0.0, 1.0 }, true );
			Matrix mfar0 = new Matrix( new double[] { x, y, 1.0, 1.0 }, true );
			
			Matrix mnear = inv * mnear0; mnear /= mnear[3,0];
			Matrix mfar = inv * mfar0; mfar /= mfar[3,0];
			
			Vec3f near = new Vec3f( (float) mnear[0,0], (float) mnear[1,0], (float) mnear[2,0] );
			Vec3f far = new Vec3f( (float) mfar[0,0], (float) mfar[1,0], (float) mfar[2,0] );
			
			return new List<Vec3f>( new Vec3f[] { near, far } );
		}
		
		// from: http://www.opentk.com/node/1276
		// assumes source is in [0,1] range for x,y,z
		private Vec3f Unproject( Vec3f source, Matrix matProject, Matrix matModel )
		{
			//Matrix.Invert
			Matrix matrix = Matrix.Invert( matModel * matProject );
			Matrix s = new Matrix( new double[,] { { source.x, source.y, source.z, 1.0 } } ).TransposeSelf();
			Matrix target = matrix * s;
			//target /= target[3,0];
			return new Vec3f( (float) target[0,0], (float) target[1,0], (float) target[2,0] );
			//Vector4d v = Vector4d.Transform( s, matrix );
			//return new Vector3d( v.X / v.W, v.Y / v.W, v.Z / v.W );
		}
		
		//protected Vec3f

		// does not work :(
		private void Perspective( float fov, float aspect, float znear, float zfar )
		{
			float t = FMath.Tan( (fov * FMath.PI / 180.0f) / 2.0f );
			float a = t * aspect;
			float b = 1 / t;
			float c = ( znear + zfar ) / ( znear - zfar );
			float d = ( 2 * znear * zfar ) / ( znear - zfar );
			float[] m = new float[] {
				a, 0, 0, 0,
				0, b, 0, 0,
				0, 0, c, -1,
				0, 0, d, 0
			};
			//GL.MatrixMode( MatrixMode.Projection );
			GL.MultMatrix( m );
		}

		private void RenderTheAxis()
		{
			if ( !renderAxis ) return;
			
			//GL.Disable( EnableCap.DepthTest );
			
			GL.PushMatrix();
			GL.Translate( -worldTranslation.x, -worldTranslation.y, -worldTranslation.z );
			
			GL.LineWidth( axisWidth );
			GL.Begin( BeginMode.Lines );
			GL.Color3( 1.0f, 0.0f, 0.0f ); GL.Vertex3( 0.0f, 0.0f, 0.0f ); GL.Vertex3( axisLength, 0.0f, 0.0f );
			GL.Color3( 0.0f, 1.0f, 0.0f ); GL.Vertex3( 0.0f, 0.0f, 0.0f ); GL.Vertex3( 0.0f, axisLength, 0.0f );
			GL.Color3( 0.0f, 0.0f, 1.0f ); GL.Vertex3( 0.0f, 0.0f, 0.0f ); GL.Vertex3( 0.0f, 0.0f, axisLength );
			GL.End();
			
			GL.PopMatrix();

			//GL.Enable( EnableCap.DepthTest );
		}

		private void RenderTheGrid()
		{
			if ( !renderGrid ) return;

			GL.Disable( EnableCap.DepthTest );

			GL.LineWidth( gridWidth );
			GL.Color3( gridColor );
			GL.Begin( BeginMode.Lines );
			for ( int i = -gridCount; i <= gridCount; i++ )
			{
				float f = (float) i / (float) gridCount;
				GL.Vertex3( f * axisLength, -axisLength, 0.0f );
				GL.Vertex3( f * axisLength, axisLength, 0.0f );
				GL.Vertex3( -axisLength, f * axisLength, 0.0f );
				GL.Vertex3( axisLength, f * axisLength, 0.0f );
			}
			GL.End();

			GL.Enable( EnableCap.DepthTest );
		}
		
		private void InitializeComponent()
		{
			this.SuspendLayout();
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Black;
			this.Name = "GLViewer";
			this.Cursor = Cursors.Cross;
			this.ResumeLayout( false );
		}
	}
}

