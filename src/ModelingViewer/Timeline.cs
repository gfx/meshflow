using System;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Timers;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Common.Libs.VMath;
using Common.Libs.MiscFunctions;
using Common.Libs.Image;
using Common.Libs.MatrixMath;

namespace ModelingViewer
{
	public enum TimelineModes
	{
		Global, Local
	}
	public class Timeline : Control
	{
		public static Timeline timeline;
		
		protected bool Initialized = false;
		
		protected ModelingHistory history;
		protected int i0min, indmin;
		protected int i0max, indmax;
		
		protected int intervalcount;
		
		protected Property<TimelineModes> mode = new Property<TimelineModes>( "Mode", TimelineModes.Global );
		
		// note: these are LoD0-based, so they'll have to be mapped into current LoD
		public PropertyBag ViewWindowProperties;
		protected Property<int> i0WindowMin;
		protected Property<int> i0WindowMax;
		protected Property<int> i0Current;
		protected Property<int> tickInterval;
		
		protected Brush brBackground = new SolidBrush( Color.FromArgb( 0, 0, 0 ) );
		protected Brush brTimeline = new SolidBrush( Color.FromArgb( 128, 128, 128 ) );
		protected Brush brWindow = new SolidBrush( Color.FromArgb( 255, 255, 255 ) );
		protected Brush brTicks = new SolidBrush( Color.FromArgb( 192, 192, 192 ) );
		protected Pen pnThumbTick = new Pen( Color.FromArgb( 255, 255, 255 ) );
		protected Brush brFiltered = new SolidBrush( Color.FromArgb( 64, 64, 64 ) );
		protected Brush brCurrentTime = new SolidBrush( Color.FromArgb( 128, 255, 255, 0 ) );
		protected Brush brLoD = new SolidBrush( Color.FromArgb( 255, 255, 255 ) );
		protected Brush brLoDDark = new SolidBrush( Color.FromArgb( 64, 64, 64 ) );
		
		public PropertyBool RenderOptDrawTicks = new PropertyBool( "Draw Ticks", false );
		
		private bool redrawcache = true;
		private Bitmap bmpCached = null;
		private Graphics gCached = null;
		
		Clustering clustering;
		List<Cluster> lstClusters;
		protected bool[] isfiltered;
		float xleft, xright, xspan, xmult;
		int indsub, indspan, indend;
		
		private enum MouseStates
		{
			Nothing, Scrubbing, MovingWindowStart, MovingWindowEnd, MovingWindow
		};
		private int lastmx;
		private int lastmy;
		private MouseStates mousestate = MouseStates.Nothing;
		
		public delegate void Index0CurrentChangedHandler( bool click );
		public event Index0CurrentChangedHandler Index0CurrentChanged;
		public void FireIndex0CurrentChangedHandler() { if( Index0CurrentChanged != null ) Index0CurrentChanged( mousestate != Timeline.MouseStates.Nothing ); }
		
		public Timeline( ModelingHistory history )
		{
			this.history = history;
			this.i0min = 0;
			this.i0max = history.SnapshotCount - 1;
			this.indmin = 0;
			this.i0WindowMin = new PropertyConstrainedInt( "Window Min", i0min, i0min, i0min ); //i0max
			this.i0WindowMax = new PropertyConstrainedInt( "Window Max", i0max, i0min, i0max ); //i0min
			this.i0Current = new PropertyConstrainedInt( "Current Time", i0WindowMin.Val, i0WindowMin.Val, i0WindowMax.Val );
			this.tickInterval = new PropertyConstrainedInt( "Tick Interval", 100, 1, int.MaxValue );
			
			this.intervalcount = Composition.GetPreset( CompositionPresets.Intervals ).Intervals_Count;
			
			CacheFiltered();
			
			mode.PropertyChanged += delegate { SetupViewMapping(); RefreshControl(); };
			i0Current.PropertyChanged += delegate {
				UpdateConstraints();
				RefreshControl();
				FireIndex0CurrentChangedHandler();
			};
			i0WindowMin.PropertyChanged += delegate {
				UpdateConstraints();
				SetupViewMapping();
				RefreshControl();
			};
			i0WindowMax.PropertyChanged += delegate {
				UpdateConstraints();
				SetupViewMapping();
				RefreshControl();
			};
			history.CurrentLevel.PropertyChanged += delegate {
				CacheFiltered();
				SetupViewMapping();
				RefreshControl();
				FireIndex0CurrentChangedHandler();
			};
			history.Filters.Reevaluated += delegate {
				CacheFiltered();
				SetupViewMapping();
				RefreshControl();
				FireIndex0CurrentChangedHandler();
			};
			tickInterval.PropertyChanged += delegate { redrawcache = true; RefreshControl(); };
			//history.Filters.Reevaluated += delegate { CacheFiltered(); RefreshControl(); };
			
			ViewWindowProperties = new PropertyBag( i0WindowMin, i0WindowMax );
		}
		
		public void ToggleMode()
		{
			switch( mode.Val ) {
			case TimelineModes.Global: mode.Set( TimelineModes.Local ); tickInterval.Set( 1 ); break;
			case TimelineModes.Local: mode.Set( TimelineModes.Global ); tickInterval.Set( 100 ); break;
			default: throw new Exception( "Unhandled mode!" );
			}
		}
		
		public Cluster GetCurrentCluster()
		{
			int ind = SnapIndex0ToUnfilteredCluster(i0Current.Val);
			if( ind == -1 ) return lstClusters[indmax];
			return lstClusters[ind];
		}
		
		public Cluster GetViewWindowCluster()
		{
			return new Cluster( i0WindowMin.Val, i0WindowMax.Val, "Intervals", Composition.GetPreset( CompositionPresets.Intervals ), GetViewWindowIntervals() );
		}
		
		public int[] GetViewWindowIntervals()
		{
			FilteringSet filtering = history.Filters;
			List<Cluster> lst = new List<Cluster>( lstClusters );
			
			List<int> indavailable = new List<int>();
			int firstind = clustering.GetClusterIndex( i0WindowMin.Val );
			int lastind = clustering.GetClusterIndex( i0WindowMax.Val );
			for( int ind = firstind; ind <= lastind; ind++ )
			{
				if( isfiltered[ind] ) continue; // filtering.IsFiltered( lst[ind] ) ) continue;
				indavailable.Add( ind );
			}
			
			int t = indavailable.Count;
			
			if( t == 0 ) return new int[] { clustering.GetClusterIndex( i0Current.Val ) };
			
			int c = Math.Min( t, intervalcount );
			int[] snapshots = new int[c];
			
			for( int i = 0; i < c; i++ )
			{
				int i2 = (int)((float)i * (float)(t-1) / (float)(c-1));
				int ind = indavailable[ Math.Max( 0, i2 ) ];
				snapshots[i] = lst[ind].start;
			}
			
			return snapshots;
		}
		
		public Clustering GetCurrentLayer() { return clustering; }
		
		public int GetIndex0ViewWindowStart() { return i0WindowMin.Val; }
		public int GetIndex0ViewWindowEnd() { return i0WindowMax.Val; }
		public int GetIndex0Current() { return i0Current.Val; }
		
		public void CurrentToFirst()
		{
			int ind = SnapIndex0ToUnfilteredCluster( i0WindowMin.Val, 1 );
			if( ind == -1 ) return;
			i0Current.Set( lstClusters[ind].start );
		}
		
		public void CurrentToLast()
		{
			int ind = SnapIndex0ToUnfilteredCluster( i0WindowMax.Val, -1 );
			if( ind == -1 ) return;
			i0Current.Set( lstClusters[ind].start );
		}
		
		public void CurrentToNext()
		{
			int ind = clustering.GetClusterIndex( i0Current.Val );
			ind = SnapIndex0ToUnfilteredCluster( lstClusters[ind].end + 1, 1 );
			if( ind == -1 ) return;
			i0Current.Set( lstClusters[ind].start );
		}
		
		public void CurrentToPrev()
		{
			int ind = clustering.GetClusterIndex( i0Current.Val );
			ind = SnapIndex0ToUnfilteredCluster( lstClusters[ind].start - 1, -1 );
			if( ind == -1 ) return;
			i0Current.Set( lstClusters[ind].start );
		}
		
		public void InitializeControl()
		{
			this.CreateHandle();
			
			this.DoubleBuffered = true;
			this.SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true );
			this.MinimumSize = new Size( 50, 50 );
			
			SetupCache();
			
			Initialized = true;
		}
		
		private void CacheFiltered()
		{
			clustering = history.CurrentLevel.Val;
			lstClusters = clustering.GetClusters();
			indmax = lstClusters.Count - 1;
			isfiltered = new bool[lstClusters.Count];
			lstClusters.EachInParallel( (Cluster cluster, int ind) => isfiltered[ind] = history.Filters.IsFiltered( cluster ) );
			//for( int i = indmin; i <= indmax; i++ ) isfiltered[i] = history.Filters.IsFiltered( lstClusters[i] );
			redrawcache = true;
		}
		
		// sets up the caching bitmap for rendering clusters
		// assuming that the control is going to be refreshed (not forcing a refresh here)
		private void SetupCache()
		{
			bmpCached = new Bitmap( Width, Height );
			gCached = Graphics.FromImage( bmpCached );
			gCached.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
			gCached.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighSpeed;
			gCached.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
			SetupViewMapping();
			redrawcache = true;
		}
		
		private void UpdateConstraints()
		{
			((PropertyConstrained<int>)i0WindowMin).Maximum = i0Current.Val;
			((PropertyConstrained<int>)i0WindowMax).Minimum = i0Current.Val;
			((PropertyConstrained<int>)i0Current).Maximum = i0WindowMax.Val;
			((PropertyConstrained<int>)i0Current).Minimum = i0WindowMin.Val;
		}
		
		// sets up the mapping from cluster index to control
		private void SetupViewMapping()
		{
			xleft = 40;
			xright = Width - 20;
			xspan = xright - xleft + 1;
			
			if( mode.Val == TimelineModes.Global ) {
				indsub = 0;
				indend = indmax - 1;
			} else {
				indsub = MapIndex0ToClusterIndex( i0WindowMin.Val );
				indend = ClampClusterIndex( MapIndex0ToClusterIndex( i0WindowMax.Val ) );
			}
			indspan = indend - indsub + 1;
			
			xmult = xspan / (float)indspan;
			
			redrawcache = true;
		}
		
		private void RefreshControl()
		{
			if( !Initialized ) return;
			// invalidate the control (done in control's thread)
			this.Invoke( (MethodInvoker) delegate { Invalidate(); } );
		}
		
		protected override void OnResize( EventArgs e )
		{
			SetupCache();
			base.OnResize( e );
		}
		
		private Bitmap GetBackgroundBitmap()
		{
			if( !redrawcache && bmpCached != null ) return bmpCached;
			if( bmpCached == null ) SetupCache();
			
			gCached.FillRectangle( brBackground, 0, 0, Width, Height );
			
			// draw view window
			int indwindowmin = MapIndex0ToClusterIndex(i0WindowMin.Val);
			int indwindowmax = MapIndex0ToClusterIndex(i0WindowMax.Val);
			int i0start = lstClusters[indwindowmin].start;
			int i0end = lstClusters[indwindowmax].end;
			if( indwindowmax < indmax ) i0end = lstClusters[indwindowmax+1].start;
			//RenderIndex0Block( gCached, brWindow, lstClusters[indwindowmin].start, lstClusters[indwindowmax].end, 21 );
			RenderIndex0Block( gCached, brWindow, i0start, i0end, 21 );
			
			// draw thumbnail ticks
			int[] snapshots = GetViewWindowIntervals();
			//int lasti = -1, count = 0;
			foreach( int isnapshot in snapshots )
			{
				//if( lasti == isnapshot ) { count++; } else { lasti = isnapshot; count = 0; }
				RenderIndex0ThumbTick( gCached, pnThumbTick, isnapshot, 25 ); // + count );
			}
			
			// draw timeline
			RenderIndex0Block( gCached, brTimeline, i0min, i0max, 15 );
			
			// draw darkened filtered clusters
				for( int i = indmin; i <= indmax; i++ )
					if( isfiltered[i] ) RenderClusterBlock( gCached, brFiltered, i, 15 );
			
			// draw ticks
			if( RenderOptDrawTicks )
			{
				int indstart = MapControlToClusterIndex( 0 ); indstart -= indstart % tickInterval.Val;
				int indend = MapControlToClusterIndex( Width );
				for( int i = indstart; i <= indend; i+= tickInterval.Val )
				{
					RenderIndex0Block( gCached, brTicks, lstClusters[i].start, lstClusters[i].start, 3 );
				}
			}
			
			// draw LoD
			gCached.FillRectangle( brBackground, 0, 0, 20, Height );
			int c = history.CurrentClusteringLayerLevel_Get();
			RenderCenteredString( gCached, "" + c, brLoD, 10, Height / 2 );
			if( clustering.Above != null )
				RenderCenteredString( gCached, "" + (c+1), brLoDDark, 10, Height / 2 - 13 );
			if( clustering.Below != null )
				RenderCenteredString( gCached, "" + (c-1), brLoDDark, 10, Height / 2 + 13 );
			
			redrawcache = false;
			return bmpCached;
		}
		
		protected override void OnPaint( PaintEventArgs e )
		{
			Graphics g = e.Graphics;
			Bitmap back = null;
			
			long timeback = Timer.GetExecutionTime_ms( delegate {
				back = GetBackgroundBitmap();
			} );
			//System.Console.WriteLine( "back: " + timeback );
			
			g.DrawImageUnscaled( back, 0, 0 );
			
			int ind = SnapIndex0ToUnfilteredCluster( i0Current.Val );
			RenderClusterBlock( g, brCurrentTime, ind, 25 );
			
			//RenderIndex0Block( g, brCurrentTime, i0currentsnap, i0currentsnap + 1, 25 );
			
		}
		
		/*protected override void OnKeyPress( System.Windows.Forms.KeyPressEventArgs e )
		{
			if( e.KeyChar == '\t' ) { ToggleMode(); e.Handled = true; }
			else base.OnKeyPress( e );
		}*/
		
		protected override bool ProcessCmdKey (ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;
			
			if( ( msg.Msg == WM_KEYDOWN ) || ( msg.Msg == WM_SYSKEYDOWN ) )
			{
				switch( keyData ) {
				case Keys.Tab:
					ToggleMode();
					return true;
				}
			}
			return base.ProcessCmdKey( ref msg, keyData );
		}
		
		protected override void OnMouseDown( MouseEventArgs e )
		{
			//if( e.Button == MouseButtons.Middle ) { ToggleMode(); }
			
			if( e.Button == MouseButtons.Left ) {
				//i0Current.Set( MapControlToIndex0_ClusterStart( e.X ) );
				mousestate = Timeline.MouseStates.Scrubbing;
			}
			
			if( e.Button == MouseButtons.Right ) {
				//float winleft = MapIndex0ToControl( i0WindowMin.Val );
				//float winright = MapIndex0ToControl( i0WindowMax.Val );
				//float winwidth = winright - winleft + 1;
				//float winfirstthird = winleft + winwidth * 1.0f / 3.0f;
				//float winsecondthird = winleft + winwidth * 2.0f / 3.0f;
				//float winhalf = winleft + winwidth * 1.0f / 2.0f;
				
				//if( e.X < winfirstthird ) mousestate = Timeline.MouseStates.MovingWindowStart;
				//else if( e.X > winsecondthird ) mousestate = Timeline.MouseStates.MovingWindowEnd;
				//else mousestate = Timeline.MouseStates.MovingWindow;
				
				//if( e.X < winhalf ) mousestate = Timeline.MouseStates.MovingWindowStart;
				//else mousestate = Timeline.MouseStates.MovingWindowEnd;
				
				if( e.X < MapIndex0ToControl( i0Current.Val ) ) mousestate = Timeline.MouseStates.MovingWindowStart;
				else mousestate = Timeline.MouseStates.MovingWindowEnd;
				
				ViewWindowProperties.DeferPropertyChanged = true;
			}
			
			OnMouseMove( e );
		}
		
		protected override void OnMouseUp( MouseEventArgs e )
		{
			mousestate = Timeline.MouseStates.Nothing;
			OnMouseMove( e );
			
			ViewWindowProperties.DeferPropertyChanged = false;
		}
		
		private bool handlingmousemove = false;
		protected override void OnMouseMove( MouseEventArgs e )
		{
			if( handlingmousemove ) return;
			handlingmousemove = true;
			
			int dx = e.X - lastmx;
			int dy = e.Y - lastmy;
			lastmx = e.X;
			lastmy = e.Y;
			int ind;
			
			switch( mousestate ) {
				
			case MouseStates.Nothing:
				break;
				
			case MouseStates.Scrubbing:
				i0Current.Set( MapControlToIndex0_ClusterStart( e.X ) );
				break;
				
			case MouseStates.MovingWindow:
				// TODO: if in global mode, make jump
				if( dx < 0 ) {
					int oldx = i0WindowMin.Val;
					int oldind = MapIndex0ToClusterIndex( oldx );
					ind = ClampClusterIndex( oldind + dx );
					int dind = ind - oldind;
					int oldi0cur = i0Current.Val;
					
					i0WindowMin.SetWithoutEvent( lstClusters[ind].start );
					i0Current.SetWithoutEvent( lstClusters[MapIndex0ToClusterIndex( oldi0cur ) + dind].start );
					i0WindowMax.SetWithoutEvent( lstClusters[MapIndex0ToClusterIndex( i0WindowMax.Val ) + dind].end );
				} else {
					int oldx = i0WindowMax.Val;
					int oldind = MapIndex0ToClusterIndex( oldx );
					ind = ClampClusterIndex( oldind + dx );
					int dind = ind - oldind;
					int oldi0cur = i0Current.Val;
					
					i0WindowMax.SetWithoutEvent( lstClusters[ind].start );
					i0Current.SetWithoutEvent( lstClusters[MapIndex0ToClusterIndex( oldi0cur ) + dind].start );
					i0WindowMin.SetWithoutEvent( lstClusters[MapIndex0ToClusterIndex( i0WindowMin.Val ) + dind].end );
				}
				
				UpdateConstraints();
				SetupViewMapping();
				RefreshControl();
				FireIndex0CurrentChangedHandler();
				break;
				
			case MouseStates.MovingWindowStart:
				if( mode.Val == TimelineModes.Global ) {
					i0WindowMin.Set( MapControlToIndex0_ClusterStart( e.X ) );
				} else {
					ind = ClampClusterIndex( MapIndex0ToClusterIndex( i0WindowMin.Val ) + dx );
					i0WindowMin.Set( lstClusters[ind].start );
				}
				break;
				
			case MouseStates.MovingWindowEnd:
				if( mode.Val == TimelineModes.Global ) {
					i0WindowMax.Set( MapControlToIndex0_ClusterEnd( e.X ) );
				} else {
					ind = ClampClusterIndex( MapIndex0ToClusterIndex( i0WindowMax.Val ) + dx );
					i0WindowMax.Set( lstClusters[ind].start );
				}
				break;
				
			default: throw new Exception( "Unimplemented MouseState " + mousestate );
			}
			
			// TODO: wrap mouse?
			
			handlingmousemove = false;
		}
		
		protected override void OnMouseDoubleClick( MouseEventArgs e )
		{
			if( clustering.Below == null ) return;
			
			if( e.Button == MouseButtons.Right )
			{
				i0WindowMin.Set( i0min );
				i0WindowMax.Set( i0max );
				return;
			}
			
			if( e.Button == MouseButtons.Left )
			{
				int indmin = MapIndex0ToClusterIndex( i0Current.Val );
				int indmax = ClampClusterIndex( MapIndex0ToClusterIndex( i0Current.Val ) + 1 );
				i0WindowMin.DeferPropertyChanged = true;
				i0WindowMin.Set( lstClusters[indmin].start );
				i0WindowMax.Set( lstClusters[indmax].start );
				i0WindowMin.DeferPropertyChanged = false;
				history.CurrentLevel.Set( clustering.Below );
			}
		}
		
		protected override void OnMouseWheel( MouseEventArgs e )
		{
			if( e.Delta > 0 ) {
				history.CurrentClusteringLayerLevel_Up();
				//if( clustering.Above != null ) history.CurrentLevel.Set( clustering.Above );
			} else if( e.Delta < 0 ) {
				history.CurrentClusteringLayerLevel_Down();
				//if( clustering.Below != null ) history.CurrentLevel.Set( clustering.Below );
			}
		}
		
		protected void RenderCenteredString( Graphics g, String st, Brush brush, int x, int y )
		{
			SizeF s = g.MeasureString( st, this.Font );
			int l = x - (int)s.Width / 2;
			int t = y - (int)s.Height / 2;
			g.DrawString( st, this.Font, brush, l, t );
			
		}
		
		protected void RenderClusterBlock( Graphics g, Brush brush, int ind, int height )
		{
			int xl = (int)MapClusterIndexToControl( ind );
			int xr = (int)MapClusterIndexToControl( ind+1 ); // gets clamped????
			int t = ( Height - height ) / 2;
			int width = xr - xl + 1;
			g.FillRectangle( brush, xl, t, width, height );
		}
		protected void RenderIndex0Block( Graphics g, Brush brush, int i0Left, int i0Right, int height )
		{
			int xl = (int)MapIndex0ToControl( i0Left );
			int xr = (int)MapIndex0ToControl( i0Right );
			int t = ( Height - height ) / 2;
			int width = xr - xl + 1;
			if( xl + width < 0 || xl > Width ) return;
			g.FillRectangle( brush, xl, t, width, height );
		}
		
		protected void RenderIndex0ThumbTick( Graphics g, Pen pen, int i0, int height )
		{
			int i0snap0 = SnapIndex0ToClusterIndex0Start( i0 );
			int i0snap1 = SnapIndex0ToNextClusterIndex0Start( i0 );
			int x0 = (int)MapIndex0ToControl( i0snap0 );
			int x1 = (int)MapIndex0ToControl( i0snap1 );
			int x = ( x0 + x1 ) / 2;
			int xl = x - 2;
			int xr = x + 2;
			int y = ( Height - height ) / 2;
			int yl = y - 2;
			int yr = y - 2;
			
			g.DrawLine( pen, xl, yl, x, y );
			g.DrawLine( pen, x, y, xr, yr );
		}
		
		public int ClampClusterIndex( int ind ) { return Math.Max( indmin, Math.Min( lstClusters.Count - 1, ind ) ); }	//indmax
		public int ClampIndex0( int i0 ) { return Math.Max( i0min, Math.Min( i0max, i0 ) ); }
		
		public int SnapIndex0ToClusterIndex0Start( int i0 )
		{
			return clustering.GetCluster( ClampIndex0( i0 ) ).start;
		}
		
		public int SnapIndex0ToClusterIndex0End( int i0 )
		{
			return clustering.GetCluster( ClampIndex0( i0 ) ).end;
		}
		
		public int SnapIndex0ToNextClusterIndex0Start( int i0 )
		{
			int ind = clustering.GetClusterIndex( ClampIndex0( i0 ) );
			return lstClusters[ ClampClusterIndex( ind + 1 ) ].start;
		}
		
		public int SnapIndex0ToUnfilteredCluster( int i0 )
		{
			int i0l = SnapIndex0ToUnfilteredCluster( i0, -1 );
			int i0g = SnapIndex0ToUnfilteredCluster( i0, 1 );
			if( i0l == -1 ) return i0g;
			if( i0g == -1 || i0 - i0l < i0g - i0 ) return i0l;
			return i0g;
		}
		
		public int SnapIndex0ToUnfilteredCluster( int i0, int delta )
		{
			int ind = MapIndex0ToClusterIndex( i0 );
			for( ; ind >= indmin && ind <= indmax && isfiltered[ind]; ind += delta );
			if( ind < indmin || ind > indmax ) return -1;
			return ind;
		}
		
		public float MapIndex0ToControl( int i0 ) {
			return MapClusterIndexToControl( MapIndex0ToClusterIndex( ClampIndex0( i0 ) ) );
		}
		public int MapIndex0ToClusterIndex( int i0 )
		{
			return clustering.GetClusterIndex( ClampIndex0( i0 ) );
		}
		public float MapClusterIndexToControl( int ind )
		{
			return (float)( ClampClusterIndex( ind ) - indsub ) * xmult + xleft;
		}
		public int MapControlToClusterIndex( float x )
		{
			return ClampClusterIndex( (int)((x - xleft) / xmult) + indsub );
		}
		public int MapControlToIndex0_ClusterStart( float x )
		{
			int ind = ClampClusterIndex( MapControlToClusterIndex( x ) );
			return lstClusters[ind].start;
		}
		public int MapControlToIndex0_ClusterEnd( float x )
		{
			int ind = ClampClusterIndex( MapControlToClusterIndex( x ) );
			return lstClusters[ind].end;
		}
	}
}

