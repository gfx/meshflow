using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Common.Libs.MiscFunctions;

namespace ModelingViewer
{
	public partial class ScrubBar : Control
	{
		private ModelingHistory hist;
		private ClusteringLayers layers;
		private FilteringSet filters;
		private int level = 0;
		private Clustering curlayer;
		private int index0 = 0;
		private int index0max = 0;
		//private float fval = 0;
		
		private int timelineheight = 20;
		
		public ModelViewerControl mvcViewer;
		
		private bool selection = false;
		private int index0selstart;
		private int index0selend;
		private int disti0start;
		private int disti0end;
		
		public delegate void Index0ChangedHandler( bool click );
		public event Index0ChangedHandler Index0Changed;
		public void FireIndex0ChangedHandler() { if( Index0Changed != null ) Index0Changed( mousestate != ScrubBar.MouseStates.Nothing ); }
		
		public delegate void LevelChangedHandler();
		public event LevelChangedHandler LevelChanged;
		public void FireLevelChangedHandler() { if( LevelChanged != null ) LevelChanged(); }
		
		#region ScrubBar Properties
		
		public int Index0 {
			get { return index0; }
			set {
				bool snap = true;
				int ind = value;
				
				bool useselection = UseSelection();
				
				if( useselection ) {
					ind = Math.Max( index0selstart, Math.Min( index0selend, ind ) );
				} else {
					ind = Math.Max( 0, Math.Min( index0max, ind ) );
				}
				
				if( snap ) ind = curlayer.GetCluster( ind ).start;
				
				index0 = ind;
				
				FireIndex0ChangedHandler();
				RefreshControl();
			}
		}
		
		public int Level {
			get { return level; }
			set {
				level = Math.Max( 0, Math.Min( layers.nlevels - 1, value ) );
				curlayer = layers.GetClusteringLayer( level );
				//curlayer.FillViewables();
				Index0 = index0;
				FireLevelChangedHandler();
				refilter = true; rerender = true;
				RefreshControl();
			}
		}
		
		public Clustering CurrentLayer { get { return curlayer; } }
		
		#endregion
		
		public ScrubBar( ModelingHistory history )
		{
			this.hist = history;
			this.layers = hist.Layers;
			this.filters = hist.Filters;
			this.index0max = hist.SnapshotCount - 1;
			
			layers.Reevaluated += delegate { rerender = true; RefreshControl(); };
			filters.Reevaluated += delegate { refilter = true; rerender = true; RefreshControl(); };
			
			RefreshTimer.Elapsed += delegate { RefreshControl(); };
			
			index0 = 0;
			level = layers.nlevels - 1;
			curlayer = layers.GetClusteringLayer( level );
			//curlayer.FillViewables();
			
			InitializeControl();
			
			index0center = index0max / 2;
			zoom = (float)Width / (float)index0max;
			
			mvcViewer = new ModelViewerControl( history );
			mvcViewer.Dock = DockStyle.Top;
			mvcViewer.Height = Height - timelineheight;
			mvcViewer.RenderOptSeparateViewports.Set( true );
			//mvcViewer.viewablelod0 = 5;
			mvcViewer.RenderOptClusterInfo.Set( false );
			mvcViewer.RenderOptGrid.Set( false );
			mvcViewer.RenderOptSelectedVerts.Set( false );
			mvcViewer.RenderOptOrigin.Set( false );
			mvcViewer.RenderOptTransparentForeground.Set( false );
			mvcViewer.RenderOptSkipReordering.Set( true );
			mvcViewer.RenderOptRotationGizmoOpt.Set( false );
			Controls.Add( mvcViewer );
		}
		
		protected override void OnResize (EventArgs e)
		{
			base.OnResize( e );
			rerender = true;
			if( mvcViewer != null ) mvcViewer.Height = Height - timelineheight;
		}
		
		#region Index Functions
		
		public void MoveFirstIndex( bool skipfiltered ) {
			index0 = ( UseSelection() ? index0selstart : 0 );
			DeltaIndex( 1, skipfiltered, false );
		}
		
		public void MoveLastIndex( bool skipfiltered ) {
			index0 = ( UseSelection() ? index0selend : index0max );
			DeltaIndex( -1, skipfiltered, false );
		}
		
		public void MoveNextIndex( bool skipfiltered ) { DeltaIndex( 1, skipfiltered, true ); }
		
		public void MovePrevIndex( bool skipfiltered ) { DeltaIndex( -1, skipfiltered, true ); }
		
		private void DeltaIndex( int deltai, bool skipfiltered, bool movefirst )
		{
			bool[] filtered = GetFiltered();
			
			bool usingselection = UseSelection();
			int min = ( usingselection ? index0selstart : 0 );
			int max = ( usingselection ? index0selend : index0max );
			
			int i = index0;
			
			if( i < min ) { i = min; deltai = 1; movefirst = false; }
			if( i > max ) { i = max; deltai = -1; movefirst = false; }
			
			if( deltai == 0 ) { Index0 = i; return; }
			
			do {
				if( movefirst ) {
					if( i + deltai < min || i + deltai > max ) return;
					if( deltai < 0 ) i = curlayer.GetCluster( i - 1 ).start;
					else i = curlayer.GetCluster( i ).end + 1;
				}
				else movefirst = true;
			} while( skipfiltered && filtered[i] );
			
			Index0 = i;
		}
		
		#endregion
		
		public Cluster GetCurrentCluster() { return curlayer.GetCluster( index0 ); }
		
		private List<Cluster> GetFilteredClusters()
		{
			if( refilter || filteredclusters == null )
			{
				List<Cluster> clusters = curlayer.GetClusters();
				filteredclusters = filters.FilteredClusters( clusters );
				//bool[] isfiltered = filters.Filter( clusters );
				List<bool> isfiltered = new List<bool>();
				clusters.Each( delegate( Cluster cluster ) { isfiltered.AddRange( Enumerable.Repeat( filters.IsFiltered( cluster ), cluster.duration ) ); } );
				filtered = isfiltered.ToArray();
				
				refilter = false;
			}
			return filteredclusters;
		}
		
		private bool[] GetFiltered()
		{
			GetFilteredClusters();
			return filtered;
		}
		
		private List<List<Cluster>> SplitByNames( List<Cluster> clusters )
		{
			List<string> dnames = clusters.Select( (Cluster cluster) => cluster.name.SplitOnce('.')[0] ).Distinct().ToList();
			return dnames.Select( (string name) => clusters.Where( (Cluster cluster) => ( cluster.name.SplitOnce('.')[0] == name ) ).ToList() ).ToList();
		}
		
		#region Conversion Functions
		
		private int Index0ToControl( int i ) { return (int)((float)(this.Width-1) * (float)i / (float)index0max); }
		private int ControlToIndex0( int x ) { return (int)((float)index0max * (float)x / (float)(this.Width-1)); }
		
		private int[] ClusterToControl( Cluster cluster ) { return new int[] { Index0ToControl( cluster.start ), Index0ToControl( cluster.end ) }; }
		private void ClusterToControl( Cluster cluster, out int x0, out int x1 ) { x0 = Index0ToControl( cluster.start ); x1 = Index0ToControl( cluster.end ); }
		
		private int[] LevelToControlY( int level )
		{
			int y0, y1; LevelToControlY( level, out y0, out y1 ); return new int[] { y0, y1 };
		}
		private void LevelToControlY( int level, out int y0, out int y1 )
		{
			int levels = ( rendercollapsedlayers ? 1 : layers.nlevels );
			int l = ( rendercollapsedlayers ? 0 : level );
			
			float nestedmult = ( rendercollapsedlayers && rendernested ? (float)(level + 1) / (float)layers.nlevels : 1.0f );
			
			float bandh = (float)this.Height * 0.80f * nestedmult;
			float bandtop = ((float)this.Height - bandh) * 0.5f;
			float layerheight = bandh / (float)levels;
			float layertop = (float)((levels - 1) - l) * layerheight + bandtop;
			float layerbottom = layertop + layerheight;
			
			y0 = (int) layertop;
			y1 = (int) layerbottom;
			
			//float h = (float)this.Height;
			//float ymax = h * 0.80f;
			//float ytop = ( h - ymax ) / 2.0f;
			//float layerheight = ymax / (float)levels;
			//float lf = (float)((levels - 1) - level);
			//y0 = (int)(ytop + layerheight * lf);
			//y1 = (int)(ytop + layerheight * (lf + 1.0f) );
		}
		
		/*private int ControlToVal( int x ) { return (int)((float) x * (float)maxval / (float)this.Width ); }
		private int ControlToIndex( int x ) { return ValToIndex( ControlToVal( x ) ); }
		private int IndexStartToControl( int i ) { return ValToControl( root.starts[i] ); }
		private int IndexEndToControl( int i ) { return ValToControl( root.ends[i] ); }
		private int IndexStartToVal( int i ) { return root.starts[i]; }
		private int IndexEndToVal( int i ) { return root.ends[i]; }
		private int ValToControl( int v ) { return (int)((float) v * (float)this.Width / (float)maxval ); }
		private int ValToIndex( int v )
		{
			if( v < 0 ) return 0;
			for( int i = 0; i < maxindex - 1; i++ ) if( root.starts[i] <= v && v <= root.ends[i] ) return i;
			return maxindex;
		}*/
		
		#endregion
	}
}

