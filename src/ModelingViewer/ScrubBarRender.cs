using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public partial class ScrubBar : Control
	{
		private Brush brBackground = new SolidBrush( Color.Black );
		private Brush brTickLines = new SolidBrush( Color.Gray );
		private Brush brIndexMarker = new SolidBrush( Color.FromArgb( 128, 255, 255, 0 ) );
		private Brush brIndexMarkerDark = new SolidBrush( Color.FromArgb( 32, 255, 255, 0 ) );
		private Brush brSelection = new SolidBrush( Color.FromArgb( 128, 192, 128, 0 ) );
		private Brush brLevelDivider = new SolidBrush( Color.FromArgb( 64, 255, 255, 255 ) );
		private Brush brLevel = new SolidBrush( Color.FromArgb( 0, 0, 0 ) );
		private Brush brLevelDarken = new SolidBrush( Color.FromArgb( 128, 0, 0, 0 ) );
		private Brush brLevelDisabled = new SolidBrush( Color.FromArgb( 192, 64, 64, 64 ) );
		private Brush brClusterDarken = new SolidBrush( Color.FromArgb( 192, 0, 0, 0 ) );
		private Brush brFiltered = new SolidBrush( Color.FromArgb( 64, 64, 64 ) );
		
		public object RenderLock = new object();
		public bool IsRendering { get; private set; }
		protected System.Timers.Timer RefreshTimer = new System.Timers.Timer( 5 );
		
		private bool rendertimeline = true;
		
		private bool rendercollapsedlayers = false;
		private bool rendernested = false;
		private bool rendersplitnames = false;
		private bool renderfilteredclusters = false;
		private bool renderfiltereddarken = false;
		
		private Image bgimage = null;
		private List<Cluster> filteredclusters = null;
		private bool[] xfiltered = null;
		private bool[] filtered = null;
		private bool rerender = true;
		private bool refilter = true;
		
		private int index0center;
		private float zoom;
		
		private int laststart = -1;
		private int lastend = -1;
		
		public void RefreshControl()
		{
			lock( RenderLock )
			{
				if( IsRendering || !this.IsHandleCreated ) { RefreshTimer.Enabled = true; return; }
				RefreshTimer.Enabled = false;
				IsRendering = true;
			}
			this.Invoke((MethodInvoker) delegate { Invalidate(); } );
		}
		
		private Image GetBGImage()
		{
			if( rerender || bgimage == null || bgimage.Width != this.Width || bgimage.Height != this.Height )
			{
				rerender = true;
				
				bgimage = new Bitmap( Width, Height );
				Graphics g = Graphics.FromImage( bgimage );
				Pen penLevelDivider = new Pen( brLevelDivider );
				
				g.FillRectangle( brBackground, 0, 0, Width, Height );
				
				// draw 1/50th tick marks
				//Pen penTickLine = new Pen( brTickLines );
				//for( int vp = 0; vp <= 50; vp++ ) DrawTick( (float)vp / 50.0f, 1.00f, penTickLine, g );
				
				List<List<Cluster>> lstLayers;
				List<List<List<Cluster>>> lstLayersSplit;
				
				if( rendercollapsedlayers && !rendernested ) lstLayers = new List<List<Cluster>>() { curlayer.GetClusters() };
				else lstLayers = layers.GetClusteringLayers().Select( (Clustering clustering) => clustering.clusters ).ToList();
				
				if( !renderfilteredclusters )
				{
					//lstLayers = lstLayers.Select( (List<Cluster> lstclusters) => lstclusters.Where( (Cluster cluster) => filteredclusters.All( (Cluster fcluster) => !( cluster.start.Within( fcluster.start, fcluster.end ) && cluster.end.Within( fcluster.start, fcluster.end ) ) ) ).ToList() ).ToList();
					lstLayers = lstLayers.Select( (List<Cluster> lstclusters) => lstclusters.Where( (Cluster cluster) => !filters.IsFiltered( cluster ) ).ToList() ).ToList();
				}
				
				if( rendersplitnames ) lstLayersSplit = lstLayers.Select( ( List<Cluster> clusters ) => SplitByNames( clusters ) ).ToList();
				else lstLayersSplit = lstLayers.Select( (List<Cluster> clusters) => new List<List<Cluster>>() { clusters } ).ToList();
				
				lstLayersSplit.Reverse();
				lstLayersSplit.Each( delegate( List<List<Cluster>> lstlstClusters, int ilevelrev ) {
					int ilevel = ( layers.nlevels - 1 ) - ilevelrev;
					int y0, y1; LevelToControlY( ilevel, out y0, out y1 );
					RenderClusters( g, lstlstClusters, y0, y1, ilevel, brLevel, penLevelDivider );
				} );
				
				FillXFiltered();
			}
			
			return bgimage;
		}
		
		private void FillXFiltered()
		{
			if( !rerender ) return;
			
			List<Cluster> filteredclusters = GetFilteredClusters();
			xfiltered = new bool[Width];
			filteredclusters.Each( delegate( Cluster cluster, int i ) {
				int x0 = (int)FMath.Clamp( Index0ToControl( cluster.start ), 0, Width - 1 );
				int x1 = (int)FMath.Clamp( Index0ToControl( cluster.end ), 0, Width - 1 );
				for( int x = x0; x <= x1; x++ ) xfiltered[x] = true;
			} );
			
			rerender = false;
		}
		
		private void RenderClusters( Graphics g, List<List<Cluster>> lstClustersList, int ytop, int ybottom, int ilevel, Brush brBack, Pen penTopBottom )
		{
			int y0 = ytop, y1 = ybottom, yh = y1 - y0 + 1;
			
			int count = lstClustersList.Count;
			float yh__ = (float)(yh - 8) / (float)count;
			
			if( !(rendercollapsedlayers && rendernested) || ilevel == (layers.nlevels - 1) )
			{
				g.FillRectangle( brBack, 0, ytop, this.Width, ybottom - ytop + 1 );
				g.DrawLine( penTopBottom, 0, ytop, this.Width, ytop );
				g.DrawLine( penTopBottom, 0, ybottom, this.Width, ybottom );
			}
			
			lstClustersList.Each( delegate( List<Cluster> lstclusters, int ilc ) {
				int y0_ = (y0+4) + (int)((float)ilc * yh__);
				lstclusters.Each( delegate( Cluster cluster, int ic ) {
					int x0 = Index0ToControl( cluster.start );
					int x1 = Index0ToControl( cluster.end );
					//g.FillRectangle( cluster.brush, x0, y0 + 4, (x1-x0+1), yh - 8 );
					g.FillRectangle( cluster.brush, x0, y0_, (x1-x0+1), (int)yh__ - 1 );
					g.FillRectangle( brClusterDarken, x0 + 1, y0_ + 1, (x1-x0+1)-2, (int)yh__ - 1 - 2 );
				} );
			} );
		}
		
		protected override void OnPaint( PaintEventArgs e )
		{
			Graphics g = e.Graphics;
			Pen penIndexMarkerDark = new Pen( brIndexMarkerDark );
			Pen penTimeLine = new Pen( Color.FromArgb( 64, 64, 64 ) );
			Pen penTickMajor = new Pen( Color.FromArgb( 255, 255, 255 ) );
			Pen penTickMinor = new Pen( Color.FromArgb( 128, 128, 128 ) );
			
			mvcViewer.Visible = rendertimeline;
			
			if( rendertimeline ) {
				g.Clear( Color.Black );
				int h = Height - timelineheight / 2;
				
				if( renderfiltereddarken )
				{
					FillXFiltered();
					Pen penFiltered = new Pen( Color.FromArgb( 255, 64, 64, 64 ) );
					for( int x = 0; x < Width; x++ ) if( xfiltered[x] )
						g.DrawLine( penFiltered, x, 0, x, Height );
				}
				
				g.DrawLine( penTimeLine, 0, h, Width, h );
				
				List<Cluster> clusters = curlayer.GetClusters();
				clusters.OrderBy( (Cluster cluster) => cluster.start );
				
				int c = 0;
				for( int i = 0; i < clusters.Count; i++ )
				{
					int[] xs = ClusterToControl( clusters[i] );
					
					if( !renderfilteredclusters && filters.IsFiltered( clusters[i] ) ) continue;
					
					if( xs[1] - xs[0] > 2 ) {
						g.DrawLine( penTickMinor, xs[0], h-1, xs[0], h+1 );
						g.DrawLine( penTickMinor, xs[1], h-1, xs[1], h+1 );
					}
					
					c++;
					if( c < 10 ) continue;
					c = 0;
					
					g.DrawLine( penTickMajor, xs[0], h-2, xs[0], h+2 );
				}
				
				int st, en;
				if( selection ) { st = index0selstart; en = index0selend; }
				else { st = 0; en = index0max; }
				if( laststart != st || lastend != en )
				{
					Cluster tlcluster = new Cluster( Math.Min( st, en ), Math.Max( st, en ), "timesteps", null );
					tlcluster.composition.SetToPreset( "intervals" );
					mvcViewer.SetCluster( curlayer, tlcluster );
					laststart = st;
					lastend = en;
				}
				
			} else {
				g.DrawImageUnscaled( GetBGImage(), 0, 0 );
				
				if( renderfiltereddarken )
				{
					Pen penFiltered = new Pen( Color.FromArgb( 192, 0, 0, 0 ) );
					for( int x = 0; x < Width; x++ ) if( xfiltered[x] )
						g.DrawLine( penFiltered, x, 0, x, Height );
				}
				
				if( !rendercollapsedlayers )
				{
					for( int l = 0; l < layers.nlevels; l++ )
					{
						if( l == level ) continue;
						int[] ys = LevelToControlY( l );
						int y0 = ys[0], y1 = ys[1], yh = y1 - y0 + 1;
						if( l > level )
							g.FillRectangle( brLevelDisabled, 0, y0 + 1, this.Width, yh - 2 );
						else
							g.FillRectangle( brLevelDarken, 0, y0 + 1, this.Width, yh - 2 );
					}
				}
			}
			
			/*List<Cluster> filteredclusters = GetFilteredClusters();
			filteredclusters.Each( delegate( Cluster cluster ) {
				int x0 = Index0ToControl( cluster.start );
				int x1 = Index0ToControl( cluster.end );
				g.FillRectangle( brFiltered, x0, 0, x1-x0+1, this.Height );
			} );*/
			
			if( selection ) DrawBar( index0selstart, index0selend, 1.0f, brSelection, g );
			
			Cluster cl = GetCurrentCluster();
			int markerx0 = Index0ToControl( cl.start );
			int markerx1 = Index0ToControl( cl.end );
			g.FillRectangle( brIndexMarker, markerx0, 0, (markerx1 - markerx0 + 1), this.Height );
			g.DrawLine( penIndexMarkerDark, markerx0-1, 0, markerx0-1, this.Height );
			g.DrawLine( penIndexMarkerDark, markerx1+1, 0, markerx1+1, this.Height );
			
			IsRendering = false;
		}
		
		#region Drawing Functions
		
		private void DrawBar( int i0, int i1, float perHeight, Brush brush, Graphics g )
		{
			if( i1 < i0 ) { int t = i0; i0 = i1; i1 = t; }
			
			int x0 = Index0ToControl( i0 );
			int x1 = Index0ToControl( i1 );
			int h2 = this.Height / 2;
			int h = (int)( (float)h2 * perHeight );
			int y0 = h2 - h;
			int y1 = h2 + h;
			g.FillRectangle( brush, x0, y0, x1-x0+1, y1-y0 );
		}
		
		private void DrawTick( float perIndex0, float perHeight, Pen pen, Graphics g )
		{
			DrawTick( (int)((float)index0max * perIndex0), perHeight, pen, g );
		}
		
		private void DrawTick( int i, float perHeight, Pen pen, Graphics g )
		{
			int x = Index0ToControl( i );
			int h = (int)( (float)this.Height * perHeight );
			int y0 = (this.Height - h) / 2;
			int y1 = (this.Height + h) / 2;
			g.DrawLine( pen, x, y0, x, y1 );
		}
		
		private void DrawHorizontal( float perY, Pen pen, Graphics g )
		{
		}
		
		#endregion
		
	}
}

