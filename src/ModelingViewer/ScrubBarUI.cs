using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public partial class ScrubBar : Control
	{
		private enum MouseStates
		{
			Nothing, Scrubbing, Selecting, AdjustSelectionStart, AdjustSelectionEnd, AdjustSelection
		};
		
		private MouseStates mousestate = ScrubBar.MouseStates.Nothing;
		
		private int prevmx;
		private int prevmy;
		
		private bool bShiftPressed;
		private bool bControlPressed;
		private bool bAltPressed;
		
		private int fine = 0;
		private int finereset = 9;
		private int finethresh = 19;
		
		private void InitializeControl()
		{
			this.MinimumSize = new Size( 0, 100 );
			this.DoubleBuffered = true;
			this.SetStyle( ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw, true );
			
			this.ContextMenu = new ContextMenu( new MenuItem[] {
				new MenuItem( "Cluster", new MenuItem[] {
					new MenuItem( "Add from Selection", delegate {
						if( !selection ) { MessageBox.Show( "Make a selection first" ); return; }
						if( !(curlayer is ClusteringCustom) ) { MessageBox.Show( "Select a Custom layer first" ); return; }
						ClusteringCustom custom = (ClusteringCustom) curlayer;
						Cluster cluster = new Cluster( index0selstart, index0selend, "new", new Composition() ) { brush = new SolidBrush( Color.WhiteSmoke ) };
						try { custom.TestIfCanAdd( cluster ); custom.ClusterAdd( cluster ); }
						catch( Exception e ) { MessageBox.Show( "Cannot add cluster: " + e.Message ); }
						selection = false;
					} ),
				} ),
				new MenuItem( "Layer", new MenuItem[] {
					new MenuItem( "Add above current", new MenuItem[] {
						new MenuItem( "Custom", delegate {
							level = layers.AddLayer( level + 1, new ClusteringCustom( hist, "Custom" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
					} ),
					new MenuItem( "Add to Top", new MenuItem[] {
						new MenuItem( "Custom", delegate {
							level = layers.AddLayer( new ClusteringCustom( hist, "Custom" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Undone", delegate {
							level = layers.AddLayer( new ClusteringUndos( hist ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Repeats", delegate {
							level = layers.AddLayer( new ClusteringPredicate_Repeats( hist ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Views", delegate {
							level = layers.AddLayer( new ClusteringPredicate_Views( hist ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Selects", delegate {
							string[] ignore = ModelingHistory.ViewCommands.ToList().AddReturn( ModelingHistory.VisibilityCommands ).AddReturn( "undo.undo" ).ToArray();
							string[] selection = ModelingHistory.SelectionCommands.ToList().AddReturn( "select" ).ToArray();
							SolidBrush brush = new SolidBrush( Color.FromArgb( 0, 128, 0 ) );
							level = layers.AddLayer( new ClusteringPredicate_RepeatsWithIgnorable( "select", brush, Composition.GetPreset( CompositionPresets.Default ), selection, ignore, hist, "Cluster Repeated Selections" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Transforms", delegate {
							string[] ignore = new List<string>().AddReturn( ModelingHistory.ViewCommands, ModelingHistory.VisibilityCommands, ModelingHistory.SelectionCommands ).AddReturn( "undo.undo", "select" ).ToArray();
							string[] repcmds = ModelingHistory.TransformCommands.ToList().AddReturn( "transform" ).ToArray();
							SolidBrush brush = new SolidBrush( Color.FromArgb( 0, 255, 255 ) );
							level = layers.AddLayer( new ClusteringPredicate_RepeatsWithIgnorable( "transform", brush, Composition.GetPreset( CompositionPresets.Default ), repcmds, ignore, hist, "Cluster Repeated Transformations" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Extrudes", delegate {
							string[] ignore = new List<string>().AddReturn( ModelingHistory.ViewCommands, ModelingHistory.VisibilityCommands, ModelingHistory.SelectionCommands ).AddReturn( "undo.undo", "select" ).ToArray();
							string[] repcmds = new string[] { "topo.extrude" };
							SolidBrush brush = new SolidBrush( Color.FromArgb( 255, 192, 0 ) );
							level = layers.AddLayer( new ClusteringPredicate_RepeatsWithIgnorable( "topo.extrude", brush, Composition.GetPreset( CompositionPresets.Extrude ), repcmds, ignore, hist, "Cluster Repeated Extrudes" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Select,Transforms", delegate {
							string[] startcmds = ModelingHistory.SelectionCommands.ToList().AddReturn( "select" ).ToArray();
							string[] endcmds = ModelingHistory.TransformCommands.ToList().AddReturn( "transform" ).ToArray();
							string[] ignorecmds = ModelingHistory.ViewCommands.ToList().AddReturn( ModelingHistory.VisibilityCommands ).AddReturn( "undo.undo" ).ToArray();
							SolidBrush brush = new SolidBrush( Color.FromArgb( 0, 255, 255 ) );
							level = layers.AddLayer( new ClusteringPredicate_PairWithIgnorable( "transform", brush, Composition.GetPreset( CompositionPresets.Default ), startcmds, endcmds, ignorecmds, true, true, true, hist, "Cluster Select+Transform" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Select,Extrudes", delegate {
							string[] startcmds = ModelingHistory.SelectionCommands.ToList().AddReturn( "select" ).ToArray();
							string[] endcmds = new string[] { "topo.extrude" };
							string[] ignorecmds = ModelingHistory.ViewCommands.ToList().AddReturn( ModelingHistory.VisibilityCommands ).AddReturn( "undo.undo" ).ToArray();
							SolidBrush brush = new SolidBrush( Color.FromArgb( 255, 192, 0 ) );
							level = layers.AddLayer( new ClusteringPredicate_PairWithIgnorable( "topo.extrude", brush, Composition.GetPreset( CompositionPresets.Extrude ), startcmds, endcmds, ignorecmds, true, true, true, hist, "Cluster Select+Extrude" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
						new MenuItem( "Cluster Extrude,Transforms", delegate {
							string[] startcmds = new string[] { "topo.extrude" };
							string[] endcmds = ModelingHistory.TransformCommands.ToList().AddReturn( "transform" ).ToArray();
							string[] ignorecmds = new List<string>().AddReturn( ModelingHistory.ViewCommands, ModelingHistory.VisibilityCommands, ModelingHistory.SelectionCommands ).AddReturn( "undo.undo", "select" ).ToArray();
							SolidBrush brush = new SolidBrush( Color.FromArgb( 255, 192, 0 ) );
							level = layers.AddLayer( new ClusteringPredicate_PairWithIgnorable( "topo.extrude", brush, Composition.GetPreset( CompositionPresets.Extrude ), startcmds, endcmds, ignorecmds, true, true, true, hist, "Cluster Repeated Extrude+Transform" ) );
							curlayer = layers.GetClusteringLayer( level );
						} ),
					} ),
					new MenuItem( "Remove", new MenuItem[] {
						new MenuItem( "Top Layer", delegate {
							if( layers.nlevels == 1 ) { MessageBox.Show( "Cannot delete Level 0 layer" ); return; }
							if( level == layers.nlevels - 1 ) { level--; curlayer = curlayer.Below; }
							layers.RemoveLevel();
						} ),
						new MenuItem( "Current Layer", delegate {
							if( level == 0 ) { MessageBox.Show( "Cannot delete Level 0 layer" ); return; }
							if( level == layers.nlevels - 1 ) { level--; curlayer = layers.GetClusteringLayer( level ); layers.RemoveLevel(); }
							else { curlayer = curlayer.Above; layers.RemoveLevel( level ); }
						} ),
					} ),
				} ),
				new MenuItem( "Options", new MenuItem[] {
					new MenuItem( "Toggle Darken Filtered Clusters", delegate {
						renderfiltereddarken = !renderfiltereddarken;
						rerender = true;
						RefreshControl();
					} ),
				} ),
				new MenuItem( "Save", delegate {
					using( FileStream fs = new FileStream( "layers", FileMode.OpenOrCreate ) )
					{
						BinaryWriter bw = new BinaryWriter( fs );
						layers.WriteBinary( bw );
					}
				} ),
				new MenuItem( "Load", delegate {
					using( FileStream fs = new FileStream( "layers", FileMode.Open ) )
					{
						BinaryReader br = new BinaryReader( fs );
						layers = ClusteringLayers.ReadBinary( br, hist );
						layers.Reevaluated += delegate { rerender = true; RefreshControl(); };
						level = layers.nlevels - 1;
						curlayer = layers.GetClusteringLayer( level );
						rerender = true;
						RefreshControl();
					}
				} ),
			} );
			
			if( ModelingHistory.DEBUGMODE ) ContextMenu.MenuItems.AddRange( new MenuItem[] {
				new MenuItem( "-" ),
				new MenuItem( "Debug", new MenuItem[] {
					new MenuItem( "Check Integrity", delegate { layers.Debug_CheckIntegrity(); } ),
					new MenuItem( "Options", new MenuItem[] {
						new MenuItem( "Toggle Timeline Only", delegate {
							rendertimeline = !rendertimeline;
							rerender = true;
							RefreshControl();
						} ),
						new MenuItem( "Toggle Collapsed Layers", delegate {
							rendercollapsedlayers = !rendercollapsedlayers;
							rerender = true;
							RefreshControl();
						} ),
						new MenuItem( "Toggle Nested Layers", delegate {
							rendernested = !rendernested;
							rerender = true;
							RefreshControl();
						} ),
						new MenuItem( "Toggle Split on Names", delegate {
							rendersplitnames = !rendersplitnames;
							rerender = true;
							RefreshControl();
						} ),
						new MenuItem( "Toggle Render Filtered Clusters", delegate {
							renderfilteredclusters = !renderfilteredclusters;
							rerender = true;
							RefreshControl();
						} ),
					} ),
				} ),
			} );
		}
		
		private bool UseSelection()
		{
			bool useselection = selection;
			
			useselection &= (mousestate != ScrubBar.MouseStates.Selecting);
			useselection &= (mousestate != ScrubBar.MouseStates.AdjustSelectionStart);
			useselection &= (mousestate != ScrubBar.MouseStates.AdjustSelectionEnd);
			useselection &= (mousestate != ScrubBar.MouseStates.AdjustSelection);
			
			return useselection;
		}
		
		private void CheckKeyStates()
		{
			bControlPressed = ((Control.ModifierKeys & Keys.Control) != 0);
			bShiftPressed = ((Control.ModifierKeys & Keys.Shift) != 0);
			bAltPressed = ((Control.ModifierKeys & Keys.Alt) != 0);
		}
		
		protected override void OnKeyDown( KeyEventArgs e ) { CheckKeyStates(); }
		protected override void OnKeyUp( KeyEventArgs e ) { CheckKeyStates(); }
		
		protected override void OnMouseWheel (MouseEventArgs e)
		{
			if( mvcViewer.Visible && e.Y < mvcViewer.Height ) return;
			int s = Math.Sign( e.Delta ); if( s > 0 ) selection = false; Level = level + s;
		}
		
		protected override void OnMouseDown( MouseEventArgs e )
		{
			CheckKeyStates();
			
			if( mousestate != ScrubBar.MouseStates.Nothing ) return;
			if( e.Button != MouseButtons.Left ) return;
			
			// mode selection
			
			if( bControlPressed ) {
				int i0s = Index0ToControl( index0selstart );
				int i0e = Index0ToControl( index0selend );
				if( selection ) {
					if( Math.Abs( e.X - i0s ) <= 2 ) mousestate = ScrubBar.MouseStates.AdjustSelectionStart;
					else if( Math.Abs( e.X - i0e ) <= 2 ) mousestate = ScrubBar.MouseStates.AdjustSelectionEnd;
					else if( i0s < e.X && e.X < i0e ) mousestate = ScrubBar.MouseStates.AdjustSelection;
					else { selection = false; mousestate = ScrubBar.MouseStates.Selecting; }
				} else mousestate = ScrubBar.MouseStates.Selecting;
			} else mousestate = ScrubBar.MouseStates.Scrubbing;
			
			// initialization of mode
			fine = finereset;
			
			switch( mousestate )
			{
			
			case MouseStates.Scrubbing:
				if( !bShiftPressed ) {
					int index0new = ControlToIndex0( e.X );
					if( index0new < index0selstart || index0new > index0selend ) selection = false;
					index0 = index0new;
					DeltaIndex( -1, true, false );
				} else {
				}
				break;
			
			case MouseStates.Selecting:
				if( !bShiftPressed ) {
					Index0 = ControlToIndex0( e.X );
					index0selstart = index0;
					index0selend = curlayer.GetCluster( index0 ).end;
					selection = true;
				} else {
					index0selstart = index0;
					index0selend = curlayer.GetCluster( index0 ).end;
					selection = true;
				}
				break;
			
			case MouseStates.AdjustSelection:
				if( !bShiftPressed ) {
					Index0 = ControlToIndex0( e.X );
				} else {
				}
				disti0start = index0 - index0selstart;
				disti0end = index0selend - index0;
				break;
			
			case MouseStates.AdjustSelectionStart:
				if( !bShiftPressed ) {
					Index0 = ControlToIndex0( e.X );
					index0selstart = index0;
				} else {
					index0 = index0selstart;
				}
				break;
			
			case MouseStates.AdjustSelectionEnd:
				if( !bShiftPressed ) {
					Index0 = ControlToIndex0( e.X );
					index0selend = index0;
				} else {
					index0 = index0selend;
				}
				break;
			
			}
		}
		
		protected override void OnMouseUp( MouseEventArgs e )
		{
			mousestate = ScrubBar.MouseStates.Nothing;
			if( index0selend < index0selstart ) { int t = index0selstart; index0selstart = index0selend; index0selend = t; }
		}
		
		protected override void OnMouseMove( MouseEventArgs e )
		{
			int dx = e.X - prevmx; int dxsign = Math.Sign( dx );
			int dy = e.Y - prevmy; int dysign = Math.Sign( dy );
			prevmx = e.X;
			prevmy = e.Y;
			
			CheckKeyStates();
			
			int i0s = Index0ToControl( index0selstart );
			int i0e = Index0ToControl( index0selend );
			if( selection ) {
				if( mousestate == ScrubBar.MouseStates.AdjustSelectionStart || mousestate == ScrubBar.MouseStates.AdjustSelectionEnd ) this.Cursor = Cursors.SizeWE;
				else if( mousestate == ScrubBar.MouseStates.AdjustSelection ) this.Cursor = Cursors.Hand;
				else if( mousestate == ScrubBar.MouseStates.Nothing ) {
					if( Math.Abs( e.X - i0s ) <= 2 || Math.Abs( e.X - i0e ) <= 2 ) this.Cursor = Cursors.SizeWE;
					else if( i0s < e.X && e.X < i0e )this.Cursor = Cursors.Hand;
					else this.Cursor = Cursors.Arrow;
				} else this.Cursor = Cursors.Arrow;
			} else this.Cursor = Cursors.Arrow;
			
			if( mousestate == ScrubBar.MouseStates.Nothing ) return;
			
			if( e.Button == MouseButtons.None )
			{
				mousestate = ScrubBar.MouseStates.Nothing;
				return;
			}
			
			if( mousestate == ScrubBar.MouseStates.Scrubbing && bControlPressed )
			{
				selection = true;
				index0selstart = index0selend = index0;
				mousestate = ScrubBar.MouseStates.Selecting;
				return;
			}
			
			if( bShiftPressed ) ScrubFine( dx );
			else {
				if( dxsign != 0 ) {
					index0 = ControlToIndex0( e.X );
					DeltaIndex( dxsign, true, false );
				}
				fine = finereset;
			}
			
			switch( mousestate )
			{
			
			case MouseStates.Nothing:
			case MouseStates.Scrubbing:
				break;
			
			case MouseStates.Selecting:
			case MouseStates.AdjustSelectionEnd:
				if( index0 < index0selstart ) index0selend = curlayer.GetCluster( index0 ).start;
				else index0selend = curlayer.GetCluster( index0 ).end;
				break;
			
			case MouseStates.AdjustSelectionStart:
				if( index0 < index0selend ) index0selstart = curlayer.GetCluster( index0 ).start;
				else index0selstart = curlayer.GetCluster( index0 ).end;
				break;
			
			case MouseStates.AdjustSelection:
				int sels = Math.Max( 0, index0 - disti0start );
				int sele = Math.Min( index0max, index0 + disti0end );
				index0selstart = curlayer.GetCluster( sels ).start;
				index0selend = curlayer.GetCluster( sele ).end;
				break;
			
			default: throw new Exception( "Unimplemented: " + mousestate );
			
			}
			
		}
		
		private void ScrubFine( int dx )
		{
			Point topleft = PointToScreen( new Point( 0, 0 ) );
			Point bottomright = PointToScreen( new Point( Width - 1, Height - 1 ) );
			
			fine += dx;
			int deltai = (int)FMath.Floor((float)fine / (float)finethresh);
			fine = ( ( fine % finethresh ) + finethresh ) % finethresh;
			while( deltai != 0 )
			{
				DeltaIndex( Math.Sign( deltai ), true, true );
				deltai += -Math.Sign( deltai );
			}
			
			//fval = Math.Max( 0.0f, Math.Min( (float)index0max, fval + (float) dx / 10.0f ) );
			//index0 = (int) fval;
			//int i0 = index0;
			//DeltaIndex( Math.Sign( dx ), true, false );
			//fval += index0 - i0;
			
			// wrap mouse!
			if( Cursor.Position.X < topleft.X ) {
				Cursor.Position = new Point( Cursor.Position.X + this.Width, Cursor.Position.Y );
				prevmx = Cursor.Position.X - topleft.X;
			}
			if( Cursor.Position.X > bottomright.X ) {
				Cursor.Position = new Point( Cursor.Position.X - this.Width, Cursor.Position.Y );
				prevmx = Cursor.Position.X - topleft.X;
			}
			
		}
		
	}
}

