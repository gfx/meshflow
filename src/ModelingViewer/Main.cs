using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

using OpenTK;
//using OpenTK.Graphics;
using ColorFormat = OpenTK.Graphics.ColorFormat;

using Common.Libs.VMath;
using Common.Libs.MiscFunctions;
using Common.Libs.ParallelFunctions;

namespace ModelingViewer
{
	public class MainClass
	{
		private static string StepsFilename = null;
		private static string TVMeshFilename = null;
		
		private static bool AutoFilterUndos = true;
		private static bool AutoFilterGUICommands = true;
		private static bool AutoFilterViewCommands = true;
		
		
		[MTAThread]
		static void Main( string[] args )
		{
			ModelingHistory hist;
			Version v = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
			Common.Libs.ParallelFunctions.Parallel.ThreadCount = Environment.ProcessorCount; //Math.Max( 1, (int)( (float)Environment.ProcessorCount * 0.75f ) );
			
			System.Console.WriteLine( "ModelingViewer " + v.ToString() );
			System.Console.WriteLine( "--------------------------------------------" );
			
			CommandHandler cmdhandler = GetArgumentCommandHandler();
			
			try {
				if( !cmdhandler.ParseCommands( args ) ) return;
			} catch( CommandException e ) {
				System.Console.WriteLine( "Command line error: " + e.Message );
				return;
			}
			
			if( StepsFilename == null && TVMeshFilename == null )
			{
				System.Console.WriteLine( "Must specify either StepsFilename or TVMeshFilename" );
				return;
			}
			
			if( ViewerControl.TestGraphicsMode() == null )
			{
				System.Console.WriteLine( "Cannot create OpenGL context as specified" );
				return;
			}
			
			if( StepsFilename != null ) {
				System.Console.WriteLine( "Loading and Streaming from {0}...", StepsFilename );
				hist = new ModelingHistory( StepsFilename );
				if( TVMeshFilename != null ) {
					System.Console.WriteLine( "Writing binary to {0}...", TVMeshFilename );
					//MiscFileIO.SaveObjectToBinary( TVMeshFilename, hist );
					using( FileStream fs = new FileStream( TVMeshFilename, FileMode.Create ) )
						new BinaryWriter( fs ).WriteT( hist );
				}
				return;
			} else {
				System.Console.WriteLine( "Loading binary from {0}...", TVMeshFilename );
				//hist = (ModelingHistory) MiscFileIO.ReadObjectFromBinary( TVMeshFilename );
				using( FileStream fs = new FileStream( TVMeshFilename, FileMode.Open ) )
				{
					BinaryReader br = new BinaryReader( fs );
					br.Read( out hist );
					//hist = ModelingHistory.ReadBinaryFile( br );
				}
				if( hist == null )
				{
					System.Console.WriteLine( "Could not deserialize file" );
					return;
				}
				System.Console.WriteLine( "done" );
			}
			
			if( AutoFilterUndos || AutoFilterGUICommands || AutoFilterViewCommands )
			{
				//ModelTreeRoot root = hist.Unfilter();
				//if( AutoFilterUndos ) root = hist.FilterUndos( root );
				//if( AutoFilterGUICommands ) root = hist.FilterGUIOps( root );
				//if( AutoFilterViewCommands ) root = hist.FilterViewOps( root );
				//hist.PermanentlyAlter( root );
			}
			
			//ModelingHistory.history = hist;
			Timeline.timeline = new Timeline( hist );
			
			hist.AddDefaultClusterLayers();
			
			ViewerWindow vw = new ViewerWindow( hist );
			//vw.ShowDialog();
			Application.Run( vw );
		}
		
		public static CommandHandler GetArgumentCommandHandler()
		{
			return new CommandHandler()
			{
				sHelp = "help",
				sCommandDelimiter = "--",
				sArgumentDelimiter = " ",
				DisplayHelpFunction = delegate( string txt )
				{
					System.Console.WriteLine( txt );
					return false;
				},
				CommandRequiresArgumentsFunction = delegate( string sCmd )
				{
					System.Console.WriteLine( "Command requires argument(s): " + sCmd );
					return false;
				},
				CommandRequiresNoArgumentsFunction = delegate( string sCmd )
				{
					System.Console.WriteLine( "Command requires no argument(s): " + sCmd );
					return false;
				},
				UnknownCommandFunction = delegate( String sCmd )
				{
					System.Console.WriteLine( "Unknown command: " + sCmd );
					return false;
				},
				Commands = new List<Command>() {
					new Command() {
						sCommand = "stepsfile",
						sDescription = "specifies the (master) file containing history of steps",
						IsRequired = false,
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { StepsFilename = Path.Combine( Directory.GetCurrentDirectory(), sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "tvmeshfile",
						sDescription = "specifies the time-varying mesh file",
						IsRequired = false,
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { TVMeshFilename = Path.Combine( Directory.GetCurrentDirectory(), sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "debug",
						sDescription = "turns on debugging mode",
						fnCommand = delegate { ModelingHistory.DEBUGMODE = true; return true; }
					},
					new Command() {
						sCommand = "glinfo",
						sDescription = "displays opengl information",
						fnCommand = delegate { ViewerControl.PrintOpenGLInformation(); return false; }
					},
					new Command() {
						sCommand = "glcolorformat",
						sDescription = "specify colorformat for GL context (default: " + ViewerControl.GLColorFormat.BitsPerPixel + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { ViewerControl.GLColorFormat = new ColorFormat( Int32.Parse( sArgs[1] ) ); return true; }
					},
					new Command() {
						sCommand = "glsamples",
						sDescription = "specify samples for GL context (default: " + ViewerControl.GLSamples + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { ViewerControl.GLSamples = Int32.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "gldepth",
						sDescription = "specify depth for GL context (default: " + ViewerControl.GLDepth + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { ViewerControl.GLDepth = Int32.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "glstencil",
						sDescription = "specify stencil for GL context (default: " + ViewerControl.GLStencil + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { ViewerControl.GLStencil = Int32.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "glstereo",
						sDescription = "specify stereo (bool) for GL context (default: " + ViewerControl.GLStereo + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { ViewerControl.GLStereo = bool.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "autofilterundos",
						sDescription = "specify whether undos are automatically filtered (default: " + AutoFilterUndos + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { AutoFilterUndos = bool.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "autofilterguicommands",
						sDescription = "specify whether gui commands are automatically filtered (default: " + AutoFilterGUICommands + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { AutoFilterGUICommands = bool.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "autofilterviewcommands",
						sDescription = "specify whether view commands are automatically filtered (default: " + AutoFilterViewCommands + ")",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) { AutoFilterViewCommands = bool.Parse( sArgs[1] ); return true; }
					},
					new Command() {
						sCommand = "addcustomhelmetlayer",
						sDescription = "adds an extra (hard-coded) custom layer (only works for helmet model!)",
						fnCommand = delegate( string[] sArgs ) { ModelingHistory.AddCustomHelmetLayer = true; return true; },
					},
					new Command() {
						sCommand = "addclusterbyconnection",
						sDescription = "adds an automated layer that clusters by connection",
						fnCommand = delegate { ModelingHistory.AddClusterByConnection = true; return true; },
					},
					new Command() {
						sCommand = "clusterlayers",
						sDescription = "simplify clustering layers",
						fnCommand = delegate { ModelingHistory.ClusterLayers = true; return true; },
					},
					new Command() {
						sCommand = "camera",
						sDescription = "sets camera properties: easing speed and delay",
						sUsageDescription = "[ease_normal] [delay_normal] [ease_fast] [delay_fast]",
						nRequiredArguments = 4,
						fnCommand = delegate( string[] sArgs ) {
							ViewerWindow.EaseSpeed = Single.Parse( sArgs[1] );
							ViewerWindow.PlayDelay = Int32.Parse( sArgs[2] );
							ViewerWindow.EaseSpeedFast = Single.Parse( sArgs[3] );
							ViewerWindow.PlayDelayFast = Int32.Parse( sArgs[4] );
							return true;
						},
					},
					new Command() {
						sCommand = "cachelayers",
						sDescription = "sets the number of pre-cached layers",
						nRequiredArguments = 1,
						fnCommand = delegate( string[] sArgs ) {
							ModelingHistory.CacheTopNLayers = Int32.Parse( sArgs[1] );
							return true;
						},
					},
				}
			};
		}
	}
}
