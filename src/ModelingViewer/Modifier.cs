using System;
using System.IO;

namespace ModelingViewer
{
	public enum Modifiers
	{
		Mirror,
		//SubDiv,
		//BoolOp,
		//Solidify
	}
	public enum ModifierBoolOps
	{
		Intersect,
		Union,
		Difference
	}
	
	public abstract class Modifier : IBinaryConvertible
	{
		public Modifiers type { get; set; }
		public string name { get; set; }
		
		public Modifier( Modifiers type ) { this.type = type; }
		public Modifier( Modifiers type, string name ) { this.type = type; this.name = name; }
		
		protected abstract void WriteBinaryInfo( BinaryWriter bw );
		protected abstract void ReadBinaryInfo( BinaryReader br );
		
		public virtual void WriteBinary( BinaryWriter bw )
		{
			bw.Write( name );
			bw.WriteT( type );
			WriteBinaryInfo( bw );
		}
		
		public virtual void ReadBinary( BinaryReader br ) {
			name = br.ReadString();
			type = br.ReadEnum<Modifiers>();
			ReadBinaryInfo(br);
		}
		
		public static Modifier ReadBinaryFile( BinaryReader br )
		{
			string name = br.ReadString();
			Modifiers type = br.ReadEnum<Modifiers>();
			switch( type )
			{
			//case Modifiers.BoolOp: return new ModifierBoolOp( name, br );
			case Modifiers.Mirror: return new ModifierMirror( name, br );
			//case Modifiers.Solidify: return new ModifierSolidify( name, br );
			//case Modifiers.SubDiv: return new ModifierSubDiv( name, br );
			default: throw new Exception( "unimplemented" );
			}
		}
	}
	
	public class ModifierMirror : Modifier
	{
		public bool usex { get; set; }
		public bool usey { get; set; }
		public bool usez { get; set; }
		public float mergethreshold { get; set; }
		
		
		public ModifierMirror() : base( Modifiers.Mirror ) { }
		
		public ModifierMirror( string name, bool usex, bool usey, bool usez, float mergethreshold )
			: base( Modifiers.Mirror, name )
		{
			this.usex = usex;
			this.usey = usey;
			this.usez = usez;
			this.mergethreshold = mergethreshold;
		}
		
		public ModifierMirror( string name, BinaryReader br )
			: base( Modifiers.Mirror, name )
		{
			usex = br.ReadBoolean();
			usey = br.ReadBoolean();
			usez = br.ReadBoolean();
			mergethreshold = br.ReadSingle();
		}
		
		protected override void WriteBinaryInfo( BinaryWriter bw )
		{
			bw.Write( usex );
			bw.Write( usey );
			bw.Write( usez );
			bw.Write( mergethreshold );
		}
		
		protected override void ReadBinaryInfo( BinaryReader br )
		{
			usex = br.ReadBoolean();
			usey = br.ReadBoolean();
			usez = br.ReadBoolean();
			mergethreshold = br.ReadSingle();
		}
	}
	
	/*public class ModifierSubDiv : Modifier, IBinaryConvertible
	{
		public int levels { get; set; }
		
		public ModifierSubDiv() : base( Modifiers.SubDiv ) { }
		
		public ModifierSubDiv( string name, int levels )
			: base( Modifiers.SubDiv, name )
		{
			this.levels = levels;
		}
		
		public ModifierSubDiv( string name, BinaryReader br )
			: base( Modifiers.SubDiv, name )
		{
			levels = br.ReadInt32();
		}
		
		protected override void WriteBinaryInfo( BinaryWriter bw ) { bw.Write( levels ); }
	}
	
	public class ModifierBoolOp : Modifier, IBinaryConvertible
	{
		public ModifierBoolOps operation { get; set; }
		public string objectname { get; set; }
		
		public ModifierBoolOp() : base( Modifiers.BoolOp ) { }
		
		public ModifierBoolOp( string name, ModifierBoolOps operation, string objectname )
			: base( Modifiers.BoolOp, name )
		{
			this.operation = operation;
			this.objectname = objectname;
		}
		
		public ModifierBoolOp( string name, BinaryReader br )
			: base( Modifiers.BoolOp, name )
		{
			operation = br.ReadEnum<ModifierBoolOps>();
			objectname = br.ReadString();
		}
		
		public static ModifierBoolOps StringToBoolOp( string sboolop )
		{
			switch( sboolop )
			{
			case "INTERSECT": return ModifierBoolOps.Intersect;
			case "UNION": return ModifierBoolOps.Union;
			case "DIFFERENCE": return ModifierBoolOps.Difference;
			}
			throw new Exception( "ModifierBoolOp.StringToBoolOp: Unknown boolean op " + sboolop );
		}
		
		protected override void WriteBinaryInfo( BinaryWriter bw ) { bw.WriteT( operation ); bw.Write( objectname ); }
	}
	
	public class ModifierSolidify : Modifier, IBinaryConvertible
	{
		public float thickness { get; set; }
		public float offset { get; set; }
		public float edge_crease_inner { get; set; }
		public float edge_crease_outer { get; set; }
		public float edge_crease_rim { get; set; }
		public bool use_even_offset { get; set; }
		public bool use_quality_normals { get; set; }
		public bool use_rim { get; set; }
		
		public ModifierSolidify() : base( Modifiers.Solidify ) { }
		
		public ModifierSolidify( string name, float thickness, float offset, float edge_crease_inner, float edge_crease_outer, float edge_crease_rim, bool use_even_offset, bool use_quality_normals, bool use_rim )
			: base( Modifiers.Solidify, name )
		{
			this.thickness = thickness;
			this.offset = offset;
			this.edge_crease_inner = edge_crease_inner;
			this.edge_crease_outer = edge_crease_outer;
			this.edge_crease_rim = edge_crease_rim;
			this.use_even_offset = use_even_offset;
			this.use_quality_normals = use_quality_normals;
			this.use_rim = use_rim;
		}
		
		public ModifierSolidify( string name, BinaryReader br )
			: base( Modifiers.Solidify, name )
		{
			this.thickness = br.ReadSingle();
			this.offset = br.ReadSingle();
			this.edge_crease_inner = br.ReadSingle();
			this.edge_crease_outer = br.ReadSingle();
			this.edge_crease_rim = br.ReadSingle();
			this.use_even_offset = br.ReadBoolean();
			this.use_quality_normals = br.ReadBoolean();
			this.use_rim = br.ReadBoolean();
		}
		
		protected override void WriteBinaryInfo( BinaryWriter bw )
		{
			bw.Write( thickness );
			bw.Write( offset );
			bw.Write( edge_crease_inner );
			bw.Write( edge_crease_outer );
			bw.Write( edge_crease_rim );
			bw.Write(use_even_offset );
			bw.Write( use_quality_normals );
			bw.Write( use_rim );
		}
	}*/
}

