using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using OpenTK;
//using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Common.Libs.Image;
using Common.Libs.MatrixMath;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

//using GL = OpenTK.Graphics.OpenGL.GL;
//using EnableCap = OpenTK.Graphics.OpenGL.EnableCap;
//using ClearBufferMask = OpenTK.Graphics.OpenGL.ClearBufferMask;
//using BlendingFactorSrc = OpenTK.Graphics.OpenGL.BlendingFactorSrc;
using ColorFormat = OpenTK.Graphics.ColorFormat;
using GraphicsMode = OpenTK.Graphics.GraphicsMode;
using GraphicsContextFlags = OpenTK.Graphics.GraphicsContextFlags;

namespace ModelingViewer
{
	public enum ViewerControlMouseStates
	{
		MouseMove, MouseDown, MouseUp, MouseWheel
	}
	
	public abstract class ViewerControl : GLControl
	{
		protected ToolStrip toolstrip;
		
		protected CameraProperties Camera = new CameraProperties();
		protected Property<Color> RenderOptBackColor = new Property<Color>( "Background Color", Color.DimGray );
		
		protected bool bControlPressed = false;
		protected bool bShiftPressed = false;
		protected bool bAltPressed = false;
		
		protected bool bHideCuror = false;
		protected Cursor HiddenCursor;
		
		protected Point lastMouseLocation;
		protected bool bMouseInside = false;
		
		protected abstract void HandleMouseEnter();
		protected abstract void HandleMouseLeave();
		protected abstract void HandleMouse( MouseEventArgs e, ViewerControlMouseStates state );
		
		protected abstract void InitToolStrip();
		
		protected Matrix mCamProj;
		protected Matrix mCamTrans;
		protected Matrix mCamMatrix; // mCamTrans * mCamProj
		protected Matrix mCamUnproject;
		
		private object RenderLock = new object();
		public bool IsRendering { get; private set; }
		private System.Timers.Timer RefreshTimer = new System.Timers.Timer( 5 );
		
		#region OpenGL Information / Test Functions
		
		public static ColorFormat GLColorFormat	= new ColorFormat( 24 );
		public static int GLDepth				= 24;
		public static int GLStencil				= 8;
		public static int GLSamples				= 0;
		public static bool GLStereo				= false;
		
		public CameraProperties GetCamera() { return Camera; }
		
		public static void PrintOpenGLInformation()
		{
			// copied from TestGraphicsModes.cs in OpenTK Examples
			Dictionary<GraphicsMode, GraphicsMode> modes = new Dictionary<GraphicsMode, GraphicsMode>();
			
			foreach( ColorFormat color in new ColorFormat[] { 32, 24, 16, 8 } )
				foreach( int depth in new int[] { 24, 16, 8 } )
					foreach( int stencil in new int[] { 8, 0 } )
						foreach( int samples in new int[] { 0, 2, 4, 6, 8, 16 } )
							foreach( bool stereo in new bool[] { false, true } )
						{
							GraphicsMode mode = TestGraphicsMode( color, depth, stencil, samples, stereo );
							if( mode != null && !modes.ContainsKey( mode ) ) modes.Add( mode, mode );
						}
			
			System.Console.WriteLine( "Cl (RGBA), Dp, St, AA, Stereo" );
			System.Console.WriteLine( "-----------------------------" );
			foreach( GraphicsMode mode in modes.Keys )
				System.Console.WriteLine( String.Format( "{0}, {1:00}, {2:00}, {3:00}, {4}", mode.ColorFormat, mode.Depth, mode.Stencil, mode.Samples, mode.Stereo ) );
		}
		
		public static GraphicsMode TestGraphicsMode() { return TestGraphicsMode( GLColorFormat, GLDepth, GLStencil, GLSamples, GLStereo ); }
		public static GraphicsMode TestGraphicsMode( ColorFormat color, int depth, int stencil, int samples, bool stereo )
		{
			GraphicsMode mode = null;
			try { mode = new GraphicsMode( color, depth, stencil, samples, 48, 2, stereo ); mode.ToString(); } catch(Exception) { mode = null; }
			return mode;
		}
		
		#endregion
		
		#region Constructor
		
		public ViewerControl( ) : base( new GraphicsMode( GLColorFormat, GLDepth, GLStencil, GLSamples, 48, 2, GLStereo ), 3, 0, GraphicsContextFlags.Debug ) //  Default
		{
			InitializeComponent();
			
			base.CreateControl();
			
			Cursor = Cursors.Cross;
			HiddenCursor = MiscFileIO.LoadCursorResource( "blank.cur" );
			VSync = false;
			
			Camera.Properties.PropertyChanged += delegate { RefreshControl(); };
			RenderOptBackColor.PropertyChanged += delegate { RefreshControl();  };
			
			RefreshTimer.Elapsed += delegate { RefreshControl(); };
		}
		
		public void SetToolStrip( ToolStrip toolstrip ) { this.toolstrip = toolstrip; InitToolStrip(); }
		
		public void RefreshControl()
		{
			if( IsRendering || !this.IsHandleCreated ) { RefreshTimer.Enabled = true; return; }
			
			//lock( RenderLock )
			//{
			//	if( IsRendering || !this.IsHandleCreated ) { RefreshTimer.Enabled = true; return; }
				RefreshTimer.Enabled = false;
				IsRendering = true;
			//}
			
			this.Invoke( (MethodInvoker) RefreshControlHelper ); // delegate { Invalidate(); }
		}
		internal void RefreshControlHelper()
		{
			Invalidate( true );
		}
		
		#endregion
		
		#region Control Code
		
		private void InitializeComponent()
		{
			this.SuspendLayout();
			// 
			// GLViewer
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = RenderOptBackColor.Val; //new Vec3f( 0.35f, 0.35f, 0.35f );
			this.Name = "ViewerControl";
			this.ResumeLayout( false );
		}
		
		#endregion
		
		#region Control Events: OnPaint, OnMouse*, OnResize
		
		protected override void OnPaint( PaintEventArgs e )
		{
			base.OnPaint( e );
			MakeCurrent();
			SetUpRender();
			RenderData();
			SwapBuffers();
			IsRendering = false;
		}
		
		protected override void OnMouseDown( MouseEventArgs e )
		{
			base.OnMouseDown( e );
			
			bControlPressed = ((Control.ModifierKeys & Keys.Control) != 0);
			bShiftPressed = ((Control.ModifierKeys & Keys.Shift) != 0);
			bAltPressed = ((Control.ModifierKeys & Keys.Alt) != 0);
			
			lastMouseLocation = e.Location;
			
			HandleMouse( e, ViewerControlMouseStates.MouseDown );
		}
		
		protected override void OnMouseUp ( MouseEventArgs e )
		{
			base.OnMouseUp( e );
			lastMouseLocation = e.Location;
			HandleMouse( e, ViewerControlMouseStates.MouseUp );
		}
		
		protected override void OnMouseMove( MouseEventArgs e )
		{
			base.OnMouseMove( e );
			HandleMouse( e, ViewerControlMouseStates.MouseMove );
			lastMouseLocation = e.Location;
		}
		
		protected override void OnResize (EventArgs e)
		{
			base.OnResize( e );
			Camera.Resize( Width, Height );
		}
		
		protected override void OnMouseWheel (MouseEventArgs e)
		{
			base.OnMouseWheel (e);
			HandleMouse( e, ViewerControlMouseStates.MouseWheel );
		}
		
		protected override void OnMouseEnter( EventArgs e )
		{
			base.OnMouseEnter( e );
			Focus();
			bMouseInside = true;
			SetMouseCursor();
			HandleMouseEnter();
		}
		
		protected override void OnMouseLeave( EventArgs e )
		{
			base.OnMouseLeave( e );
			bMouseInside = false;
			SetMouseCursor();
			HandleMouseLeave();
		}
		
		protected void SetMouseCursor()
		{
			if( bMouseInside ) {
				if( bHideCuror ) Cursor = HiddenCursor;
				else Cursor = Cursors.Cross;
			} else {
				Cursor = Cursors.Cross;
			}
		}
		
		#endregion
		
		private void SetUpRender()
		{
			mCamProj = Camera.GetProjectionMatrix();
			mCamTrans = Camera.GetTransformationMatrix();
			mCamMatrix = mCamTrans * mCamProj;
			mCamUnproject = Matrix.Invert( mCamMatrix );
			if( mCamUnproject == null )
			{
				System.Console.WriteLine( "Position " + Camera.GetPosition().ToStringFormatted() );
				System.Console.WriteLine( "Target " + Camera.GetTarget().ToStringFormatted() );
				System.Console.WriteLine( "Up " + Camera.GetUp().ToStringFormatted() );
				System.Console.WriteLine( "Rotation " + Camera.GetRotation().ToStringWXYZ() );
				System.Console.WriteLine( "Distance " + Camera.GetDistance() );
				System.Console.WriteLine( "Projection\n" + mCamProj.ToString() );
				System.Console.WriteLine( "Translation\n" + mCamTrans.ToString() );
				System.Console.WriteLine( "Matrix\n" + mCamMatrix.ToString() );
				//throw new Exception( "Cannot invert matrix!" );
				mCamUnproject = Matrix.Eye( 4 );
			}
			
			GL.ClearColor( RenderOptBackColor.Val ); //Color.FromArgb( 0, 255, 255, 255 ) ); // Color.DimGray );
			
			//GL.Enable( EnableCap.AlphaTest );
			//GL.AlphaFunc( AlphaFunction.Gequal, 0.5f );
			
			GL.Enable( EnableCap.DepthTest );
			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );
			
			GL.Enable( EnableCap.Multisample );
			//GL.BlendFunc( BlendingFactorSrc.One, BlendingFactorDest.OneMinusSrcAlpha );
			GL.BlendFunc( BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha );
			GL.DepthFunc( DepthFunction.Lequal );
			
			//GL.Enable( EnableCap.LineSmooth );
			//GL.Enable( EnableCap.PointSmooth );
			//GL.Enable( EnableCap.PolygonSmooth );
			GL.Hint( HintTarget.PointSmoothHint, HintMode.Nicest );
			GL.Hint( HintTarget.LineSmoothHint, HintMode.Nicest );
			GL.Hint( HintTarget.PolygonSmoothHint, HintMode.Nicest );
			
			GL.Enable( EnableCap.Texture2D );
			GL.Hint( HintTarget.PerspectiveCorrectionHint, HintMode.Nicest );
			
			float mult = 0.01f;
			float[] constant = { mult * 200.0f, mult * 0.0f, mult * 0.0f };
			float[] linear = { mult * 0.0f, mult * 0.2f, mult * 0.0f };
			float[] quad = { mult * 0.25f, mult * 0.0f, mult * 0.016f };
			if( !Camera.GetOrtho() ) {
				//GL.Enable( EnableCap.PointSmooth );
				//GL.PointSize( 2.5f );
				GL.PointParameter( PointParameterName.PointDistanceAttenuation, linear );
				GL.PointParameter( PointParameterName.PointSizeMax, 100.0f );
			} else {
				GL.PointParameter( PointParameterName.PointDistanceAttenuation, constant );
			}
			
			//GL.PolygonOffset( 5.0f, 5.0f );
			GL.PolygonOffset( 1.0f, 1.0f );
			GL.Enable( EnableCap.PolygonOffsetFill );
			GL.Enable( EnableCap.PolygonOffsetLine );
			GL.Enable( EnableCap.PolygonOffsetPoint );
			
			/*float[] diffuse = new float[] { 10.0f, 10.0f, 10.0f, 1.0f };
			float[] specular = new float[] { 0.0f, 10.0f, 0.0f, 1.0f };
			float[] lightpos = new float[] { 0.0f, 0.0f, 10.0f } ;
			GL.Light( LightName.Light0, LightParameter.Diffuse, diffuse );
			GL.Light( LightName.Light0, LightParameter.Specular, specular );
			GL.Light( LightName.Light0, LightParameter.Position, lightpos );
			GL.LightModel( LightModelParameter.LightModelTwoSide, 1 );
			GL.Material( MaterialFace.FrontAndBack, MaterialParameter.Shininess, new float[] { 60.0f } );
			GL.Material( MaterialFace.FrontAndBack, MaterialParameter.Specular, new float[] { 1.0f, 1.0f, 1.0f, 1.0f } );
			GL.Material( MaterialFace.FrontAndBack, MaterialParameter.Diffuse, new float[] { 1.0f, 1.0f, 1.0f, 1.0f } );
			GL.Enable( EnableCap.Light0 );
			GL.Enable( EnableCap.Lighting );
			GL.ShadeModel( ShadingModel.Smooth );*/
			
			GL.MatrixMode( MatrixMode.Projection );
			Matrix4d mProj4d = mCamProj.ToOpenTKMatrix4d();
			GL.LoadMatrix( ref mProj4d );
			
			GL.MatrixMode( MatrixMode.Modelview );
			Matrix4d mTran4d = mCamTrans.ToOpenTKMatrix4d();
			GL.LoadMatrix( ref mTran4d );
			
			GL.Viewport( 0, 0, this.Width, this.Height );
			
			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );
		}
		
		#region Rendering Functions
		
		public abstract void RenderData();
		
		protected void RenderCircle( Vec3f center, Vec3f x, Vec3f y, Vec3f color )
		{
			GL.Begin( BeginMode.LineLoop );
			for( int i = 0; i < 360; i += 10 )
			{
				float r = (float)i / 180.0f * FMath.PI;
				float xm = FMath.Cos( r );
				float ym = FMath.Sin( r );
				Vec3f p = center + x * xm + y * ym;
				GL.Color3( color.x, color.y, color.z );
				GL.Vertex3( p.x, p.y, p.z );
			}
			GL.End();
		}
		
		protected void RenderPoint( Vec3f coord, Vec3f color )
		{
			GL.Begin( BeginMode.Points );
			GL.Color3( color.x, color.y, color.z );
			GL.Vertex3( coord.x, coord.y, coord.z );
			GL.End();
		}
		protected void RenderPoints( Vec3f[] coords, Vec3f[] colors )
		{
			GL.Begin( BeginMode.Points );
			for( int i = 0; i < coords.Length; i++ )
			{
				Vec3f coord = coords[i];
				Vec3f color = colors[i];
				GL.Color3( color.x, color.y, color.z );
				GL.Vertex3( coord.x, coord.y, coord.z );
			}
			GL.End();
		}
		
		protected void RenderLine( Vec3f p1, Vec3f c1, Vec3f p2, Vec3f c2 )
		{
			GL.Begin( BeginMode.Lines );
			GL.Color3( c1.r, c1.g, c1.b );
			GL.Vertex3( p1.x, p1.y, p1.z );
			GL.Color3( c2.r, c2.g, c2.b );
			GL.Vertex3( p2.x, p2.y, p2.z );
			GL.End();
		}
		
		protected void RenderText()
		{
				
		}
		
		protected void RenderRotationGizmo()
		{
			float size = 2.0f;
			float scale = ( Camera.GetOrtho() ? ( 8.0f / (float)Camera.Scale.Val ) : ( 1.0f / 600.0f ) );
			
			Vec3f center = Unproject2DTo3D( new Vec3f( 0.0f, 0.0f, -0.5f ) );
			Vec3f xaxis = Vec3f.X * size * scale + center;
			Vec3f yaxis = Vec3f.Y * size * scale + center;
			Vec3f zaxis = Vec3f.Z * size * scale + center;
			GL.LineWidth( 1.0f );
			RenderLine( center, Vec3f.X, xaxis, Vec3f.X );
			RenderLine( center, Vec3f.Y, yaxis, Vec3f.Y );
			RenderLine( center, Vec3f.Z, zaxis, Vec3f.Z );
		}
		
		protected void RenderViewable( FlattenedViewable v, float linewidth )
		{
			GL.LineWidth( linewidth );
			RenderViewable( v );
			GL.LineWidth( 1.0f );
		}
		protected void RenderViewable( FlattenedViewable v )
		{
			int groupsize = v.GroupSize;
			int count = v.nVerts;
			BeginMode mode;
			switch ( groupsize )
			{
			case 1: mode = BeginMode.Points; break;
			case 2: mode = BeginMode.Lines; break;
			case 3: mode = BeginMode.Triangles; break;
			case 4: mode = BeginMode.Quads; break;
			default: throw new ArgumentException( "ViewerControl.RenderViewable: Unimplemented group size: " + groupsize );
			}
			
			GL.EnableClientState( ArrayCap.ColorArray );
			GL.EnableClientState( ArrayCap.VertexArray );
			GL.VertexPointer( 3, VertexPointerType.Float, 0, v.Verts );
			GL.ColorPointer( 3, ColorPointerType.Float, 0, v.Colors );
			GL.DrawArrays( mode, 0, count );
			GL.DisableClientState( ArrayCap.ColorArray );
			GL.DisableClientState( ArrayCap.VertexArray );
		}
		
		protected void RenderViewable( FlattenedViewableAlpha v )
		{
			int groupsize = v.GroupSize;
			int count = v.nVerts;
			BeginMode mode;
			switch ( groupsize )
			{
				case 1: mode = BeginMode.Points; break;
				case 2: mode = BeginMode.Lines; break;
				case 3: mode = BeginMode.Triangles; break;
				default: throw new ArgumentException( "ViewerControl.RenderViewable: Unimplemented group size: " + groupsize );
			}
			
			GL.EnableClientState( ArrayCap.ColorArray );
			GL.EnableClientState( ArrayCap.VertexArray );
			
			GL.VertexPointer( 3, VertexPointerType.Float, 0, v.Verts );
			GL.ColorPointer( 4, ColorPointerType.Float, 0, v.Colors );
			
			GL.DrawArrays( mode, 0, count );
			
			GL.DisableClientState( ArrayCap.ColorArray );
			GL.DisableClientState( ArrayCap.VertexArray );
		}
		
		protected void RenderViewable( IndexedViewableAlpha v ) { RenderViewable( v, false ); }
		protected void RenderViewable( IndexedViewableAlpha v, bool wireframe )
		{
			BeginMode mode;
			
			GL.EnableClientState( ArrayCap.VertexArray );
			GL.EnableClientState( ArrayCap.ColorArray );
			
			while( v != null )
			{
				
				GL.VertexPointer( 3, VertexPointerType.Float, 0, v.Verts );
				int indsetcount = v.Indices.Length;
				for( int iIndSet = 0; iIndSet < indsetcount; iIndSet++ )
				{
					int[] indset = v.Indices[iIndSet];
					int count = indset.Count();
					if( count == 0 ) continue;
					
					switch ( v.GroupSizes[iIndSet] )
					{
					case 1: mode = BeginMode.Points; break;
					case 2: mode = BeginMode.Lines; break;
					case 3: mode = BeginMode.Triangles; break;
					case 4: mode = BeginMode.Quads; break;
					default: throw new ArgumentException( "ViewerControl.RenderViewable: Unimplemented group size: " + v.GroupSizes[iIndSet] );
					}
					
					if( wireframe && v.GroupSizes[iIndSet] > 2 ) continue;
					
					GL.ColorPointer( 4, ColorPointerType.Float, 0, v.Colors[iIndSet] );
					
					if( v.PointSizes != null ) {
						float mult = Math.Min( (float)Camera.Scale.Val / 6.0f, 2.0f );// * ((float)Camera.Height.Val / 720.0f);
						if( Camera.Ortho ) GL.PointSize( v.PointSizes[iIndSet] * mult );
						else GL.PointSize( v.PointSizes[iIndSet] );
					}
					if( v.LineWidths != null ) {
						//float s = (float)Camera.Scale.Val; //System.Console.WriteLine( s );
						//float s50 = Math.Max( 0.01f, 50 - s );
						if( Camera.Ortho ) GL.LineWidth( v.LineWidths[iIndSet] );
						else GL.LineWidth( v.LineWidths[iIndSet] );
					}
					
					GL.DrawElements( mode, count, DrawElementsType.UnsignedInt, indset );
				}
				
				v = v.attached;
			}
			
			GL.DisableClientState( ArrayCap.ColorArray );
			GL.DisableClientState( ArrayCap.VertexArray );
			
			GL.PointSize( 2.5f );
			
		}
		
		public Vec3f Project3DTo2D( Vec3f pt ) { return MatVecMult( mCamMatrix, pt ); }
		public Vec3f Unproject2DTo3D( Vec3f pt ) { return MatVecMult( mCamUnproject, pt ); }
		private Vec3f MatVecMult( Matrix mat, Vec3f v )
		{
			double x = mat[0,0] * v.x + mat[1,0] * v.y + mat[2,0] * v.z + 1 * mat[3,0];
			double y = mat[0,1] * v.x + mat[1,1] * v.y + mat[2,1] * v.z + 1 * mat[3,1];
			double z = mat[0,2] * v.x + mat[1,2] * v.y + mat[2,2] * v.z + 1 * mat[3,2];
			double w = mat[0,3] * v.x + mat[1,3] * v.y + mat[2,3] * v.z + 1 * mat[3,3];
			
			return new Vec3f( (float) (x / w), (float) (y / w), (float) (z / w) );
		}
		
		
		public delegate string GetVertexLabel( int iVert );
		protected void RenderGroupIndices( IndexedViewableAlpha v, int iIndSet, GLText gltext )
		{
			RenderLabeledGroups( v, iIndSet, gltext, delegate(int ivert) { return ""+ivert; } );
		}
		protected void RenderLabeledGroups( IndexedViewableAlpha v, int iIndSet, GLText gltext, GetVertexLabel getlabel )
		{
			float multy = (float)Height / 2.0f;
			float multx = (float)Width / 2.0f;
			
			int[] inds = v.Indices[iIndSet];
			int sz = v.GroupSizes[iIndSet];
			
			for( int i = 0; i < v.Indices[iIndSet].Length; i += sz )
			{
				Vec3f avg = new Vec3f();
				for( int j = 0; j < sz; j++ )
					avg += v.Verts[inds[i+j]];
				avg /= (float)sz;
				Vec3f p = Project3DTo2D( avg );
				
				string slabel = getlabel( i );
				if( slabel == "" ) continue;
				
				gltext.WriteString( p.x * multx, p.y * multy, slabel, GLTextPosition.LeftBottom );
			}
		}
		
		protected void RenderCameraTarget( float size, Vec3f color )
		{
			GL.LineWidth( 2.00f );
			RenderXYZCross( Camera.GetTarget(), size, color );
			GL.LineWidth( 1.00f );
		}
		
		protected void RenderOriginCross( float size, Vec3f color )
		{
			RenderXYZCross( new Vec3f( 0.0f, 0.0f, 0.01f ), size, color );
		}
		
		protected void RenderXYZCross( Vec3f center, float size, Vec3f color )
		{
			Vec3f x = new Vec3f( size, 0.0f, 0.0f );
			Vec3f y = new Vec3f( 0.0f, size, 0.0f );
			Vec3f z = new Vec3f( 0.0f, 0.0f, size );
			FlattenedViewable cross = new FlattenedViewable(
				new Vec3f[] { center - x, center + x, center - y, center + y, center - z, center + z },
				new Vec3f[] { color, color, color, color, color, color },
				2 
				);
			RenderViewable( cross );
		}
		
		protected void RenderGridXY() { RenderGrid( Vec3f.X, Vec3f.Y ); }
		protected void RenderGridXZ() { RenderGrid( Vec3f.X, Vec3f.Z ); }
		protected void RenderGridYZ() { RenderGrid( Vec3f.Y, Vec3f.Z ); }
		protected void RenderGridXY( float alpha ) { RenderGrid( Vec3f.X, Vec3f.Y, alpha ); }
		protected void RenderGridXZ( float alpha ) { RenderGrid( Vec3f.X, Vec3f.Z, alpha ); }
		protected void RenderGridYZ( float alpha ) { RenderGrid( Vec3f.Y, Vec3f.Z, alpha ); }
		
		protected void RenderGrid( Vec3f axis0, Vec3f axis1 ) { RenderGrid( axis0, axis1, 0.20f ); }
		protected void RenderGrid( Vec3f axis0, Vec3f axis1, float alpha )
		{
			if( alpha < 0.1f ) return;
			
			float axisLength = 10.0f;
			int gridCount = 10;
			Vec4f gridColor = new Vec4f( 0.5f, 0.5f, 0.5f, alpha );
			Vec4f gridMinorColor = new Vec4f( 0.45f, 0.45f, 0.45f, alpha * 0.5f );
			float gridWidth = 0.5f;
			
			bool renderMinor = ( Camera.Scale.Val > 10.0 );
			
			axis0 *= axisLength;
			axis1 *= axisLength;
			Vec3f axis2 = Vec3f.Normalize( axis0 ^ axis1 ) * -0.01f;
			
			Vec3f p00, p01, p10, p11;
			GL.Enable( EnableCap.Blend );
			GL.BlendFunc( BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha );
			GL.LineWidth( gridWidth );
			GL.Begin( BeginMode.Lines );
			for ( int i = -gridCount; i <= gridCount; i++ )
			{
				float f = (float) i / (float) gridCount;
				
				p00 = axis0 * f + axis1 + axis2;
				p01 = axis0 * f - axis1 + axis2;
				p10 = axis1 * f + axis0 + axis2;
				p11 = axis1 * f - axis0 + axis2;
				
				GL.Color4( gridColor.x, gridColor.y, gridColor.z, gridColor.w );
				GL.Vertex3( p00.x, p00.y, p00.z );
				GL.Vertex3( p01.x, p01.y, p01.z );
				GL.Vertex3( p10.x, p10.y, p10.z );
				GL.Vertex3( p11.x, p11.y, p11.z );
				
				if( renderMinor && i < gridCount )
				{
					GL.Color4( gridMinorColor.x, gridMinorColor.y, gridMinorColor.z, gridMinorColor.w );
					for( int j = 1; j < 10; j++ )
					{
						float a = (float)j / ( 10.0f * (float)gridCount );
						
						p00 = axis0 * (f+a) + axis1 + axis2 * 2.0f;
						p01 = axis0 * (f+a) - axis1 + axis2 * 2.0f;
						p10 = axis1 * (f+a) + axis0 + axis2 * 2.0f;
						p11 = axis1 * (f+a) - axis0 + axis2 * 2.0f;
						
						GL.Vertex3( p00.x, p00.y, p00.z );
						GL.Vertex3( p01.x, p01.y, p01.z );
						GL.Vertex3( p10.x, p10.y, p10.z );
						GL.Vertex3( p11.x, p11.y, p11.z );
					}
				}
			}
			
			GL.End();
		}
		
		#endregion
		
	}
}
