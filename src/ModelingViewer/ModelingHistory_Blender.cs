using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Common.Libs.VMath;
using Common.Libs.MiscFunctions;

namespace ModelingViewer
{
	public partial class ModelingHistory
	{
		#region Command Translation Dictionaries
		
		// below translations are used to translate an ambiguous command with unambiguous options to a fully unambiguous command
		// ie: generic.command("Some text here for an actual command") => actual.command()
		// * should be somewhat meaningful ( for debugging purposes :) )
		// NOTE: more translations in TranslateCommand function
		
		private static Dictionary<string,string> dictTrans_CmdCmd_BlenderToGeneric = new Dictionary<string, string>()
		{
			{ "begin",								"gui.begin" },
			
			{ "mode.toggle_edit_mode",				"mode.toggle_editmode" },
			
			{ "mode.translate_manipulator",			"gui.manipulator.select_translate" },
			{ "mode.rotate_manipulator",			"gui.manipulator.select_rotate" },
			{ "mode.scale_manipulator",				"gui.manipulator.select_scale" },
			{ "mode.manipulator_normal",			"gui.manipulator.select_normal" },
			{ "mode.manipulator_global",			"gui.manipulator.select_global" },
			{ "mode.manipulator_local",				"gui.manipulator.select_local" },
			{ "mode.manipulator_view",				"gui.manipulator.select_view" },
			{ "mode.toggle_manipulate_centers",		"gui.manipulator.toggle_centers" },
			{ "mode.toggle_3d_manipulator",			"gui.manipulator.toggle_visible" },
			
			//{ "mode.toggle_show_mod_in_edit",		"modifier.toggle_visible" },
			{ "object.modifier_add",				"modifier.add" },
			{ "modifier.toggle_mirror_x",			"modifier.mirror.toggle_x" },
			{ "modifier.toggle_mirror_y",			"modifier.mirror.toggle_y" },
			{ "modifier.toggle_mirror_z",			"modifier.mirror.toggle_z" },
			{ "modifier.set_merge_limit",			"modifier.mirror.set_merge_limit" },
			{ "object.modifier_remove",				"modifier.remove" },
			//{ "object.modifier_apply", "" },
			
			{ "ed.undo",							"undo.undo" },
			{ "ed.redo",							"undo.redo" },
			
			{ "mode.toggle_opaque",					"visible.toggle_opaque" },
			{ "object.hide_view_clear",				"visible.object.show_all" },
			{ "object.hide_view_set",				"visible.object.hide" },
			{ "mesh.reveal",						"visible.mesh.show_all" },
			{ "mesh.hide",							"visible.mesh.hide" },
			
			{ "view3d.move",						"view.move" },
			{ "view3d.rotate",						"view.rotate" },
			{ "view3d.view_persportho",				"view.toggle_persportho" },
			{ "view3d.viewnumpad",					"view.set" },
			{ "view3d.view_orbit",					"view.orbit" },
			{ "view3d.view_selected",				"view.show.selected_verts" },
			{ "view3d.localview",					"view.show.local" },
			{ "view3d.view_all",					"view.show.all" },
			{ "view3d.view_pan",					"view.pan" },
			{ "view3d.smoothview",					"view.smoothview" },
			{ "view3d.zoom",						"view.zoom" },
			{ "view3d.zoom_border",					"view.show.border" },
			//{ "view3d.select_vertex_mode", "" },
			//{ "view3d.select_edge_mode", "" },
			//{ "view3d.select_face_mode", "" },
			
			{ "mesh.primitive_circle_add",			"topo.add.circle" },
			{ "mesh.primitive_cone_add",			"topo.add.cone" },
			{ "mesh.primitive_cube_add",			"topo.add.cube" },
			{ "mesh.primitive_cylinder_add",		"topo.add.cylinder" },
			{ "mesh.primitive_grid_add",			"topo.add.grid" },
			{ "mesh.primitive_ico_sphere_add",		"topo.add.sphere.ico" },
			{ "mesh.primitive_uv_sphere_add",		"topo.add.sphere.uv" },
			{ "mesh.primitive_monkey_add",			"topo.add.monkey" },
			{ "mesh.primitive_plane_add",			"topo.add.plane" },
			{ "mesh.primitive_torus_add",			"topo.add.torus" },
			{ "mesh.edge_face_add",					"topo.add.edge_face" },
			{ "mesh.tris_convert_to_quads",			"topo.convert.tris_to_quads" },
			{ "mesh.quads_convert_to_tris",			"topo.convert.quads_to_tris" },
			{ "mesh.delete_all",					"topo.delete.all" },
			{ "mesh.delete_vert",					"topo.delete.vert" },
			{ "mesh.delete_edge",					"topo.delete.edge" },
			{ "mesh.delete_face",					"topo.delete.face" },
			{ "object.delete",						"topo.delete.object" },
			{ "mesh.duplicate",						"topo.duplicate" },
			{ "mesh.duplicate_move",				"topo.duplicate" },
			{ "object.duplicate",					"topo.duplicate_object" },
			{ "object.duplicate_move",				"topo.duplicate_object" },
			{ "mesh.extrude",						"topo.extrude" },
			{ "mesh.extrude_edges_move",			"topo.extrude" },
			{ "mesh.extrude_faces_move",			"topo.extrude" },
			{ "mesh.extrude_region_move",			"topo.extrude" },
			{ "mesh.extrude_vertices_move",			"topo.extrude" },
			{ "mesh.loopcut",						"topo.loopcut" },
			{ "mesh.loopcut_slide",					"topo.loopcut" },
			{ "mesh.merge",							"topo.merge" },
			{ "object.join",						"topo.merge_object" },
			{ "mesh.subdivide",						"topo.subdivide" },
			//{ "mesh.extrude_repeat", "" },
			
			{ "transform.resize",					"transform.resize" },
			{ "transform.rotate",					"transform.rotate" },
			{ "transform.translate",				"transform.translate" },
			{ "transform.edge_slide",				"transform.edge_slide" },
			{ "transform.set_x",					"transform.set_x" },
			{ "transform.set_y",					"transform.set_y" },
			{ "transform.set_z",					"transform.set_z" },
			{ "transform.tosphere",					"transform.to_sphere" },
			{ "view3d.manipulator",					"transform.manipulator" },
			{ "mesh.normals_make_consistent",		"transform.make_normals_consistent" },
			{ "transform.shrink_fatten",			"transform.scale_along_normals" },
			{ "transform.mirror",					"transform.mirror" },
			
			{ "mode.select_vertex",					"select.by_vertex" },
			{ "mode.select_edge",					"select.by_edge" },
			{ "mode.select_face",					"select.by_face" },
			
			{ "mesh.select_all",					"select.all" },
			{ "mesh.select_none",					"select.none" },
			{ "mesh.loop_select",					"select.loop" },
			{ "mesh.select_linked",					"select.linked" },
			{ "mesh.select_shortest_path",			"select.shortest_path" },
			{ "mesh.edgering_select",				"select.edgering" },
			{ "view3d.select",						"select.specific" },
			{ "view3d.select_border",				"select.border" },
			{ "view3d.select_circle",				"select.circle" },
			{ "view3d.select_lasso",				"select.lasso" },
			{ "object.select_all",					"select.all_objects" },
			{ "object.select_inverse",				"select.inverse_objects" },
			
		};
		
		private static Dictionary<string,string> dictTrans_OptsCmd_ObjectModeSet = new Dictionary<string, string>()
		{
			{ "mode='EDIT', toggle=True", "mode.toggle_edit_mode" },
		};
		
		private static Dictionary<string,string> dictTrans_OptsCmd_Interface = new Dictionary<string, string>()
		{
			{ "Vertex select mode",										"mode.select_vertex" },
			{ "Edge select mode",										"mode.select_edge" },
			{ "Face select mode",										"mode.select_face" },
			
			{ "Limit selection to visible (clipped with depth buffer)",	"mode.toggle_opaque" },
			{ "Use a 3D manipulator widget for controlling transforms",	"mode.toggle_3d_manipulator" },
			{ "Translate manipulator mode",								"mode.translate_manipulator" },
			{ "Rotate manipulator mode",								"mode.rotate_manipulator" },
			{ "Scale manipulator mode",									"mode.scale_manipulator" },
			{ "Manipulate object centers only",							"mode.toggle_manipulate_centers" },
			{ "Use modifier while in the edit mode",					"mode.toggle_show_mod_in_edit" },
			
			{ "Normal",													"mode.manipulator_normal" },
			{ "Global",													"mode.manipulator_global" },
			{ "Local",													"mode.manipulator_local" },
			{ "View",													"mode.manipulator_view" },
			
			{ "X",														"modifier.toggle_mirror_x" },
			{ "Y",														"modifier.toggle_mirror_y" },
			{ "Z",														"modifier.toggle_mirror_z" },
			{ "Clipping",												"modifier.toggle_clipping" },
			
			{ "Wireframe",												"gui.viewmode.wireframe" },
			{ "Solid",													"gui.viewmode.solid" },
		};
		
		private static Dictionary<string,string> dictTrans_OptsCmd_MeshDelete = new Dictionary<string, string>()
		{
			{ "type='ALL'", "mesh.delete_all" },
			{ "type='VERT'", "mesh.delete_vert" },
			{ "type='EDGE'", "mesh.delete_edge" },
			{ "type='FACE'", "mesh.delete_face" },
		};
		
		#endregion
		
		#region Known Commands (both, Handled and Ignored)
		
		private static string[] BlenderCommandsView = {
			"view3d.move", "view3d.rotate", "view3d.view_persportho",
			"view3d.viewnumpad", "view3d.view_orbit", "view3d.view_selected",
			"view3d.view_pan", "view3d.smoothview", "view3d.zoom",
			"view3d.select_vertex_mode", "view3d.select_edge_mode", "view3d.select_face_mode",
			"view3d.all", "view3d.localview", "view3d.view_all", "view3d.zoom_border",
		};
		
		private static string[] BlenderCommandsUndoable = {
			// COMMANDS THAT DO NOT CHANGE THE VERTEX LIST
			"transform.resize", "transform.rotate", "transform.translate", "transform.shrink_fatten",
			"transform.edge_slide", "transform.tosphere", "transform.mirror",
			"transform.set_x", "transform.set_y", "transform.set_z",
			"mesh.edge_face_add",																// no new verts, just a new edge or face (of 3 or 4 verts)
			"mesh.hide", "mesh.reveal",
			"mesh.select_all", "mesh.select_none", "mesh.loop_select", "mesh.select_shortest_path", "mesh.edgering_select",
			"mesh.select_linked",
			"mesh.tris_convert_to_quads", "mesh.quads_convert_to_tris",
			"mesh.normals_make_consistent",
			"mode.select_vertex", "mode.select_edge", "mode.select_face",
			"object.hide_view_clear", "object.hide_view_set",
			"object.select_all", "object.select_inverse",
			"view3d.manipulator",
			"view3d.select", "view3d.select_border", "view3d.select_circle", "view3d.select_lasso",
			"view3d.select_vertex_mode", "view3d.select_edge_mode", "view3d.select_face_mode",
			
			// VERTICES / OBJECTS ARE DELETED
			//"mesh.delete",																		// verts are removed.  inds are shifted down
			"mesh.delete_all", "mesh.delete_vert", "mesh.delete_edge", "mesh.delete_face",
			"object.delete",																	// object is removed
			
			// VERTICES / OBJECTS ARE ADDED
			"mesh.duplicate", "mesh.duplicate_move",
			"mesh.loopcut", "mesh.loopcut_slide",
			"mesh.primitive_circle_add", "mesh.primitive_cone_add", "mesh.primitive_cube_add",
			"mesh.primitive_cylinder_add", "mesh.primitive_grid_add", "mesh.primitive_ico_sphere_add",
			"mesh.primitive_monkey_add", "mesh.primitive_plane_add", "mesh.primitive_torus_add",
			"mesh.primitive_uv_sphere_add",
			"mesh.subdivide",																	// adds and selects new verts, keeping prev verts selected
			"object.duplicate",																	// copies object
			"object.duplicate_move",															// copies object
			
			// SPECIAL CASES ( VERTS ARE ADDED AND *POTENTIALLY* DELETED )
			"mesh.extrude", "mesh.extrude_edges_move", "mesh.extrude_faces_move",				// n new verts are added, where n is the number of selected verts in step0
			"mesh.extrude_region_move", "mesh.extrude_repeat", "mesh.extrude_vertices_move",
			"mesh.merge",																		// verts are merged (removed).  inds are shifted down?
			"object.join",
			
			"object.modifier_add",
		};
		
		private static string[] BlenderCommandsSpecial = {
			// THE VERY FIRST SNAPSHOT
			"begin",
			
			// SPECIAL-SPECIAL CASES
			//"undo", "redo",
			"ed.undo","ed.redo",
			
			// COMMANDS THAT DO NOT CHANGE THE VERTEX LIST
			"mode.toggle_edit_mode",
			"mode.toggle_opaque",
			"object.modifier_add", "object.modifier_apply", "object.modifier_remove",
			"modifier.toggle_mirror_x", "modifier.toggle_mirror_y", "modifier.toggle_mirror_z",
			"modifier.set_merge_limit",
		};
		
		// may be some overlap among the commands
		private static string[][] BlenderCommandsHandledArray = {
			BlenderCommandsSpecial,
			BlenderCommandsView,
			BlenderCommandsUndoable,
		};
		
		// ********************************************************
		// NOTE: changes here require the data to be reprocessed!!!
		// ********************************************************
		private static string[] BlenderCommandsIgnorable = {
			"ignore",
			
			// UNHANDLED CASES; KNOW TO BE IGNORABLE
			"console.autocomplete", "console.banner", "console.delete", "console.execute",
			"console.history_append", "console.history_cycle", "console.insert", "console.move",
			"console.scrollback_append", "console.select_set",
			//"ed.redo",																			// => redo; this can be called too many times
			//"ed.undo",																			// => undo; this can be called too many times
			"undo", "redo",
			"file.execute", "file.highlight", "file.select", "image.open",
			"info.reports_display_update",
			"object.editmode_toggle",																// => mode.toggle_edit_mode; this is called when continuing from edit_mode state
			"mode.toggle_manipulate_centers", "mode.toggle_show_mod_in_edit",
			"mode.manipulator_normal", "mode.manipulator_global", "mode.manipulator_local", "mode.manipulator_view",
			"mode.scale_manipulator", "mode.translate_manipulator", "mode.rotate_manipulator",
			"screen.actionzone", "screen.area_move", "screen.region_scale", "screen.area_split", "screen.area_join",
			"view2d.pan", "view2d.scroller_activate", "view2d.scroll_up", "view2d.scroll_down", "view2d.scroll_left", "view2d.scroll_right",
			"view3d.cursor3d", "view3d.properties",
			"view3d.snap_cursor_to_selected", "view3d.snap_cursor_to_center", "view3d.snap_cursor_to_grid",
			
			"wm.context_toggle_enum", "wm.context_set_enum", "wm.context_cycle_enum",
			"screen.screen_full_area",
			
			"object.origin_set", "object.scale_clear",
			
			"view3d.edit_mesh_extrude_move_normal",
			"file.parent", "file.select_execute", "view3d.background_image_add",
			"gui.viewmode.solid", "gui.viewmode.wireframe",
			"params.size", "modifier.toggle_clipping",
			
			// UNHANDLED CASES; NEED TO HANDLE?
			"view3d.add_background_image", "view3d.remove_background_image",
			
			// was handled, skipping now
			"begin", "object.mode_set", "mode.toggle_edit_mode", 
			
			"mode.translate_manipulator", "mode.rotate_manipulator", "mode.scale_manipulator", "mode.manipulator_normal",
			"mode.manipulator_global", "mode.manipulator_local", "mode.manipulator_view", "mode.toggle_manipulate_centers",
			"mode.toggle_3d_manipulator", "gui.manipulator.toggle_visible",
			
			"view3d.layers", "uv.unwrap",
		};
		
		private static string[] BlenderInterfaceOptionsIgnorable = {
			"Individual Origins", "Median Point", "Active Element", "3D Cursor", "3D View",
			"Children expanded in the user interface",
			"Expanded in the user interface",
			"Input", "User Preferences",
			"Show key map event and property details in the user interface",
			"Display reference images behind objects in the 3D View",
			"Show the expanded in the user interface",
			"Top", "Right", "Left", "Front",
			"Disable", "Edges",
			"Realtime display of a modifier",
			"Bounding Box",
		};
		
		private static string[] BlenderCommandsObjectList = {			// command that change the list of objects
			"object.delete",
			"object.duplicate",																	// copies object
			"object.duplicate_move",															// copies object
			"object.join",
			
			// the following adds as new object if not in edit mode; otherwise, the primitive is added to edited object
			"mesh.primitive_circle_add", "mesh.primitive_cone_add", "mesh.primitive_cube_add",
			"mesh.primitive_cylinder_add", "mesh.primitive_grid_add", "mesh.primitive_ico_sphere_add",
			"mesh.primitive_monkey_add", "mesh.primitive_plane_add", "mesh.primitive_torus_add",
			"mesh.primitive_uv_sphere_add",
		};
		
		private static string[] BlenderCommandsNoChange = {
			"view3d.move", "view3d.rotate", "view3d.view_persportho", "view3d.view_all",
			"view3d.viewnumpad", "view3d.view_orbit", "view3d.view_selected",
			"view3d.view_pan", "view3d.smoothview", "view3d.zoom", "view3d.all", "view3d.localview", "view3d.zoom_border",
			"console.autocomplete", "console.banner", "console.delete", "console.execute",
			"console.history_append", "console.history_cycle", "console.insert", "console.move",
			"console.scrollback_append", "console.select_set",
			"undo", "redo",
			"file.execute", "file.highlight", "file.select", "image.open",
			"info.reports_display_update",
			"object.editmode_toggle",																// => mode.toggle_edit_mode; this is called when continuing from edit_mode state
			"mode.toggle_manipulate_centers", "mode.toggle_show_mod_in_edit",
			"mode.manipulator_normal", "mode.manipulator_global", "mode.manipulator_local", "mode.manipulator_view",
			"mode.scale_manipulator", "mode.translate_manipulator", "mode.rotate_manipulator",
			"screen.actionzone", "screen.area_move", "screen.region_scale", "screen.area_split", "screen.area_join",
			"view2d.pan", "view2d.scroller_activate", "view2d.scroll_up", "view2d.scroll_down",
			"view3d.cursor3d", "view3d.properties",
			"view3d.snap_cursor_to_selected",
			"view3d.add_background_image", "view3d.remove_background_image"
		};
		
		#endregion
		
		#region History Construction
		// reverse engineer the ops to figure out how the verts of the current snapshot relates to the verts of the next snapshot
		
		private void LoadSteps( string sStepsFilename )
		{
			nsnapshots = 0;
			int skipped = 0;
			int overwrote = 0;
			List<SnapshotScene> lstscenes = new List<SnapshotScene>();
			SnapshotScene tvms;
			
			string path = Path.GetDirectoryName( sStepsFilename );
			
			using( Stream s = new FileStream( sStepsFilename, FileMode.Open ) )
			{
				TextReader tr = new StreamReader( s );
				string l;
				while( (l = tr.ReadLine()) != null )
				{
					string[] parts = l.SplitOnce( ':' ); //l.Split( frdelim );
					string file = Path.Combine( path, parts[0].Trim() + ".ply" );
					string[] reason = parts[1].SplitOnce( '(' ); //parts[1].Split( rdelim );
					
					string command = reason[0].Trim();
					string options = ( reason.Length == 2 ? reason[1].Substring( 0, reason[1].Length - 1 ).Trim() : "" );
					
					if( command == "interface" )
					{
						float f;
						string nqopts = options.Substring( 1, options.Length - 2 );
						if( Single.TryParse( nqopts, out f ) ) {
							
							int isnapshot;
							for( isnapshot = nsnapshots - 1; isnapshot >= 0; isnapshot-- )
							{
								string cmd = lstscenes[isnapshot].command;
								if( cmd.StartsWith( "mesh.primitive_" ) ) break;
								if( !BlenderCommandsNoChange.Contains( cmd ) ) { isnapshot = -1; break; }
							}
							
							if( isnapshot >= 0 ) {
								
								SnapshotScene lastmesh = lstscenes[isnapshot];
								System.Console.WriteLine( "Overwriting mesh in " + isnapshot + ":" + (nsnapshots-1) + ": " + lastmesh.command + " with interface(" + f + ")" );
								tvms = new SnapshotScene( file, isnapshot, lastmesh.command, lastmesh.opts, null, false, true );
								
								tvms.command = lstscenes[isnapshot].command;
								tvms.timeindex = lstscenes[isnapshot].timeindex;
								lstscenes.RemoveAt( isnapshot );
								lstscenes.Insert( isnapshot, tvms );
								overwrote++;
								
								// update following scenes that don't change geometry (view ops, etc)
								for( int isnapshot2 = isnapshot + 1; isnapshot2 < nsnapshots; isnapshot2++ )
								{
									SnapshotScene scene = lstscenes[isnapshot2];
									//SnapshotModel[] models = scene.Models;
									System.Console.WriteLine( "updating {0} ( {1} {2} )", isnapshot2, tvms.nmodels, scene.nmodels );
									scene.SetPrevScene( lstscenes[isnapshot2-1] );
									/*for( int i = 0; i < scene.nmodels; i++ )
									{
										SnapshotModel[] omodels = tvms.Models;
										models[i].prev = omodels[i];
										models[i].nverts = omodels[i].nverts;
										models[i].selinds = omodels[i].selinds;
									}*/
								}
								continue;
								
							} else {
								
								System.Console.WriteLine( "interface(" + options + ") recognized, but could not find last added mesh" );
								
							}
							
						}
					}
					
					TranslateCommand( ref command, ref options );
					if( !IsAHandledCommand( command, options, file ) ) { skipped++; continue; }
					
					if( nsnapshots == 0 ) tvms = new SnapshotScene( file, nsnapshots, command, options, null, false, false );
					else if( BlenderCommandsNoChange.Contains( command ) ) {
						tvms = new SnapshotScene( file, nsnapshots, command, options, lstscenes[nsnapshots-1], true, false );
					} else {
						tvms = new SnapshotScene( file, nsnapshots, command, options, lstscenes[nsnapshots-1], false, BlenderCommandsObjectList.Contains( command ) );
					}
					
					TranslateCommand( tvms );
					
					lstscenes.Add( tvms );
					nsnapshots++;
					
					if( nsnapshots % 500 == 0 )
					{
						ForceGarbageCollection( false );
						GC.Collect();
						GC.WaitForPendingFinalizers();
						System.Console.WriteLine( "Loaded " + nsnapshots + " files" );
					}
				}
			}
			
			System.Console.WriteLine();
			System.Console.WriteLine();
			System.Console.WriteLine( "Statistics:" );
			System.Console.WriteLine( "    Total:     {0}", (skipped + nsnapshots + overwrote ) );
			System.Console.WriteLine( "    Skipped:   {0}", skipped );
			System.Console.WriteLine( "    Overwrote: {0}", overwrote );
			System.Console.WriteLine( "    Loaded:    {0}", nsnapshots );
			System.Console.WriteLine();
			
			snapshots = lstscenes.ToArray();
		}
		
		// removes unnecessary prefixes; translates certain commands
		// this might is a good place to translate commands (operations) from a variety of
		// applications (maya, 3dsmax, blender, etc.) into a common language
		private void TranslateCommand( ref string command, ref string opts )
		{
			//if( nsnapshots == 0 ) { command = "begin"; opts = ""; return; }
			
			if( command.StartsWith( "bpy.ops." ) ) command = command.Substring(8);
			
			if( command == "object.mode_set" )
			{
				command = dictTrans_OptsCmd_ObjectModeSet[opts];
				opts = "";
			}
			
			if( command == "mesh.delete" )
			{
				command = dictTrans_OptsCmd_MeshDelete[opts];
				opts = "";
			}
			
			if( command == "interface" )
			{
				opts = opts.Substring( 1, opts.Length - 2 ).Trim();
				
				if( dictTrans_OptsCmd_Interface.ContainsKey( opts ) ) {
					command = dictTrans_OptsCmd_Interface[ opts ];
					opts = "";
				} else if( opts.StartsWith( "X: " ) ) {
					command = "transform.set_x";
					opts = opts.Substring( 3 );
				} else if( opts.StartsWith( "Y: " ) ) {
					command = "transform.set_y";
					opts = opts.Substring( 3 );
				} else if( opts.StartsWith( "Z: " ) ) {
					command = "transform.set_z";
					opts = opts.Substring( 3 );
				} else if( opts.StartsWith( "Merge Limit: " ) ) {
					command = "modifier.set_merge_limit";
					opts = opts.Substring( 13 );
				} else if( opts.StartsWith( "Size: " ) ) {
					command = "params.size";
					opts = opts.Substring( "Size: ".Length );
				} else if( BlenderInterfaceOptionsIgnorable.Contains( opts ) ) {
					command = "ignore";
				} else {
					System.Console.WriteLine( "ModelingHistory.TranslateCommand: Unknown interface command \"" + opts + "\"" );
				}
			}
		}
		
		private void TranslateCommand( SnapshotScene scene )
		{
			if( scene.command == "mesh.select_all" )
			{
				if( scene.GetEditModel().selinds.Length == 0 ) scene.command = "mesh.select_none";
			}
		}
		
		private void TranslateCommandsToGeneric()
		{
			System.Console.Write( "Translating..." );
			foreach( SnapshotScene scene in snapshots )
			{
				if( !dictTrans_CmdCmd_BlenderToGeneric.ContainsKey( scene.command ) ) throw new Exception( "Unknown command: " + scene.command );
				string ncmd = dictTrans_CmdCmd_BlenderToGeneric[scene.command];
				scene.command = ncmd;
			}
			System.Console.WriteLine( "done" );
		}
		
		private bool IsAHandledCommand( string command, string options, string file )
		{
			if( BlenderCommandsIgnorable.Contains( command ) ) { return false; }
			
			foreach( string[] commands in BlenderCommandsHandledArray ) if( commands.Contains( command ) ) return true;
			
			// unhandled cases are reported in case one might be important
			System.Console.WriteLine( "Skipping unhandled command: " + command + "(" + options + ") in " + file );
			return false;
		}
		
		// account for undo and redo operations
		private void FillBaseModifyID()
		{
			basemodifyid = new int[nsnapshots];
			Stack<int> undo = new Stack<int>();
			Stack<int> redo = new Stack<int>();
			int iundo, ilastundo;
			
			basemodifyid[0] = 0;
			undo.Push( 0 );
			
			//System.Console.WriteLine( "0:0: begin" );
			for( int i = 1; i < nsnapshots; i++ )
			{
				string cmd = snapshots[i].command;
				
				if( BlenderCommandsUndoable.Contains(cmd) ) {
					basemodifyid[i] = undo.Peek();
					undo.Push( i );							// command is pushed on to undo stack
					if( cmd == "mode.select_vertex" || cmd == "mode.select_edge" || cmd == "mode.select_face" ) undo.Push(i);
					redo.Clear();							// performing an operation clears out redo stack
					//System.Console.WriteLine( "Undoable command pushed to Undo; Cleared Redo.  " + snapshots[i].file + ":" + snapshots[i].command );
				} else if( cmd == "ed.undo" ) {
					//iundo = undo.Pop();
					ilastundo = undo.Peek();
					iundo = undo.Peek();
					int skipped = 0;
					while( !snapshots[i].SuperficialComparison(snapshots[iundo]) ) {
						//System.Console.WriteLine( "skipping " + snapshots[iundo].file + " (" + snapshots[iundo].command + ")" );
						ilastundo = iundo;
						iundo = undo.Pop();
						skipped++;
						if( skipped == 25 ) throw new Exception( "ModelingHistory.FillBaseModifyID: Cannot find appropriate undo" );
					}
					if( !snapshots[i].SuperficialComparison(snapshots[iundo]) ) throw new Exception( "Sanity check failed" );
					//System.Console.WriteLine( "Undo command found ("+snapshots[i].file+"). Rolled back to: " + snapshots[iundo].file + ":" + snapshots[iundo].command );
					//System.Console.WriteLine( "    from " + i + " to " + iundo );
					redo.Push( ilastundo );						// remember what is undone, in case we redo it
					undo.Push( iundo );
					basemodifyid[i] = iundo;
					
				} else if( cmd == "ed.redo" ) {
					undo.Push( redo.Pop() );				// remember what is redone, in case we undo it
					basemodifyid[i] = undo.Peek();
					System.Console.WriteLine( "Redo command found ("+snapshots[i].file+"). Rolled forward to: " + snapshots[basemodifyid[i]].file + ":" + snapshots[basemodifyid[i]].command );
				} else {
					basemodifyid[i] = undo.Peek();			// non-undoable commands are not pushed onto undo stack
				}
				//System.Console.WriteLine( snapshots[i].file + " (" + snapshots[i].command + ") => " + snapshots[basemodifyid[i]].file + " (" + snapshots[basemodifyid[i]].command + ")" );
			}
			System.Console.WriteLine();
		}
		
		// assign uids for objects
		private void AssignObjectUIDs()
		{
			nuobjs = 0;
			
			snapshots[0].ObjectUIDs = new int[snapshots[0].nmodels];
			for( int i = 0; i < snapshots[0].nmodels; i++ )
				snapshots[0].ObjectUIDs[i] = (nuobjs++);
			
			for( int isnapshot = 1; isnapshot < nsnapshots; isnapshot++ )
			{
				SnapshotScene curscene = snapshots[isnapshot];
				SnapshotScene prevscene = snapshots[isnapshot-1]; //snapshots[basemodifyid[isnapshot]];
				
				List<int> curuids = new List<int>( prevscene.ObjectUIDs );
				
				switch( curscene.command )
				{
				
				case "object.delete":													// selected objects are deleted
					for( int i = 0, t = 0; i < prevscene.nmodels; i++ )
					{
						if( prevscene.Models[i].objselected ) curuids.RemoveAt( t );
						else t++;
					}
					break;
				
				case "mesh.primitive_circle_add":
				case "mesh.primitive_cone_add":
				case "mesh.primitive_cube_add":
				case "mesh.primitive_cylinder_add":
				case "mesh.primitive_grid_add":
				case "mesh.primitive_ico_sphere_add":
				case "mesh.primitive_monkey_add":
				case "mesh.primitive_plane_add":
				case "mesh.primitive_torus_add":
				case "mesh.primitive_uv_sphere_add":
					if( curscene.InEditMode() ) break;									// if in edit mode, the primitive mesh is added to the active object
					curuids.Add( nuobjs++ );
					break;
				
				case "object.duplicate":												// copy the selected objects
				case "object.duplicate_move":
					for( int i = 0; i < prevscene.nmodels; i++ )
						if( prevscene.Models[i].objselected ) curuids.Add( nuobjs++ );
					break;
					
				case "object.join":
					// very special case.  delete selected objects; add one object (which contains all of the verts from the selected objects including the vertuids information!!)
					throw new Exception( "ModelingHistory: Unimplemented command object.join" );
				
				}
				
				if( curuids.Count != curscene.nmodels )
				{
					System.Console.WriteLine( "\n\n\nObject Accounting Error" );
					System.Console.WriteLine( "prevscene.nmodels  = " + prevscene.nmodels );
					System.Console.WriteLine( "prevscene.sel.Len  = " + prevscene.SelectedObjects.Length );
					System.Console.WriteLine( "prevscene.file     = " + prevscene.file + ": " + prevscene.command );
					System.Console.WriteLine( "prevscene.editmode = " + prevscene.InEditMode() );
					System.Console.WriteLine( "curscene.nmodels   = " + curscene.nmodels );
					System.Console.WriteLine( "curscene.file      = " + curscene.file + ": " + curscene.command );
					System.Console.WriteLine( "curscene.editmode  = " + curscene.InEditMode() );
					System.Console.WriteLine( "curuids.Count      = " + curuids.Count );
					
					throw new Exception( "Expected: curuids.Count == curscene.nmodels" );
				}
				
				curscene.ObjectUIDs = curuids.ToArray();
			}
		}
		
		private void AssignVertexUIDs()
		{
			bool first;
			bool[] delete;
			List<int> vertuids;
			
			nuverts = 0;
			
			for( int iobjuid = 0; iobjuid < nuobjs; iobjuid++ )
			{
				first = true;
				vertuids = new List<int>();
				for( int isnapshot = 0; isnapshot < nsnapshots; isnapshot++ )
				{
					SnapshotScene curscene = snapshots[isnapshot];
					SnapshotScene prevscene = snapshots[basemodifyid[isnapshot]];
					
					SnapshotModel model = curscene.GetObjectFromUID( iobjuid );
					if( model == null ) continue;
					
					if( first )
					{
						// first instance, create the verts
						vertuids = new List<int>( model.nverts );
						for( int i = 0; i < model.nverts; i++ )
							vertuids.Add( nuverts++ );
						
						model.vertuids = vertuids.ToArray();
						
						first = false;
						continue;
					}
					
					SnapshotModel prevmodel = prevscene.GetObjectFromUID( iobjuid );
					int[] prevvertuids = prevmodel.vertuids;
					vertuids = new List<int>( prevvertuids );
					
					if( !model.objedit ) 										// nothing happens if not editting the object of interest
					{
						model.vertuids = vertuids.ToArray();
						continue;
					}
					
					switch( curscene.command )
					{
					
					// VERTICES ARE DELETED
					case "mesh.delete_face":									// verts are removed.  inds are shifted down
					case "mesh.delete_edge":
						// any selected vertex that is not connected to a non-selected vertex is deleted
						
						// mark for deletion
						delete = new bool[prevmodel.nverts];
						for( int i = 0; i < prevmodel.nverts; i++ ) if( prevmodel.isvertselected[i] ) delete[i] = true;
						
						// check connections to non-selected verts
						for( int i = 0; i < prevmodel.groups[1].Count; i++ )
						{
							int i0 = prevmodel.groups[1][i][0];
							int i1 = prevmodel.groups[1][i][1];
							if( prevmodel.isvertselected[i0] && !prevmodel.isvertselected[i1] ) delete[i0] = false;
							if( prevmodel.isvertselected[i1] && !prevmodel.isvertselected[i0] ) delete[i1] = false;
						}
						for( int i = 0; i < prevmodel.groups[2].Count; i++ )
						{
							int i0 = prevmodel.groups[2][i][0];
							int i1 = prevmodel.groups[2][i][1];
							int i2 = prevmodel.groups[2][i][2];
							if( prevmodel.isvertselected[i0] && ( !prevmodel.isvertselected[i1] || !prevmodel.isvertselected[i2] ) ) delete[i0] = false;
							if( prevmodel.isvertselected[i1] && ( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i2] ) ) delete[i1] = false;
							if( prevmodel.isvertselected[i2] && ( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i1] ) ) delete[i2] = false;
						}
						for( int i = 0; i < prevmodel.groups[3].Count; i++ )
						{
							int i0 = prevmodel.groups[3][i][0];
							int i1 = prevmodel.groups[3][i][1];
							int i2 = prevmodel.groups[3][i][2];
							int i3 = prevmodel.groups[3][i][3];
							if( prevmodel.isvertselected[i0] && ( !prevmodel.isvertselected[i1] || !prevmodel.isvertselected[i2] || !prevmodel.isvertselected[i3] ) ) delete[i0] = false;
							if( prevmodel.isvertselected[i1] && ( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i2] || !prevmodel.isvertselected[i3] ) ) delete[i1] = false;
							if( prevmodel.isvertselected[i2] && ( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i1] || !prevmodel.isvertselected[i3] ) ) delete[i2] = false;
							if( prevmodel.isvertselected[i3] && ( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i1] || !prevmodel.isvertselected[i2] ) ) delete[i3] = false;
						}
						
						for( int i = 0, iremove = 0; i < prevmodel.nverts; i++ )
						{
							if( delete[i] ) vertuids.RemoveAt( iremove );
							else iremove++;
						}
						break;
					
					case "mesh.delete_vert":
						for( int i = 0, iremove = 0; i < prevmodel.nverts; i++ )
						{
							if( prevmodel.isvertselected[i] ) vertuids.RemoveAt( iremove );
							else iremove++;
						}
						break;
					
					case "mesh.delete_all":
						vertuids.Clear();
						break;
					
					// VERTICES ARE ADDED
					case "mesh.duplicate":
					case "mesh.duplicate_move":
					case "mesh.loopcut":
					case "mesh.loopcut_slide":
					case "mesh.primitive_circle_add":
					case "mesh.primitive_cone_add":
					case "mesh.primitive_cube_add":
					case "mesh.primitive_cylinder_add":
					case "mesh.primitive_grid_add":
					case "mesh.primitive_ico_sphere_add":
					case "mesh.primitive_monkey_add":
					case "mesh.primitive_plane_add":
					case "mesh.primitive_torus_add":
					case "mesh.primitive_uv_sphere_add":
					case "mesh.subdivide":					// adds and selects new verts, but keeps prev verts selected
						for( int i = prevmodel.nverts; i < model.nverts; i++ )
							vertuids.Add( nuverts++ );
						//for( int i = 0; i < model.nverts; i++ )
						//{
						//	if( model.isvertselected[i] && (i >= prevmodel.nverts || !prevmodel.isvertselected[i] ) ) vertuids.Add( (nuverts++) );
						//}
						break;
						
						
					// SPECIAL CASES
					case "mesh.extrude":
					case "mesh.extrude_edges_move":
					case "mesh.extrude_faces_move":
					case "mesh.extrude_region_move":
					case "mesh.extrude_repeat":
					case "mesh.extrude_vertices_move":		// n new verts are added, where n is the number of selected verts in step0
						// if all verts of a part are selected, and the part has no volume, extrude solidifies
						// if all verts of a part are selected, and the part has a volume, extrude makes a copy
						// either way, the entire part is duplicated.
						bool partallselected = true;
						
						for( int iedge = 0; iedge < prevmodel.groups[1].Count; iedge++ )
						{
							int i0 = prevmodel.groups[1][iedge][0];
							int i1 = prevmodel.groups[1][iedge][1];
							bool s0 = prevmodel.isvertselected[i0];
							bool s1 = prevmodel.isvertselected[i1];
							if( !s0 && !s1 ) continue;
							//if( ( s0 && !s1 ) || ( s1 && !s0 ) ) { partallselected = false; break; }
							if( !s1 || !s0 ) { partallselected = false; break; }
							if( !prevmodel.groupselected[1][iedge] ) { partallselected = false; break; }
						}
						if( partallselected )
						{
							// add new verts
							for( int i = 0; i < model.nverts; i++ )
							{
								if( model.isvertselected[i] ) vertuids.Add( (nuverts++) );
							}
							break;
						}
						
						// initially mark selected verts for deletion
						delete = prevmodel.isvertselected.CloneArray();
						bool mirrorx = false;
						bool mirrory = false;
						bool mirrorz = false;
						float threshold = 0.0f; //assuming only one value for threshold  (this is a BAD assumption; ie: a hack)
						
						foreach( Modifier mod in prevmodel.modifiers )
						{
							if( mod.type != Modifiers.Mirror ) continue;
							ModifierMirror modmir = (ModifierMirror) mod;
							mirrorx |= modmir.usex;
							mirrory |= modmir.usey;
							mirrorz |= modmir.usez;
							threshold = modmir.mergethreshold;
						}
						
						// "inland" verts are deleted!
						// terribly slow, but it gets the job done
						for( int i = 0; i < prevmodel.nverts; i++ )
						{
							if( !prevmodel.isvertselected[i] ) continue;
							
							for( int iedge = 0; iedge < prevmodel.groups[1].Count; iedge++ )
							{
								int i0 = prevmodel.groups[1][iedge][0];
								int i1 = prevmodel.groups[1][iedge][1];
								if( i0 != i && i1 != i ) continue;
								
								if( !prevmodel.groupselected[1][iedge] )
								//if( !prevmodel.isvertselected[i0] || !prevmodel.isvertselected[i1] )
								{
									delete[i] = false;
									break;
								}
								
								// found an edge with vert i
								// if edge is not shared by 2 faces, then vert i is not deleted
								
								int count = 0;
								Vec3f[] verts = prevmodel.GetVerts();
								if( mirrorx && Math.Abs( verts[i0].x ) <= threshold && Math.Abs( verts[i1].x ) <= threshold ) count++;
								if( mirrory && Math.Abs( verts[i0].y ) <= threshold && Math.Abs( verts[i1].y ) <= threshold ) count++;
								if( mirrorz && Math.Abs( verts[i0].z ) <= threshold && Math.Abs( verts[i1].z ) <= threshold ) count++;
								
								for( int itri = 0; itri < prevmodel.groups[2].Count; itri++ )
								{
									int a = prevmodel.groups[2][itri][0];
									int b = prevmodel.groups[2][itri][1];
									int c = prevmodel.groups[2][itri][2];
									if( a == b || a == c || b == c ) System.Console.WriteLine( "duplicate verts in tri" );
									if(
									   a == i0 && ( b == i1 || c == i1 ) ||
									   b == i0 && ( a == i1 || c == i1 ) ||
									   c == i0 && ( a == i1 || b == i1 )
									   ) 
									{
										if( !prevmodel.groupselected[2][itri] ) {
											delete[i] = false;
											break;
										}
										//if( !prevmodel.isvertselected[a] || !prevmodel.isvertselected[b] || !prevmodel.isvertselected[c] )
										//{
										//	delete[i] = false;
										//	break;
										//}
										count++;
									}
								}
								if( !delete[i] ) break;
								
								for( int iquad = 0; iquad < prevmodel.groups[3].Count; iquad++ )
								{
									int a = prevmodel.groups[3][iquad][0];
									int b = prevmodel.groups[3][iquad][1];
									int c = prevmodel.groups[3][iquad][2];
									int d = prevmodel.groups[3][iquad][3];
									if( a == b || a == c || a == d || b == c || b == d || c == d ) System.Console.WriteLine( "duplicate verts in quad" );
									if(
									   a == i0 && ( b == i1 || c == i1 || d == i1 ) ||
									   b == i0 && ( a == i1 || c == i1 || d == i1 ) ||
									   c == i0 && ( a == i1 || b == i1 || d == i1 ) ||
									   d == i0 && ( a == i1 || b == i1 || c == i1 )
									   ) 
									{
										if( !prevmodel.groupselected[3][iquad] ) {
											delete[i] = false;
											break;
										}
										//if( !prevmodel.isvertselected[a] || !prevmodel.isvertselected[b] || !prevmodel.isvertselected[c] || !prevmodel.isvertselected[d] )
										//{
										//	delete[i] = false;
										//	break;
										//}
										count++;
									}
								}
								if( count > 2 ) System.Console.WriteLine( "non-manifold detected (i="+i+",count="+count+")" );
								if( count < 2 )
								{
									delete[i] = false;
								}
								if( !delete[i] ) break;
							}
						}
						
						int cremoved = 0, cadded = 0;
						for( int i = 0, iremove = 0; i < prevmodel.nverts; i++ )
						{
							if( delete[i] ) { vertuids.RemoveAt( iremove ); cremoved++; }
							else iremove++;
						}
						
						// add new verts
						for( int i = 0; i < model.nverts; i++ )
						{
							if( model.isvertselected[i] ) { vertuids.Add( (nuverts++) ); cadded++; }
						}
						
						//System.Console.WriteLine( "prev {0}, removed {1}, added {2}", prevmodel.nverts, cremoved, cadded );
						
						break;
						
					case "mesh.merge":						// verts are merged (removed).  inds are shifted down?
						// remove selected verts in step0, add selected verts in step1
						bool firstdel = true;
						for( int i = 0, iremove = 0; i < prevmodel.nverts; i++ )
						{
							if( prevmodel.isvertselected[i] ) {
								if( firstdel ) { vertuids.Insert( i, (nuverts++) ); iremove++; firstdel = false; }
								vertuids.RemoveAt( iremove );
							} else iremove++;
						}
						//vertuids.Add( (nuverts++) );
						break;
							
					}
					
					if( vertuids.Count != model.nverts )
					{
						System.Console.WriteLine( "\n\n\nVertex Accounting Error" );
						System.Console.WriteLine( "prevscene.file = " + prevscene.file + ": " + prevscene.command );
						System.Console.WriteLine( "curscene.file  = " + curscene.file + ": " + curscene.command );
						System.Console.WriteLine( "prev count         = " + prevmodel.nverts );
						System.Console.WriteLine( "cur count (actual) = " + model.nverts );
						System.Console.WriteLine( "cur count (est)    = " + vertuids.Count );
						
						throw new Exception( "Expected: vertuids.Count == model.nverts" );
					}
					
					model.vertuids = vertuids.ToArray();
				}
			}
		}
		
		#endregion
	}
}

