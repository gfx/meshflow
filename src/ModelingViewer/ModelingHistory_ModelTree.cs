using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using Common.Libs.VMath;
using Common.Libs.MiscFunctions;
using Common.Libs.MatrixMath;

namespace ModelingViewer
{
	public partial class ModelingHistory
	{
		
		private static string rxgui = @"(gui\.\S+ )+";
		private static string rxview = @"(view\.\S+ )+";
		private static string rxselect = @"(select\.\S+ )+";
		//private static string rxview = @"(((view3d\.(zoom|rotate|move|smoothview|view_(orbit|pan|persportho)|viewnumpad))|(mesh\.(hide|reveal))) )+";
		//private static string rxselect = @"(((selections)|(view3d\.(select|select_(border|circle|lasso)))|(mesh\.(select_(all|shortest_path)|loop_select|edgering_select))) )+";
		private static string rxtrans = @"(transform\.\S+ )+";
		private static string rxtopos = @"(topo\.\S+ )+";
		private static string rxundo = @"((ed\.undo) )+";
		
		// RegExGrouping elements
		// 0: the regex to match
		// 1: label assigned to each cluster group
		// 2: command assigned to each cluster group
		// 3: delimiter to use when joining the operators into a single string
		public static string[] RegExGrouping_GUI = { rxgui, "gui", "gui", " " };
		public static string[] RegExGrouping_Views = { rxview, "view", "view", " " };
		public static string[] RegExGrouping_Selections = { rxselect, "select", "select", " " };
		public static string[] RegExGrouping_Transforms = { rxtrans, "transform", "transform", " " };
		public static string[] RegExGrouping_Topos = { rxtopos, "topos", "topos", " " };
		public static string[] RegExGrouping_SelectTransforms = { @"((select )(transform ))+", "selecttransforms", "selecttransforms", " " };
		public static string[] RegExGrouping_SelectTopo = { @"((select )(topo(\.\S+)? ))", "topo", "topo", " " };
		public static string[] RegExGrouping_Undos = { rxundo, "undo", "undo", " " };
		
		public IndexedViewableAlpha GetViewable( ModelTree t, bool highlighted, bool usefinalpos )
		{
			IndexedViewableAlpha v = t.GetViewable();
			
			if( highlighted ) v = Viewable_Highlight( v );
			if( usefinalpos ) v = Viewables_FinalPositions( v );
			
			return v;
		}
		
		public ModelTreeRoot PermanentlyAlter( ModelTreeRoot root )
		{
			List<ModelTreeSingle> lst = root.FlattenUnfiltered();
			var newsnapshots = lst.Select( (ModelTreeSingle n) => n.snapshot );
			
			snapshots = newsnapshots.ToArray();
			nsnapshots = snapshots.Length;
			basemodifyid = new int[nsnapshots]; for( int i = 0; i < nsnapshots; i++ ) basemodifyid[i] = Math.Max( 0, i - 1 );
			SetModelPrevs();
			FindFinalPositions();
			
			return Unfilter();
		}
		
		public void PrintModelTreeRootInformation( ModelTreeRoot root )
		{
			System.Console.WriteLine( "ModelTreeRoot Information" );
			System.Console.WriteLine( "root.lst.Count = " + root.lst.Count );
			foreach( ModelTree node in root.lst )
			{
				System.Console.Write( node.command + ": " );
				PrintModelTreeInformation( node );
				System.Console.WriteLine();
			}
		}
		
		public void PrintModelTreeInformation( ModelTree node )
		{
			if( node is ModelTreeSingle ) { System.Console.Write( ((ModelTreeSingle)node).snapshot.timeindex ); }
			if( node is ModelTreeFilter ) { System.Console.Write( "f( " ); PrintModelTreeInformation( ((ModelTreeFilter)node).node ); System.Console.Write( " )" ); }
			if( node is ModelTreeSummary )
			{
				System.Console.Write( "< " );
				foreach( ModelTree n in ((ModelTreeSummary)node).lst ) { PrintModelTreeInformation( n ); System.Console.Write( " " ); }
				System.Console.Write( ">" );
			}
		}
		
		public void PrintModelTreeInformationDetailed( ModelTree node ) { PrintModelTreeInformationDetailed( node, "" ); }
		
		private void PrintModelTreeInformationDetailed( ModelTree node, string inset )
		{
			if( node is ModelTreeSingle ) {
				System.Console.WriteLine( inset + ((ModelTreeSingle)node).snapshot.timeindex + ": " + node.label + ", " + node.command );
			} else if( node is ModelTreeFilter ) {
				System.Console.WriteLine( inset + "filtered: " + node.label + ", " + node.command + ":" );
				PrintModelTreeInformationDetailed( ((ModelTreeFilter)node).node, inset + "  " );
			} else if( node is ModelTreeSummary ) {
				ModelTreeSummary s = (ModelTreeSummary) node;
				System.Console.WriteLine( inset + "summary: " + node.label + ", " + node.command + ":" );
				System.Console.WriteLine( inset + s.composition ); 
				foreach( ModelTree n in s.lst )
				{
					PrintModelTreeInformationDetailed( n, inset + "  " );
				}
			}
		}
		
		#region Filter Functions
		
		public ModelTreeRoot Unfilter()
		{
			List<ModelTree> lst = new List<ModelTree>();
			for( int i = 0; i < nsnapshots; i++ ) lst.Add( new ModelTreeSingle( snapshots[i] ) );
			return new ModelTreeRoot( lst );
		}
		
		public ModelTreeRoot FilterByFunc( ModelTreeRoot root, Func<ModelTree,bool> filterfunc )
		{
			List<ModelTree> lst = new List<ModelTree>();
			
			foreach( ModelTree t in root.lst )
			{
				if( !filterfunc( t ) ) lst.Add( t );
				else lst.Add( new ModelTreeFilter( t ) );
			}
			
			ModelTreeRoot nroot = new ModelTreeRoot( lst );
			
			System.Console.WriteLine( "Filter Statistics" );
			System.Console.WriteLine( "    Before: {0}", root.lst.Count );
			System.Console.WriteLine( "    After:  {0}", nroot.lst.Count );
			
			return nroot;
		}
		
		public ModelTreeRoot FilterViewOps( ModelTreeRoot root )
		{
			return FilterByFunc( root, delegate( ModelTree t ) {
				return t.command.StartsWith( "view" );
			} );
		}
		
		public ModelTreeRoot FilterGUIOps( ModelTreeRoot root )
		{
			return FilterByFunc( root, delegate( ModelTree t ) {
				return t.command.StartsWith( "gui" );
			} );
		}
		
		public ModelTreeRoot FilterSelectOps( ModelTreeRoot root )
		{
			return FilterByFunc( root, delegate( ModelTree t ) {
				return t.command == "select" || t.command.StartsWith( "select." );
			} );
		}
		
		public ModelTreeRoot FilterEditOps( ModelTreeRoot root )
		{
			ModelTreeRoot nroot = FilterByFunc( root, delegate( ModelTree t ) {
				return !ContainsViewChangeCommand( t.command );
			} );
			
			System.Console.WriteLine( "Filter Statistics" );
			System.Console.WriteLine( "    Before: {0}", root.lst.Count );
			System.Console.WriteLine( "    After:  {0}", nroot.lst.Count );
			
			return nroot;
		}
		
		public ModelTreeRoot FilterTransforms( ModelTreeRoot root )
		{
			ModelTreeRoot nroot = FilterByFunc( root, delegate( ModelTree t ) {
				return ( t.command.StartsWith( "transform" ) );
			} );
			return nroot;
		}
		
		public ModelTreeRoot FilterNonHighlighted( ModelTreeRoot root )
		{
			ModelTreeRoot nroot = FilterByFunc( root, delegate( ModelTree t ) {
				IndexedViewableAlpha viewable = t.GetViewable();
				for( int i = 0; i < viewable.nVerts; i++ )
				{
					if( !viewable.Selected[i] ) continue;
					int uid = viewable.VertUIDs[i];
					if( highlighted[ uid ] != HighlightColors.None ) return false;
				}
				return true;
			} );
			
			System.Console.WriteLine( "Filter Statistics" );
			System.Console.WriteLine( "    Before: {0}", root.lst.Count );
			System.Console.WriteLine( "    After:  {0}", nroot.lst.Count );
			
			return nroot;
		}
		
		// does NOT handle redos!
		public ModelTreeRoot FilterUndos( ModelTreeRoot root )
		{
			List<ModelTree> lstnodes = new List<ModelTree>();
			
			for( int i = root.lst.Count - 1; i >= 0; )
			{
				if( root.lst[i].command == "gui.undo" )
				{
					int j = basemodifyid[i];
					for( ; i > j; i-- )
						lstnodes.Insert( 0, new ModelTreeFilter( root.lst[i] ) );
					//i = basemodifyid[i] + 1;
				}
				else
				{
					lstnodes.Insert( 0, root.lst[i] );
					i--;
				}
			}
			
			ModelTreeRoot nroot = new ModelTreeRoot( lstnodes );
			
			System.Console.WriteLine( "Filter Statistics" );
			System.Console.WriteLine( "    Before: {0}", root.lst.Count );
			System.Console.WriteLine( "    After:  {0}", nroot.lst.Count );
			
			return nroot;
		}
		
		private bool AllFiltered( ModelTree t )
		{
			if( t is ModelTreeFilter ) return true;
			if( t is ModelTreeSingle ) return false;
			ModelTreeSummary s = (ModelTreeSummary) t;
			foreach( ModelTree n in s.lst ) if( !AllFiltered( n ) ) return false;
			return true;
		}
		
		public ModelTreeRoot SmooshFiltered( ModelTreeRoot root )
		{
			List<ModelTree> lstnew = new List<ModelTree>();
			List<ModelTree> lstrun = new List<ModelTree>();
			ModelTree n;
			
			for( int i = 0; i < root.lst.Count; i++ )
			{
				n = root.lst[i];
				lstrun.Add( n );
				
				if( AllFiltered( n ) ) continue;
				
				if( lstrun.Count == 1 ) lstnew.Add( n );
				else lstnew.Add( new ModelTreeSummary( n.label, n.command, n.parameters, lstrun ));
				lstrun = new List<ModelTree>();
			}
			
			if( lstrun.Count == 1 ) lstnew.Add( lstrun[0] );
			else if( lstrun.Count > 1 ) { n = lstrun.Last(); lstnew.Add( new ModelTreeSummary( n.label, n.command, n.parameters, lstrun )); }
			
			return new ModelTreeRoot( lstnew );
		}
		
		#endregion
		
		#region Cluster Functions
		
		public ModelTreeRoot ClusterRepeats( ModelTreeRoot root )
		{
			string lcommand = null;
			List<ModelTree> repeats = new List<ModelTree>();
			List<ModelTree> nlst = new List<ModelTree>( root.lst.Count );
			
			foreach( ModelTree node in root.lst )
			{
				if( node.command != lcommand )
				{
					if( repeats.Count == 1 ) nlst.Add( repeats[0] );
					else if( repeats.Count > 1 ) nlst.Add( new ModelTreeSummary( lcommand, lcommand, "", repeats ) );
					repeats = new List<ModelTree>();
					lcommand = node.command;
				}
				repeats.Add( node );
			}
			
			if( repeats.Count == 1 ) nlst.Add( repeats[0] );
			else if( repeats.Count > 1 ) nlst.Add( new ModelTreeSummary( lcommand, lcommand, "", repeats ) );
			
			return new ModelTreeRoot( nlst );
		}
		
		public ModelTreeRoot ClusterBySelectMoves( ModelTreeRoot root )
		{
			ModelTreeRoot nroot = root;
			
			nroot = RegExGrouping( RegExGrouping_Selections, new SolidBrush( Color.Blue ), nroot );
			nroot = RegExGrouping( RegExGrouping_Transforms, new SolidBrush( Color.Red ), nroot );
			nroot = RegExGrouping( RegExGrouping_SelectTransforms, new SolidBrush( Color.Purple ), nroot );
			nroot = RegExGrouping( RegExGrouping_SelectTopo, new SolidBrush( Color.Cyan ), nroot );
			//nroot = RegExGrouping( RegExGrouping_Undos, nroot );
			
			System.Console.WriteLine( "Clustered!" );
			foreach( ModelTree t in nroot.lst )
				System.Console.WriteLine( t.command );
			System.Console.WriteLine();
			System.Console.WriteLine();
			System.Console.WriteLine();
			
			return nroot;
		}
		
		public ModelTreeRoot ClusterAll( ModelTreeRoot root )
		{
			List<ModelTree> lst = new List<ModelTree>();
			lst.Add( new ModelTreeSummary( "all", "all", "", root.lst ) );
			return new ModelTreeRoot( lst );
		}
		
		public ModelTreeRoot RegExGrouping( string[] sRegExGrouping, Brush brush, ModelTreeRoot root )
		{
			return RegExGrouping( sRegExGrouping, brush, SummaryCompositions.Last, root );
		}
		
		public ModelTreeRoot RegExGrouping( string[] sRegExGrouping, Brush brush, SummaryCompositions composition, ModelTreeRoot root )
		{
			return RegExGrouping( sRegExGrouping[0], sRegExGrouping[1], sRegExGrouping[2], sRegExGrouping[3], brush, composition, root );
		}
		
		public ModelTreeRoot RegExGrouping( string sRegEx, string sLabel, string sCommand, string delim, Brush brush, ModelTreeRoot root )
		{
			return RegExGrouping( sRegEx, sLabel, sCommand, delim, brush, SummaryCompositions.Last, root );
		}
		
		public ModelTreeRoot RegExGrouping( string sRegEx, string sLabel, string sCommand, string delim, Brush brush, SummaryCompositions composition, ModelTreeRoot root )
		{
			int i = 0;
			int mi = 0;
			int mei = 0;
			
			List<ModelTree> steps = new List<ModelTree>();
			Regex regex = new Regex( sRegEx );
			
			//InlinedInfo ii = InlineCommands( delim, root );
			string inlined; List<int> inds;
			InlineCommands( delim, root, out inlined, out inds );
			
			Match match = regex.Match( inlined );
			if( match.Success ) { mi = inds[match.Index]; mei = inds[match.Index + match.Length - 1]; }
			else { mi = root.lst.Count; mei = -1; }
			
			while( i < root.lst.Count )
			{
				for( ; i < mi; i++ )
				{
					steps.Add( root.lst[i] );
				}
				
				List<ModelTree> summary = new List<ModelTree>();
				for( ; i <= mei; i++ )
				{
					summary.Add( root.lst[i] );
				}
				if( summary.Count > 0 )
				{
					ModelTreeSummary newnode = new ModelTreeSummary( sLabel, sCommand, "newparams", summary );
					newnode.composition = composition;
					newnode.scrubbrush = brush;
					steps.Add( newnode );
				}
				
				match = match.NextMatch();
				if( match.Success ) { mi = inds[match.Index]; mei = inds[match.Index + match.Length - 1]; }
				else { mi = root.lst.Count; mei = -1; }
			}
			
			return new ModelTreeRoot( steps );
		}
		
		private void InlineCommands( string delim, ModelTreeRoot root, out string inlined, out List<int> indices )
		{
			string cmds = "";
			List<int> inds = new List<int>();
			
			root.lst.Each( delegate( ModelTree t, int ind ) {
				cmds += t.command + delim;
				inds.AddRange( Enumerable.Repeat( ind, t.command.Length + delim.Length ) );
			} );
			
			inlined = cmds;
			indices = inds;
		}
		
		#endregion
		
	}
}

