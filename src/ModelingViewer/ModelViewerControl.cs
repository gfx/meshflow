using System;
using System.ComponentModel;
using System.Xml;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;
using System.Timers;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Common.Libs.VMath;
using Common.Libs.MiscFunctions;
using Common.Libs.Image;
using Common.Libs.MatrixMath;


namespace ModelingViewer
{
	public enum ViewerMouseStates
	{
		Nothing,
		PanAndTiltCamera,
		OrbitOnlyCamera,
		OrbitAndDollyCamera,
		HighlightLine,
		HighlightSphere,
		TruckCamera,
		TruckCameraRightForward,
		Zoom,
		ResizeBrush,
	}
	
	public struct MouseStatus { public int x, y; public MouseButtons buttons; }
	
	public enum CameraSmoothing { Gaussian, Bilateral, Binary };
	
	public class ModelViewerControl : ViewerControl
	{
		protected ModelingHistory hist;
		protected Clustering layer;
		protected Cluster cluster;
		protected IndexedViewableAlpha[] viewables;
		protected CameraProperties[] cameras;
		protected string timestamp;
		
		private static int snapshotcount = 0;
		
		public float EaseSpeed = 0.03f;
		public float FixedCameraDist = 0.0f;
		
		//protected bool usefinalpos = false;
		
		#region Rendering Options
		
		protected int viewports = 1;
		
		protected Property<ViewerMouseStates> ToolSelection = new Property<ViewerMouseStates>( "Tool Selection", ViewerMouseStates.OrbitOnlyCamera);
		protected Property<MouseStatus> MouseState = new Property<MouseStatus>( "Mouse State", new MouseStatus() );
		protected MouseStatus LastMouseState = new MouseStatus();
		protected bool ignorestatechange = false;
		
		protected PropertyBag ViewerProperties = new PropertyBag();
		
		public PropertyBool RenderOptBoundingBox = new PropertyBool( "Render Bounding Box", true );
		public PropertyBool RenderOptOrigin = new PropertyBool( "Render Origin", false );
		//public PropertyBool RenderOptExtra = new PropertyBool( "Render Extra", true );
		public PropertyBool RenderOptTargetPosition = new PropertyBool( "Render Camera Target Position", false );
		
		public PropertyBool RenderOptGroupNumbers = new PropertyBool( "Render Vert Numbers", false );
		public PropertyBool RenderOptVTags = new PropertyBool( "Render VTags", false );
		public PropertyBool RenderOptTTags = new PropertyBool( "Render TTags", true );
		public PropertyBool RenderOptClusterInfo = new PropertyBool( "Render Cluster Information", false );
		
		public PropertyBool RenderOptSeparateViewports = new PropertyBool( "Render Viewables in Separate Viewports", false );
		public PropertyBool RenderOptGrid = new PropertyBool( "Render Grid", false );
		public PropertyBool RenderOptRotationGizmoOpt = new PropertyBool( "Render Rotation Gizmo", true );
		
		public PropertyBool RenderOptTransparentForeground = new PropertyBool( "Render Transparent Foreground", false );
		public PropertyBool RenderOptSkipReordering = new PropertyBool( "Skip Reordering", false );
		public PropertyBool RenderOptUseFinalPos = new PropertyBool( "Use Final Position", false );
		
		public PropertyBool RenderOptSelectedVerts = new PropertyBool( "Render Selected Verts", false );
		public PropertyBool RenderOptShowAnnotations = new PropertyBool( "Show Annotations", true );
		public PropertyBool RenderOptShowRemoved = new PropertyBool( "Show Removed", false );
		public PropertyBool RenderOptShowSelected = new PropertyBool( "Show Selected", false );
		public PropertyBool RenderOptWireframe = new PropertyBool( "Wireframe", false );
		
		public PropertyBool ViewEasing = new PropertyBool( "View Easing", true );
		public Property<ViewSelections> ViewSelection = new Property<ViewSelections>( "Camera Selection", ViewSelections.Artist );
		public CameraProperties ViewUser = new CameraProperties();
		public PropertyBool SmoothedCamera = new PropertyBool( "Smoothed Camera", false );
		public Property<CameraSmoothing> SmoothingMethod = new Property<CameraSmoothing>( "Smoothing Method", CameraSmoothing.Gaussian );
		public Property<float> SigmaT = new Property<float>( "Sigma T", 4.0f );
		public Property<float> SigmaX = new Property<float>( "Sigma X", 4.0f );
		public Property<float> BinEpsilon = new Property<float>( "Binary: Epsilon", 0.2f );
		public PropertyBool BinFromt0 = new PropertyBool( "Binary: From t0", false );
		public Property<float> BinWTar = new Property<float>( "Binary: Weight Target", 0.1f );
		public Property<float> BinWRot = new Property<float>( "Binary: Weight Rotation", 0.01f );
		public Property<float> BinWDis = new Property<float>( "Binary: Weight Distance", 0.1f );
		
		public PropertyConstrainedDouble BrushSize = new PropertyConstrainedDouble( "Brush Size", 0.50, 0.10, 1.00 );
		public PropertyConstrainedDouble BrushCollisionRatio = new PropertyConstrainedDouble( "Brush Collision Ratio", 0.25, 0.01, 1.00 );
		public Property<HighlightColors> BrushColor = new Property<HighlightColors>( "Brush Color", HighlightColors.Red ); 
		
		#endregion
		
		#region UI Objects
		
		protected ToolStripButton tsbOrbitDolly;
		protected ToolStripButton tsbOrbit;
		protected ToolStripButton tsbPan;
		protected ToolStripButton tsbTruck;
		protected ToolStripButton tsbHighlightLine;
		protected ToolStripButton tsbHighlightSphere;
		protected ToolStripButton tsbHighlightClear;
		
		protected ToolStripButton tsbNumberVerts;
		protected ToolStripButton tsbOrtho;
		
		protected ToolStripButton tsbAlignUp;
		
		protected ToolStripDropDownButton tsbView;
		protected ToolStripDropDown tsddView;
		protected ToolStripButton tsbViewArtist;
		protected ToolStripButton tsbViewBest;
		protected ToolStripButton tsbViewUser;
		protected ToolStripButton tsbViewSmoothNone;
		protected ToolStripButton tsbViewSmoothGaussian;
		protected ToolStripButton tsbViewSmoothBilateral;
		protected ToolStripButton tsbViewSmoothBinary;
		protected ToolStripButton tsbViewSigmaT;
		protected ToolStripButton tsbViewSigmaX;
		protected ToolStripButton tsbViewBinEpsilon;
		protected ToolStripButton tsbViewBinFromt0;
		protected ToolStripButton tsbViewBinWTar;
		protected ToolStripButton tsbViewBinWRot;
		protected ToolStripButton tsbViewBinWDis;
		
		protected ContextMenu contextmenu;
		
		#endregion
		
		#region Camera Moving Vars
		
		protected System.Timers.Timer smoothtimer = new System.Timers.Timer( 5 );
		protected float smootht;
		protected float dsrc;
		protected float dtar;
		protected Quatf qsrc;
		protected Quatf qtar;
		protected Vec3f vsrc;
		protected Vec3f vtar;
		
		#endregion
		
		protected bool render_backcolor = false;
		
		protected Thread processthread;
		protected bool reprocess = false;
		protected float[] depth;
		protected Bitmap screenshot = null;
		
		protected GLText gltext;
		
		protected FlattenedViewable fvOrigin = new FlattenedViewable(
			new Vec3f[] { new Vec3f( 0, 0, 0 ), new Vec3f( 5, 0, 0 ), new Vec3f( 0, 0, 0 ), new Vec3f( 0, 5, 0 ), new Vec3f( 0, 0, 0 ), new Vec3f( 0, 0, 5 ) },
			new Vec3f[] { new Vec3f( 5, 0, 0 ), new Vec3f( 5, 0, 0 ), new Vec3f( 0, 5, 0 ), new Vec3f( 0, 5, 0 ), new Vec3f( 0, 0, 5 ), new Vec3f( 0, 0, 5 ) },
			2
			);
		
		#region Constructor
		
		public ModelViewerControl( ModelingHistory hist, bool cam0 )
			: base( )
		{
			if( base.IsHandleCreated == false ) throw new Exception( "GLControl Handle is not created!" );
			
			processthread = new Thread( ProcessViewable );
			
			this.hist = hist;
			SetCluster( hist.Layers.GetClusteringLayer(), hist.Layers.GetClusters()[0] );
			hist.TagsChanged += delegate { RefreshControl(); };
			
			depth = new float[Width*Height];
			screenshot = new Bitmap( this.Width, this.Height );
			
			ViewUser.Name = "Custom";
			if( cam0 ) ViewUser.Set( cameras[0] );					// set default user camera
			else {
				ViewUser.Set( new Vec3f(), new Quatf( 0.73907f, -0.53697f, -0.23907f, -0.32906f ), 10.53924f, true );
				ViewSelection.Set( ViewSelections.User );
				Camera.Set( ViewUser );
			}
			Camera.Sync( ViewUser );
			
			CreateToolStipButtons();
			
			ToolSelection.PropertyChanged += delegate {
				UpdateToolStrip();
				RefreshControl();
			};
			
			ViewSelection.PropertyChanged += delegate {
				SmoothView();
				tsbViewArtist.Checked = ( ViewSelection.Val == ViewSelections.Artist );
				tsbViewBest.Checked = ( ViewSelection.Val == ViewSelections.BestView );
				tsbViewUser.Checked = ( ViewSelection.Val == ViewSelections.User );;
			};
			SmoothedCamera.PropertyChanged += delegate { SetCluster( layer, cluster ); UpdateToolStrip(); };
			SmoothingMethod.PropertyChanged += delegate { SetCluster( layer, cluster ); UpdateToolStrip(); };
			SigmaT.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			SigmaX.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			BinEpsilon.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			BinFromt0.PropertyChanged += delegate { SetCluster( layer, cluster ); UpdateToolStrip(); };
			BinWTar.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			BinWRot.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			BinWDis.PropertyChanged += delegate { SetCluster( layer, cluster ); };
			
			ViewerProperties.PropertyChanged += delegate( object o, PropertyChangedEventArgs e ) {
				string props = e.PropertyName;
				if(
				   props.Contains( RenderOptSelectedVerts.Name ) ||
				   props.Contains( RenderOptShowRemoved.Name ) ||
				   props.Contains( RenderOptShowAnnotations.Name ) ||
				   props.Contains( RenderOptShowSelected.Name ) )
				{
					cluster.viewable = null;
					SetCluster( layer, cluster, true );
				} else RefreshControl();
			};
			ViewerProperties.AddProperty( RenderOptBoundingBox );
			ViewerProperties.AddProperty( RenderOptOrigin );
			//ViewerProperties.AddProperty( RenderOptExtra );
			ViewerProperties.AddProperty( RenderOptTargetPosition );
			ViewerProperties.AddProperty( RenderOptGroupNumbers );
			ViewerProperties.AddProperty( RenderOptVTags );
			ViewerProperties.AddProperty( RenderOptTTags );
			ViewerProperties.AddProperty( RenderOptSeparateViewports );
			ViewerProperties.AddProperty( RenderOptClusterInfo );
			ViewerProperties.AddProperty( RenderOptGrid );
			ViewerProperties.AddProperty( RenderOptRotationGizmoOpt );
			ViewerProperties.AddProperty( RenderOptTransparentForeground );
			ViewerProperties.AddProperty( RenderOptSkipReordering );
			ViewerProperties.AddProperty( RenderOptSelectedVerts );
			ViewerProperties.AddProperty( RenderOptShowAnnotations );
			ViewerProperties.AddProperty( RenderOptShowRemoved );
			ViewerProperties.AddProperty( RenderOptShowSelected );
			ViewerProperties.AddProperty( RenderOptWireframe );
			
			gltext = new GLText( "LibMonoBlueShadowed.png" );
			
			smoothtimer.Elapsed += SmoothTimerElapsed;
			processthread.Start();
			
			contextmenu = new ContextMenu( new MenuItem[] {
				new MenuItem( "View", new MenuItem[] {
					new MenuItem( "Artist", delegate { ViewSelection.Set( ViewSelections.Artist ); } ),
					new MenuItem( "BestView", delegate { ViewSelection.Set( ViewSelections.BestView ); } ),
					new MenuItem( "User", delegate { ViewSelection.Set( ViewSelections.User ); } ),
				} ),
				new MenuItem( "Toggle Options", new MenuItem[] {
					new MenuItem( "Ortho/Persp", delegate { ViewUser.Ortho.Set( !ViewUser.Ortho.Val ); ViewSelection.Set( ViewSelections.User ); } ),
					new MenuItem( "Render Grid", delegate { RenderOptGrid.Val = !RenderOptGrid.Val; } ),
					new MenuItem( "Render Origin", delegate { RenderOptOrigin.Val = !RenderOptOrigin.Val; } ),
					new MenuItem( "Render Camera Target", delegate { RenderOptTargetPosition.Val = !RenderOptTargetPosition.Val; } ),
					new MenuItem( "Render Selections", delegate { RenderOptSelectedVerts.Val = !RenderOptSelectedVerts.Val; } ),
					new MenuItem( "Render Rotation Gizmo", delegate { RenderOptRotationGizmoOpt.Val = !RenderOptRotationGizmoOpt.Val; } ),
					new MenuItem( "Render VTags", delegate { RenderOptVTags.Val = !RenderOptVTags.Val; } ),
				} ),
				new MenuItem( "Clear Selection", delegate {
					hist.HighlightsClearAll(); RefreshControl();
				} ),
				new MenuItem( "Render to bitmap", delegate {
					MakeCurrent();
					Bitmap bmp = new Bitmap( this.Width, this.Height );
					BitmapData data = bmp.LockBits( ClientRectangle, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb );
					GL.ReadPixels( 0, 0, ClientSize.Width, ClientSize.Height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, data.Scan0 );
					bmp.UnlockBits( data );
					bmp.RotateFlip( RotateFlipType.RotateNoneFlipY );
					bmp.Save( "bitmap.png" );
					bmp.Dispose();
				} ),
			} );
			
			if( ModelingHistory.DEBUGMODE ) contextmenu.MenuItems.AddRange( new MenuItem[] {
				new MenuItem( "-" ),
				new MenuItem( "Debug", new MenuItem[] {
					new MenuItem( "Options", new MenuItem[] {
						new MenuItem( "Transparent Foreground", delegate { RenderOptTransparentForeground.Val = !RenderOptTransparentForeground.Val; } ),
						new MenuItem( "Use Final Positions", delegate { RenderOptUseFinalPos.Toggle(); SetCluster( layer, cluster ); } ),
					} ),
					new MenuItem( "Print ViewUser", delegate {
						Vec3f rot = ViewUser.GetRotation().Vector;
						string srot = String.Format( "<<{0}, {1}, {2}>, {3}>", rot.x, rot.y, rot.z, ViewUser.GetRotation().Scalar );
						string star = ViewUser.GetTarget().ToStringFormatted();
						System.Console.WriteLine( "Camera: " + star + " + " + ViewUser.GetDistance() + " x " + srot );
					} ),
					new MenuItem( "Print cameras", delegate {
						cameras.Each( delegate( CameraProperties cam, int i ) {
							Vec3f rot = cam.GetRotation().Vector;
							string srot = String.Format( "<<{0}, {1}, {2}>, {3}>", rot.x, rot.y, rot.z, cam.GetRotation().Scalar );
							string star = cam.GetTarget().ToStringFormatted();
							System.Console.WriteLine( "Camera " + i + ": " + star + " + " + cam.GetDistance() + " x " + srot );
						} );
					} ),
				} ),
			} );
			
			if( !cam0 ) ViewSelection.Set( ViewSelections.User );
		}
		
		#endregion
		
		#region Cluster / Viewable Functions
		
		public bool HasRendered = true;
		public void SetCluster( Clustering layer, Cluster cluster ) { SetCluster( layer, cluster, false ); }
		public void SetCluster( Clustering layer, Cluster cluster, bool overridesame )
		{
			bool same = ( this.cluster == cluster && this.layer == layer );
			
			if( !same || overridesame )
			{
				this.layer = layer;
				this.cluster = cluster;
				
				HasRendered = false;
				
				long timeview = Timer.GetExecutionTime_ms( delegate {
					viewables = hist.GetViewables( cluster, RenderOptUseFinalPos, RenderOptShowSelected, !RenderOptShowAnnotations, RenderOptShowRemoved );
				} );
				long timetriminvis = Timer.GetExecutionTime_ms( delegate {
					foreach( IndexedViewableAlpha v in viewables ) IndexedViewableAlpha.TrimInvisible( v );
				} );
				//System.Console.WriteLine( "view: " + timeview + ", trim: " + timetriminvis );
				
				reprocess = false;
			}
			
			//this.cameras = hist.GetCameras( layer, cluster, viewablesum ).AddReturn( ViewUser );
			//this.cameras = cluster.GetCameras().AddReturn( ViewUser );
			
			int ind = layer.GetClusterIndex( cluster );
			if( ind != -1 )
			{
				//List<CameraProperties>[] cams = layer.GetCameras();
				//System.Console.WriteLine( cams.Length + " " + ind );
				//this.cameras = cams[ind].AddReturn( ViewUser ).ToArray();
				this.cameras = hist.GetCameras( cluster, SigmaT.Val, SigmaX.Val, BinEpsilon.Val, BinFromt0.Val, BinWTar.Val, BinWRot.Val, BinWDis.Val ).AddReturn( ViewUser );
			}
			timestamp = String.Format( "{0:0000}+{1:000}", cluster.start, ( cluster.end - cluster.start ) );
			
			SmoothView();
		}
		
		//public IndexedViewableAlpha GetViewableSum() { return viewablesum; }
		
		private void ProcessViewable() // object sender, ElapsedEventArgs e )
		{
			Cluster cur = null;
			while( !ModelingHistory.ENDING )
			{
				while( !reprocess && !ModelingHistory.ENDING ) Thread.Sleep( 5 );
				if( ModelingHistory.ENDING ) return;
				
				reprocess = false;
				
				cur = cluster;
				//IndexedViewableAlpha[] vs = hist.GetViewables( cur, RenderOptUseFinalPos, RenderOptSelectedVerts, false );
				if( reprocess ) continue;
				
				//viewables = vs;
				
				RefreshControl();
			}
		}
		
		#endregion
		
		#region Camera Functions
		
		private void SmoothTimerElapsed( object sender, ElapsedEventArgs e )
		{
			smootht = Math.Min( smootht + EaseSpeed, 1.0f );
			if( smootht == 1.0f ) smoothtimer.Enabled = false;
			
			Quatf q = Quatf.Slerp( qsrc, qtar, smootht );
			Vec3f v = vsrc * ( 1.0f - smootht ) + vtar * smootht;
			float d = dsrc * ( 1.0f - smootht ) + dtar * smootht;
			
			ViewUser.Set( v, q, d );
			RefreshControl();
		}
		
		private int GetViewIndex() { return GetViewIndex( ViewSelection.Val ); }
		private int GetViewIndex( ViewSelections viewsel )
		{
			int ismooth = 0;
			
			switch( SmoothingMethod.Val ) {
			case CameraSmoothing.Gaussian: ismooth = 0; break;
			case CameraSmoothing.Bilateral: ismooth = 1; break;
			case CameraSmoothing.Binary: ismooth = 2; break;
			}
			
			switch( viewsel ) {
			case ViewSelections.Artist:		return ( !SmoothedCamera ? 0 : 2 + ismooth );
			case ViewSelections.BestView:	return ( !SmoothedCamera ? 1 : 5 + ismooth );
			case ViewSelections.User:		return 7;
			default: throw new Exception( "Unimplemented ViewSelection: " + ViewSelection.Val );
			}
		}
		
		public void SmoothView()
		{
			if( ViewSelection.Val == ViewSelections.User ) { RefreshControl(); return; }
			SmoothView( cameras[GetViewIndex()] );
		}
		
		private void SmoothView( CameraProperties cam )
		{
			SmoothView( cam.GetTarget(), cam.GetRotation(), cam.GetDistance(), cam.GetOrtho() );
		}
		
		public void SmoothView( Vec3f target )
		{
			SmoothView( target, ViewUser.GetRotation(), ViewUser.GetDistance(), ViewUser.GetOrtho() );
		}
		public void SmoothView( Quatf rotation )
		{
			SmoothView( ViewUser.GetTarget(), rotation, ViewUser.GetDistance(), ViewUser.GetOrtho() );
		}
		public void SmoothView( float distance )
		{
			SmoothView( ViewUser.GetTarget(), ViewUser.GetRotation(), distance, ViewUser.GetOrtho() );
		}
		public void SmoothView( bool ortho )
		{
			SmoothView( ViewUser.GetTarget(), ViewUser.GetRotation(), ViewUser.GetDistance(), ortho );
		}
		
		public void SmoothView( Vec3f target, Quatf rotation, float distance, bool orthographic )
		{
			if( FixedCameraDist > 0.0f ) { distance = FixedCameraDist; target = new Vec3f(); }
			if( ViewEasing ) {
				smoothtimer.Enabled = false;
				smootht = 0.0f;
				
				qsrc = ViewUser.GetRotation();
				vsrc = ViewUser.GetTarget();
				dsrc = ViewUser.GetDistance();
				
				qtar = rotation;
				vtar = target;
				dtar = distance;
				
				ViewUser.Ortho.Set( orthographic );
				
				smoothtimer.Enabled = true;
				RefreshControl();
			} else {
				qtar = rotation;
				vtar = target;
				dtar = distance;
				Camera.Set( target, rotation, distance, orthographic );
			}
		}
		
		public bool CameraReady
		{
			get
			{
				if( ViewSelection.Val != ViewSelections.Artist ) return true;
				//if( !smoothtimer.Enabled ) return true;
				
				Quatf rot = Camera.GetRotation();
				Vec3f tar = Camera.GetTarget();
				float dist = Camera.GetDistance();
				
				Vec3f fwd = rot.Rotate( -Vec3f.Z );
				Vec3f up = rot.Rotate( Vec3f.Y );
				Vec3f pos = tar - fwd * dist;
				
				Vec3f dfwd = qtar.Rotate( -Vec3f.Z );
				Vec3f dup = qtar.Rotate( Vec3f.Y );
				Vec3f dpos = vtar - dfwd * dtar;
				
				float off = ( tar - vtar).LengthSqr * 200.0f;
				//off += ( rot % qtar ) * 200.0f;
				off += ( 1.0f - up % dup ) * 200.0f;
				off += ( pos - dpos ).LengthSqr * 200.0f;
				off += ( dist - dtar ) * ( dist - dtar ) * 200.0f;
				//System.Console.Write( " " + off );
				return off < 0.1f;
			}
		}
		
		#endregion
		
		#region Rendering Functions
		
		private bool savesnapshot = false;
		public void SaveSnapshot( string filename )
		{
			string file = String.Format( "{0}{1:0000}.png", filename, snapshotcount);
			string filed = String.Format( "{0}{1:0000}d.png", filename, snapshotcount);
			string filem = String.Format( "{0}{1:0000}m.png", filename, snapshotcount);
			
			//MakeCurrent();
			Bitmap bmp = new Bitmap( screenshot );
			Bitmap bmpd = new Bitmap( this.Width, this.Height );
			Bitmap bmpm = new Bitmap( this.Width, this.Height );
			
			float max = 0.0f;
			float min = 1.0f;
			for( int x = 0; x < this.Width; x++ )
				for( int y = 0; y < this.Height; y++ )
			{
				float d = depth[x+y*this.Width];
				if( d < 1.0f ) { max = Math.Max( max, d ); min = Math.Min( min, d ); }
			}
			float diff = max - min;
			
			for( int x = 0; x < this.Width; x++ )
				for( int y = 0; y < this.Height; y++ )
			{
				float d = Math.Max( 1.0f - ( (depth[x + y * this.Width] - min) / diff ), 0.0f );
				int v = (int)(d * 255.0f);
				Color c = Color.FromArgb( v, v, v );
				bmpd.SetPixel( x, this.Height - 1 - y, c );
				c = bmp.GetPixel( x, y );
				if( d == 0.0f ) d = 1.0f;
				c = Color.FromArgb( (int)((float)c.R * d), (int)((float)c.G * d), (int)((float)c.B * d) );
				bmpm.SetPixel( x, this.Height - 1 - y, c );
			}
			
			bmp.RotateFlip( RotateFlipType.RotateNoneFlipY );
			
			bmp.Save( file );
			//bmpd.Save( filed );
			//bmpm.Save( filem );
			
			bmp.Dispose();
			bmpd.Dispose();
			bmpm.Dispose();
			
			System.Console.WriteLine( "Saved snapshot to " + file );
			snapshotcount++;
			
			//savesnapshot = false;
			//savesnapshot = true;
			//RefreshControl();
		}
		
		public void ResetSnapshotCount() { snapshotcount = 0; }
		
		/*private void WriteSnapshot()
		{
			string file = String.Format( "snap{0:0000}.png", snapshotcount);
			//MakeCurrent();
			Bitmap bmp = new Bitmap( this.Width, this.Height );
			BitmapData data = bmp.LockBits( ClientRectangle, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb );
			GL.ReadPixels( 0, 0, ClientSize.Width, ClientSize.Height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, data.Scan0 );
			bmp.UnlockBits( data );
			bmp.RotateFlip( RotateFlipType.RotateNoneFlipY );
			bmp.Save( file );
			bmp.Dispose();
			
			System.Console.WriteLine( "Saved snapshot to " + file );
			snapshotcount++;
			savesnapshot = false;
		}*/
		
		public bool CurrentlyRendering { get; private set; }
		public override void RenderData()
		{
			CurrentlyRendering = true;
			
			// the following keep bad things from happening when the viewable changes while still rendering
			// do NOT remove
			IndexedViewableAlpha[] viewablesrender = viewables;
			if( viewablesrender == null || viewablesrender.Length == 0 ) return;
			
			DateTime starttime = DateTime.Now;			// used to calc rendering time
			
			if( viewports != viewablesrender.Count() ) { viewports = viewablesrender.Count(); ViewUser.Resize( Width / viewports, Height ); }
			
			int vw = Width / viewports;
			int vh = Height; // / c;
			int vt = 0; //(Height - vh ) / 2;
			
			for( int i = 0; i < viewports; i++ )
			{
				GL.Viewport( vw * i + 1, vt, vw - 2, vh );
				RenderViewport( viewablesrender[i] );
			}
			GL.ReadPixels( 0, 0, Width, Height, OpenTK.Graphics.OpenGL.PixelFormat.DepthComponent, PixelType.Float, depth );
			
			if( RenderOptRotationGizmoOpt.Val ) {
				GL.Viewport( 0, 0, 100, (int)(100.0f * (float)Height / (float)Width) );
				RenderRotationGizmo();
			}
			
			GL.Viewport( 0, 0, Width, Height );
			RenderTool();
			
			DateTime endtime = DateTime.Now;
			TimeSpan durtime = endtime - starttime;
			if( RenderOptClusterInfo || RenderOptTTags || ModelingHistory.DEBUGMODE ) RenderClusterText( durtime );
			
			BitmapData data = screenshot.LockBits( ClientRectangle, ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb );
			GL.ReadPixels( 0, 0, ClientSize.Width, ClientSize.Height, OpenTK.Graphics.OpenGL.PixelFormat.Bgra, OpenTK.Graphics.OpenGL.PixelType.UnsignedByte, data.Scan0 );
			screenshot.UnlockBits( data );
			//if( savesnapshot ) WriteSnapshot();
			
			CurrentlyRendering = false;
			HasRendered = true;
		}
		
		private void RenderViewport( IndexedViewableAlpha viewable )
		{
			GL.BlendFunc( BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha );
			
			if( RenderOptGrid ) {
				float dotXZ = FMath.Pow( Math.Abs( Camera.GetForward() % Vec3f.Y ), 100.0f );
				RenderGridXZ( dotXZ * 0.2f );
				float dotYZ = FMath.Pow( Math.Abs( Camera.GetForward() % Vec3f.X ), 100.0f );
				RenderGridYZ( dotYZ * 0.2f );
				//if( ViewSelection.Val == ViewSelections.Front ) RenderGridXZ();
				//else if( ViewSelection.Val == ViewSelections.Right ) RenderGridYZ();
				//else RenderGridXY();
				RenderGridXY( ( 1.0f - dotXZ ) * ( 1.0f - dotYZ ) * 0.2f );
			}
			
			GL.Clear( ClearBufferMask.DepthBufferBit );
			
			if( RenderOptOrigin ) RenderViewable( fvOrigin );
			
			if( RenderOptTargetPosition ) RenderCameraTarget( 0.01f, Vec3f.One );
			
			RenderViewable( hist.DeEmphasizeNonHighlighted( viewable, RenderOptBackColor ), RenderOptWireframe );
			
			gltext.InitTexturing( Width, Height );
			if( RenderOptGroupNumbers )
			{
				//RenderLabeledGroups( viewables[0], 0, gltext, i => "" + viewables[0].VertUIDs[viewables[0].Indices[0][i]] );
				for( int i = 0; i < viewable.GroupSizes.Length; i++ )
				{
					if( viewable.GroupSizes[i] != 2 ) continue;
					int[] edgeinds = viewable.Indices[i];
					RenderLabeledGroups( viewable, i, gltext, iedge => String.Format( "{0}-{1}", viewable.VertUIDs[edgeinds[iedge]], viewable.VertUIDs[edgeinds[iedge+1]] ) );
				}
			}
			if( RenderOptVTags )
			{
				for( int i = 0; i < viewable.GroupSizes.Length; i++ )
				{
					if( viewable.GroupSizes[i] != 1 ) continue;
					int[] vertinds = viewable.Indices[i];
					RenderLabeledGroups( viewable, i, gltext, ivert => 
					                    ( viewable.Selected[vertinds[ivert]] ? String.Join( ",", hist.GetVTags( viewable.VertUIDs[vertinds[ivert]] ).ToArray() ) : "" )
					                    );
				}
			}
		}
		
		private static int avgwindow = 1000;
		private int curavgpos = 0;
		private double[] mstimes = new double[avgwindow];
		private double msrunning = 0;
		private int runwindow = 0;
		private void RenderClusterText( TimeSpan durtime )
		{
			double tx = gltext.GetLeft();
			double ty = gltext.GetTop();
			
			GL.Clear( ClearBufferMask.DepthBufferBit );
			gltext.InitTexturing( Width, Height );
			
			if( RenderOptTTags )
			{
				gltext.WriteString( tx, ref ty, "OTag: " + cluster.GetOTag(), GLTextPosition.LeftTop );
				gltext.WriteString( tx, ref ty, "TTags:" + String.Join( ",", cluster.GetTTags() ), GLTextPosition.LeftTop );
			}
			if( RenderOptClusterInfo || ModelingHistory.DEBUGMODE )
			{
				string sview = cameras[GetViewIndex()].Name;
				gltext.WriteString( tx, ref ty, "View: " + sview, GLTextPosition.LeftTop );
				gltext.WriteString( tx, ref ty, "Time: " + timestamp, GLTextPosition.LeftTop );
			}
			
			if( ModelingHistory.DEBUGMODE )
			{
				double mscur = durtime.TotalMilliseconds;
				msrunning += - mstimes[curavgpos] + mscur;
				mstimes[curavgpos] = mscur;
				curavgpos = ( curavgpos + 1 ) % avgwindow;
				runwindow = Math.Max( runwindow, curavgpos );
				double msavg = msrunning / (double)runwindow;
				string report = String.Format( "Render: {0:0.00},{1:0.00}", mscur, msavg );
				gltext.WriteString( tx, ref ty, report, GLTextPosition.LeftTop );
			}
			
		}
		
		private void RenderTool()
		{
			if( ToolSelection.Val == ViewerMouseStates.HighlightLine && bMouseInside )
			{
				float x = 2.0f * (float)lastMouseLocation.X / (float)Width - 1.0f;
				float y = 1.0f - 2.0f * (float)lastMouseLocation.Y / (float)Height;
				Vec3f center = Unproject2DTo3D( new Vec3f( x, y, -0.95f ) ); //0.125f ) );
				float scale = ( ViewUser.GetOrtho() ? 8.0f / (float)ViewUser.Scale.Val : 1.0f / 600.0f );
				Vec3f xaxis = ViewUser.GetRight() * (float)BrushSize.Val * scale; // / 600.0f * (float)ViewUser.Scale.Val; // Unproject2DTo3D( new Vec3f( 0.5f, 0.0f, 0.1f ) );
				Vec3f yaxis = ViewUser.GetUp() * (float)BrushSize.Val * scale; // / 600.0f * (float)ViewUser.Scale.Val; // Unproject2DTo3D( new Vec3f( 0.0f, 0.5f, 0.0f ) );
				Vec3f c;
				if( BrushColor.Val == HighlightColors.None ) c = new Vec3f( 1.0f, 0.0f, 0.0f );
				else c = new Vec3f( 1.0f, 1.0f, 0.0f );
				RenderCircle( center, xaxis, yaxis, c );
			}
		}
		
		#endregion
		
		#region UI Functions
		
		protected void CreateToolStipButtons()
		{
			tsbOrbit = new ToolStripButton( "Orbit Target", MiscFileIO.LoadBitmapResource( "orbit.png" ) );
			//tsbOrbit.CheckOnClick = true;
			tsbOrbit.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbOrbit.Click += delegate { ToolSelection.Set( ViewerMouseStates.OrbitOnlyCamera ); };
			
			tsbOrbitDolly = new ToolStripButton( "Orbit and Dolly Target", MiscFileIO.LoadBitmapResource( "orbittruck.png" ) );
			//tsbOrbitDolly.CheckOnClick = true;
			tsbOrbitDolly.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbOrbitDolly.Click += delegate { ToolSelection.Set( ViewerMouseStates.OrbitAndDollyCamera ); };
			
			tsbPan = new ToolStripButton( "Pan and Tilt Camera", MiscFileIO.LoadBitmapResource( "pancamera.png" ) );
			//tsbPan.CheckOnClick = true;
			tsbPan.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbPan.Click += delegate { ToolSelection.Set( ViewerMouseStates.PanAndTiltCamera ); };
			
			tsbTruck = new ToolStripButton( "Truck Camera", MiscFileIO.LoadBitmapResource( "truckcamera.png" ) );
			//tsbTruck.CheckOnClick = true;
			tsbTruck.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbTruck.Click += delegate { ToolSelection.Set( ViewerMouseStates.TruckCamera ); };
			
			tsbHighlightLine = new ToolStripButton( "Highlight Line", MiscFileIO.LoadBitmapResource( "highlightline.png" ) );
			//tsbHighlightLine.CheckOnClick = true;
			tsbHighlightLine.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbHighlightLine.Click += delegate { ToolSelection.Set( ViewerMouseStates.HighlightLine ); };
			
			tsbHighlightSphere = new ToolStripButton( "Highlight Sphere", MiscFileIO.LoadBitmapResource( "highlightsphere.png" ) );
			//tsbHighlightSphere.CheckOnClick = true;
			tsbHighlightSphere.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbHighlightSphere.Click += delegate { ToolSelection.Set( ViewerMouseStates.HighlightSphere ); };
			tsbHighlightSphere.Enabled = false;
			
			tsbHighlightClear = new ToolStripButton( "Clear Highlight", MiscFileIO.LoadBitmapResource( "highlightclear.png" ) );
			tsbHighlightClear.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbHighlightClear.Click += delegate { ViewerWindow.viewwin.UnFilterHighlightedVerts.Enabled.Set( false ); hist.HighlightsClearAll(); };
			
			tsbNumberVerts = new ToolStripButton( "Toggle Vertex Numbers On / Off", MiscFileIO.LoadBitmapResource( "numbers.png" ) );
			tsbNumberVerts.CheckOnClick = true;
			tsbNumberVerts.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbNumberVerts.CheckedChanged += delegate {
				RenderOptGroupNumbers.Set( tsbNumberVerts.Checked );
			};
			
			tsbOrtho = new ToolStripButton( "Toggle Orthographic / Perspective", MiscFileIO.LoadBitmapResource( "ortho.png" ) );
			tsbOrtho.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbOrtho.Click += delegate { ViewUser.Ortho.Toggle(); ViewSelection.Set( ViewSelections.User ); };
			tsbOrtho.Checked = ViewUser.Ortho.Val;
			ViewUser.Ortho.PropertyChanged += delegate { tsbOrtho.Checked = ViewUser.Ortho.Val; };
			
			tsbAlignUp = new ToolStripButton( "Align Up Direction", MiscFileIO.LoadBitmapResource( "alignupvec.png" ) );
			tsbAlignUp.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbAlignUp.Click += delegate {
				
				//UpdateAndSwitchToUserView( Camera.GetTarget(), Camera.GetRotation(), Camera.GetDistance(), tsbOrtho.Checked );
			};
			tsbAlignUp.Enabled = false;
			
			tsbViewArtist = new ToolStripButton( "Artist" );
			tsbViewBest = new ToolStripButton( "BestView" );
			tsbViewUser = new ToolStripButton( "User" );
			tsbViewSmoothNone = new ToolStripButton( "None" );
			tsbViewSmoothGaussian = new ToolStripButton( "Gaussian Smoothing" );
			tsbViewSmoothBilateral = new ToolStripButton( "Bilateral Smoothing" );
			tsbViewSmoothBinary = new ToolStripButton( "Binary Smoothing" );
			tsbViewSigmaT = new ToolStripButton( "Set Sigma T..." );
			tsbViewSigmaX = new ToolStripButton( "Set Sigma X..." );
			tsbViewBinEpsilon = new ToolStripButton( "Set Binary Epsilon..." );
			tsbViewBinFromt0 = new ToolStripButton( "Toggle Binary From-t0" );
			tsbViewBinWTar = new ToolStripButton( "Set Binary Weight Target..." );
			tsbViewBinWRot = new ToolStripButton( "Set Binary Weight Rotation..." );
			tsbViewBinWDis = new ToolStripButton( "Set Binary Weight Distance..." );
			
			tsbViewArtist.Click += delegate { ViewSelection.Set( ViewSelections.Artist ); };
			tsbViewBest.Click += delegate { ViewSelection.Set( ViewSelections.BestView ); };
			tsbViewUser.Click += delegate { ViewSelection.Set( ViewSelections.User ); };
			tsbViewSmoothNone.Click += delegate { SmoothedCamera.Set( false ); };
			tsbViewSmoothGaussian.Click += delegate { SmoothedCamera.Set( true ); SmoothingMethod.Set( CameraSmoothing.Gaussian ); };
			tsbViewSmoothBilateral.Click += delegate { SmoothedCamera.Set( true ); SmoothingMethod.Set( CameraSmoothing.Bilateral ); };
			tsbViewSmoothBinary.Click += delegate { SmoothedCamera.Set( true ); SmoothingMethod.Set( CameraSmoothing.Binary ); };
			tsbViewSigmaT.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", SigmaT, false, false, true );
			};
			tsbViewSigmaX.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", SigmaX, false, false, true );
			};
			tsbViewBinEpsilon.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", BinEpsilon, false, false, true );
			};
			tsbViewBinFromt0.Click += delegate {
				BinFromt0.Toggle();
			};
			tsbViewBinWTar.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", BinWTar, false, true, true );
			};
			tsbViewBinWRot.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", BinWRot, false, true, true );
			};
			tsbViewBinWDis.Click += delegate {
				InputBox.GetPropertyFloat( "Adjust Smoothing Parameters", BinWDis, false, true, true );
			};
			
			tsddView = new ToolStripDropDown();
			tsddView.Items.Add( tsbViewArtist );
			tsddView.Items.Add( tsbViewBest );
			tsddView.Items.Add( tsbViewUser );
			tsddView.Items.Add( new ToolStripSeparator() );
			tsddView.Items.Add( new ToolStripLabel( "- Camera Smoothing -" ) );
			tsddView.Items.Add( tsbViewSmoothNone );
			tsddView.Items.Add( tsbViewSmoothGaussian );
			tsddView.Items.Add( tsbViewSmoothBilateral );
			tsddView.Items.Add( tsbViewSmoothBinary );
			tsddView.Items.Add( new ToolStripSeparator() );
			tsddView.Items.Add( new ToolStripLabel( "- Smoothing Options -" ) );
			tsddView.Items.Add( tsbViewSigmaT );
			tsddView.Items.Add( tsbViewSigmaX );
			tsddView.Items.Add( tsbViewBinEpsilon );
			tsddView.Items.Add( tsbViewBinFromt0 );
			tsddView.Items.Add( tsbViewBinWTar );
			tsddView.Items.Add( tsbViewBinWRot );
			tsddView.Items.Add( tsbViewBinWDis );
			
			tsbView = new ToolStripDropDownButton( "View Selection", MiscFileIO.LoadBitmapResource( "viewselection.png" ) );
			tsbView.DisplayStyle = ToolStripItemDisplayStyle.Image;
			tsbView.DropDown = tsddView;
		}
		
		protected override void InitToolStrip()
		{
			if( ModelingHistory.DEBUGMODE )
			{
				toolstrip.Items.Add( tsbOrbit );
				toolstrip.Items.Add( tsbOrbitDolly );
				toolstrip.Items.Add( tsbPan );
				toolstrip.Items.Add( tsbTruck );
				toolstrip.Items.Add( tsbHighlightLine );
				toolstrip.Items.Add( tsbHighlightSphere );
			}
			toolstrip.Items.Add( tsbHighlightClear );
			
			if( ModelingHistory.DEBUGMODE )
			{
				toolstrip.Items.Add( new ToolStripSeparator() );
				toolstrip.Items.Add( tsbNumberVerts );
				toolstrip.Items.Add( tsbOrtho );
				//toolstrip.Items.Add( tsbAlignUp );
				toolstrip.Items.Add( tsbView );
			}
			
			UpdateToolStrip();
		}
		
		protected void UpdateToolStrip()
		{
			if( ignorestatechange ) return;
			ignorestatechange = true;
			
			tsbOrbit.Checked = ( ToolSelection.Val == ViewerMouseStates.OrbitOnlyCamera );
			tsbOrbitDolly.Checked = ( ToolSelection.Val == ViewerMouseStates.OrbitAndDollyCamera );
			tsbPan.Checked = (ToolSelection.Val == ViewerMouseStates.PanAndTiltCamera );
			tsbTruck.Checked = (ToolSelection.Val == ViewerMouseStates.TruckCamera );
			tsbHighlightLine.Checked = ( ToolSelection.Val == ViewerMouseStates.HighlightLine );
			tsbHighlightSphere.Checked = ( ToolSelection.Val == ViewerMouseStates.HighlightSphere );
			
			tsbViewArtist.Checked = ( ViewSelection.Val == ViewSelections.Artist );
			tsbViewBest.Checked = ( ViewSelection.Val == ViewSelections.BestView );
			tsbViewUser.Checked = ( ViewSelection.Val == ViewSelections.User );;
			tsbViewSmoothNone.Checked = !SmoothedCamera.Val;
			tsbViewSmoothGaussian.Checked = SmoothedCamera.Val & ( SmoothingMethod.Val == CameraSmoothing.Gaussian );
			tsbViewSmoothBilateral.Checked = SmoothedCamera.Val & ( SmoothingMethod.Val == CameraSmoothing.Bilateral );
			tsbViewSmoothBinary.Checked = SmoothedCamera.Val & ( SmoothingMethod.Val == CameraSmoothing.Binary );
			tsbViewBinFromt0.Checked = BinFromt0.Val;
			
			if( ToolSelection.Val == ViewerMouseStates.HighlightLine ) bHideCuror = true;
			else bHideCuror = false;
			SetMouseCursor();
			
			ignorestatechange = false;
		}
		
		#endregion
		
		#region Event Functions
		
		protected override void OnResize( EventArgs e )
		{
			//base.OnResize( e );
			depth = new float[Width*Height];
			if( screenshot != null ) screenshot.Dispose();
			if( this.Width > 0 && this.Height > 0 ) screenshot = new Bitmap( this.Width, this.Height );
			
			if( RenderOptSeparateViewports.Val ) {
				if( viewables == null || viewables.Count() == 0 ) return;
				viewports = viewables.Count();
				ViewUser.Resize( Width / viewports, Height );
			} else {
				viewports = 1;
				ViewUser.Resize( Width, Height );
			}
		}
		
		protected override bool ProcessCmdKey (ref Message msg, Keys keyData)
		{
			const int WM_KEYDOWN = 0x100;
			const int WM_SYSKEYDOWN = 0x104;
			
			if( ( msg.Msg == WM_KEYDOWN ) || ( msg.Msg == WM_SYSKEYDOWN ) )
			{
				switch( keyData ) {
					
				case Keys.D1:
					ViewSelection.Set( ViewSelections.Artist );
					break;
					
				case Keys.D2:
					SmoothedCamera.Toggle();
					//ViewSelection.Set( ViewSelections.BestView );
					break;
					
				case Keys.D3:
					ViewUser.Ortho.Toggle();
					ViewSelection.Set( ViewSelections.User );
					break;
					
				case Keys.D0:
					snapshotcount = 0;
					break;
					
				case Keys.NumPad1:
					ViewSelection.Set( ViewSelections.User );
					SmoothView( new Quatf( 0.707107f, -0.707107f, 0.0f, 0.0f ) );
					break;
					
				case Keys.NumPad3:
					ViewSelection.Set( ViewSelections.User );
					SmoothView( new Quatf( -0.5f, 0.5f, 0.5f, 0.5f ) );
					break;
					
				case Keys.NumPad7:
					ViewSelection.Set( ViewSelections.User );
					SmoothView( new Quatf( -1.0f, 0.0f, 0.0f, 0.0f ) );
					break;
					
				case Keys.Tab:
					switch( ToolSelection.Val ) {
					case ViewerMouseStates.OrbitOnlyCamera: ToolSelection.Set( ViewerMouseStates.HighlightLine ); break;
					default: ToolSelection.Set( ViewerMouseStates.OrbitOnlyCamera ); break;
					}
					UpdateToolStrip();
					RefreshControl();
					return true;
					
				case Keys.F2:
					SaveSnapshot( "snap" );
					break;
					
				case Keys.F12:
					SmoothView( new Vec3f(), new Quatf( 0.73907f, -0.53697f, -0.23907f, -0.32906f ), 10.53924f, true );
					break;
					
				case Keys.Home:
					ViewSelection.Set( ViewSelections.User );
					SmoothView( Vec3f.Zero );
					//SmoothView( Vec3f.Zero, new Quatf( 0.707107f, -0.707107f, 0.0f, 0.0f ), 10.0f, true );
					break;
					
				case Keys.A:
					RenderOptShowAnnotations.Toggle();
					break;
					
				case Keys.B:
					render_backcolor = !render_backcolor;
					RenderOptBackColor.Set( ( render_backcolor ? Color.White : Color.DimGray ) );
					break;
					
				case Keys.C:
					ViewerWindow.viewwin.UnFilterHighlightedVerts.Enabled.Set( false ); 
					hist.HighlightsClearAll();
					break;
					
				case Keys.E:
					ViewEasing.Toggle();
					break;
					
				case Keys.F:
					string d = "" + FixedCameraDist;
					if( InputBox.Show( "Set fixed camera distance", "Enter the fixed camera distance ( <=0 := off )", ref d ) == DialogResult.Cancel ) break;
					FixedCameraDist = Single.Parse( d );
					SmoothView();
					break;
					
				case Keys.G:
					RenderOptGrid.Toggle();
					break;
					
				case Keys.I:
					hist.HighlightConnected( viewables );
					RefreshControl();
					break;
					
				case Keys.O:
					RenderOptRotationGizmoOpt.Toggle();
					break;
					
				case Keys.R:
					RenderOptShowRemoved.Toggle();
					break;
					
				case Keys.T:
					//RenderOptGroupNumbers.Toggle();
					RenderOptTTags.Toggle();
					break;
					
				case Keys.S:
					RenderOptShowSelected.Toggle();
					break;
					
				case Keys.V:
					RenderOptVTags.Toggle();
					break;
					
				case Keys.W:
					RenderOptWireframe.Toggle();
					break;
					
				case Keys.Add:
					BrushColor.Set( HighlightColors.Red );
					ToolSelection.Set( ViewerMouseStates.HighlightLine );
					RefreshControl();
					break;
					
				case Keys.Subtract:
					BrushColor.Set( HighlightColors.None );
					ToolSelection.Set( ViewerMouseStates.HighlightLine );
					RefreshControl();
					break;
					
				}
			}
			return base.ProcessCmdKey( ref msg, keyData );
		}
		
		protected override void HandleMouseEnter()
		{
			RefreshControl();
		}
		
		protected override void HandleMouseLeave()
		{
			RefreshControl();
		}
		
		protected override void HandleMouse( MouseEventArgs e, ViewerControlMouseStates state )
		{
			LastMouseState = MouseState.Val;
			MouseState.Val = new MouseStatus() { x = e.X, y = e.Y, buttons = e.Button };
			ViewerMouseStates mstate = ToolSelection.Val;
			int dx = e.X - lastMouseLocation.X;
			int dy = e.Y - lastMouseLocation.Y;
			
			
			if( state == ViewerControlMouseStates.MouseUp ) return;
			if( e.Button == MouseButtons.Right ) mstate = ViewerMouseStates.TruckCameraRightForward;
			
			if( state == ViewerControlMouseStates.MouseWheel )
			{
				if( mstate == ViewerMouseStates.HighlightLine ) mstate = ViewerMouseStates.ResizeBrush;
				else mstate = ViewerMouseStates.Zoom;
			}
			
			if( e.Button == MouseButtons.None && state != ViewerControlMouseStates.MouseWheel )
			{
				if( mstate == ViewerMouseStates.HighlightLine ) { RefreshControl(); }
				return;
			}
			
			if( e.Button == MouseButtons.Middle ) mstate = ViewerMouseStates.OrbitAndDollyCamera;
			//if( e.Button == MouseButtons.Right ) mstate = ViewerMouseStates.OrbitOnlyCamera;
			if( bShiftPressed && e.Button == MouseButtons.Left ) mstate = ViewerMouseStates.TruckCamera;
			
			
			bool updateuser = false;
			
			switch( mstate )
			{
			
			case ViewerMouseStates.Nothing:
				break;
				
			case ViewerMouseStates.PanAndTiltCamera:
				smoothtimer.Enabled = false;
				ViewUser.Properties.DeferPropertyChanged = true;
				ViewUser.Pan( dx );
				ViewUser.Tilt( dy );
				ViewUser.Properties.DeferPropertyChanged = false;
				updateuser = true;
				break;
				
			case ViewerMouseStates.Zoom:
				smoothtimer.Enabled = false;
				ViewUser.Properties.DeferPropertyChanged = true;
				if( bShiftPressed ) ViewUser.DollyIntoTarget( (double)e.Delta * 0.01 );
				else ViewUser.DollyIntoTarget( (double)e.Delta * 0.1 );
				ViewUser.Properties.DeferPropertyChanged = false;
				updateuser = true;
				break;
				
			case ViewerMouseStates.ResizeBrush:
				BrushSize.Set( BrushSize.Val + (double)e.Delta / 1000.0 );
				break;
				
			case ViewerMouseStates.OrbitOnlyCamera:
				smoothtimer.Enabled = false;
				ViewUser.Properties.DeferPropertyChanged = true;
				ViewUser.OrbitTarget( (double)dx );
				ViewUser.OrbitTargetUpDown( (double) dy, true );
				ViewUser.Properties.DeferPropertyChanged = false;
				updateuser = true;
				break;
				
			case ViewerMouseStates.OrbitAndDollyCamera:
				smoothtimer.Enabled = false;
				ViewUser.Properties.DeferPropertyChanged = true;
				ViewUser.OrbitTarget( (double)dx );
				ViewUser.DollyIntoTarget( -(double)dy * 1.0 );
				ViewUser.Properties.DeferPropertyChanged = false;
				updateuser = true;
				break;
				
			case ViewerMouseStates.TruckCamera:
				smoothtimer.Enabled = false;
				if( bControlPressed ) ViewUser.TruckXYAxis( (double)dx * -0.01, (double)dy * 0.01 );
				else ViewUser.Truck( (double)dx * -0.01, (double)dy * 0.01 );
				updateuser = true;
				break;
				
			case ViewerMouseStates.TruckCameraRightForward:
				smoothtimer.Enabled = false;
				ViewUser.TruckRightForward( (double)dx * -0.01, -(double)dy * 0.01 );
				updateuser = true;
				break;
				
			case ViewerMouseStates.HighlightLine:
				HighlightSamplesLine( e.X, e.Y );
				updateuser = true;
				break;
			
			case ViewerMouseStates.HighlightSphere:
				HighlightSamplesSphere( e.X, e.Y );
				updateuser = true;
				break;
				
			default:
				throw new Exception( "Unimplemented MouseState" );
				
			}
			
			if( updateuser )
			{
				ViewSelection.Set( ViewSelections.User );
				//UpdateUserView( Camera.GetTarget(), Camera.GetRotation(), Camera.GetDistance(), Camera.GetOrtho() );
				//if( ViewSelection.Val != ViewSelections.User )
				//{
				//	//ViewSelection.IgnorePropertyChanges = true;
				//	ViewSelection.Set( ViewSelections.User );
				//	//ViewSelection.IgnorePropertyChanges = false;
				//}
				////UpdateAndSwitchToUserView();
			}
		}
		
		#endregion
		
		#region Highlight Functions
		
		protected void HighlightSamplesLineold( int mx, int my )
		{
			float sx = 2.0f * (float)mx / (float)Width - 1.0f;
			float sy = 1.0f - 2.0f * (float)my / (float)Height;
			float mult = (float)Width / (float)Height;
			
			double szsq = BrushSize.Val * BrushSize.Val / 16.0;
			
			Vec3f? cn = Highlight.GetColor( BrushColor.Val );
			Vec4f hc;
			if( cn != null ) { Vec3f c = (Vec3f) cn; hc = new Vec4f( c.x, c.y, c.z, 1.0f ); }
			else hc = new Vec4f( 0.0f, 0.0f, 0.0f, 0.0f );
			
			foreach( IndexedViewableAlpha viewable in viewables )
			{
				for( int ig = 0; ig < viewable.Indices.Length; ig++ )
				{
					if( viewable.GroupSizes[ig] != 1 ) continue;
					int[] inds = viewable.Indices[ig];
					int c = inds.Length;
					for( int i = 0; i < c; i++ )
					{
						int uid = viewable.VertUIDs[inds[i]];
						if( uid < 0 ) continue;
						
						Vec3f coord = viewable.Verts[inds[i]];
						Vec3f cproj = Project3DTo2D( coord );
						float dsq = ( cproj.x - sx ) * ( cproj.x - sx ) * mult * mult + ( cproj.y - sy ) * ( cproj.y - sy );
						
						if( dsq <= szsq ) hist.HighlightUID( uid, BrushColor.Val );
					}
				}
			}
			
			RefreshControl();
			//SetModelTree( mtree );
			//this.viewable = hist.GetViewable( mtree, true, true );
		}
		
		protected void HighlightSamplesLine( int mx, int my )
		{
			int w = Width;
			int h = Height;
			
			if( mx < 0 || my < 0 || mx >= w || my >= h ) return;
			
			float mult = (float)w / (float)h;
			float x = 2.0f * (float)mx / (float)w - 1.0f;
			float y = 1.0f - 2.0f * (float)my / (float)h;
			Vec3f center = Unproject2DTo3D( new Vec3f( x, y, 0.125f ) );
			float scale = ( ViewUser.GetOrtho() ? 8.0f / (float)ViewUser.Scale.Val : 1.0f / 600.0f );
			Vec3f xaxis = ViewUser.GetRight() * (float)BrushSize.Val * scale;
			//Vec3f yaxis = ViewUser.GetUp() * (float)BrushSize.Val * scale;
			
			Vec3f centerproj = Project3DTo2D( center );
			Vec3f xaxisproj = Project3DTo2D( center + xaxis );
			float radsqr = ( centerproj.x - xaxisproj.x ) * ( centerproj.x - xaxisproj.x ) * mult * mult;
			
			//System.Console.WriteLine( "<{0},{1}> = {2}", mx, my, depth[mx+my*w] );
			
			foreach( IndexedViewableAlpha v in viewables )
			{
				IndexedViewableAlpha viewable = v;
				while( viewable != null )
				{
					for( int ig = 0; ig < viewable.Indices.Length; ig++ )
					{
						if( viewable.GroupSizes[ig] != 1 ) continue;
						int[] inds = viewable.Indices[ig];
						int c = inds.Length;
						for( int i = 0; i < c; i++ )
						{
							int uid = viewable.VertUIDs[inds[i]];
							if( uid < 0 ) continue;
							
							Vec3f coord = viewable.Verts[inds[i]];
							Vec3f cproj = Project3DTo2D( coord );
							float dsq = ( cproj.x - centerproj.x ) * ( cproj.x - centerproj.x ) * mult * mult + ( cproj.y - centerproj.y ) * ( cproj.y - centerproj.y );
							
							if( dsq > radsqr ) continue;
							
							//System.Console.Write( "{0}: {1}", uid, cproj.ToStringFormatted() );
							int px = (int)((cproj.x + 1.0f) / 2.0f * (float)w);
							int py = (int)((cproj.y + 1.0f) / 2.0f * (float)h);
							float pz = (cproj.z + 1.0f) / 2.0f;
							float d = depth[ px + py * w ];
							//System.Console.WriteLine( " => <{0},{1}>, {2} <> {3}", px, py, d, pz );
							if( d + 0.00001f >= pz )
								hist.HighlightUID( uid, BrushColor.Val );
						}
					}
					viewable = viewable.attached;
				}
			}
			
			RefreshControl();
		}
		
		protected void HighlightSamplesSphere( int sx, int sy )
		{
			double szsq = BrushSize.Val * BrushSize.Val;
			double cszsq = szsq * BrushCollisionRatio.Val * BrushCollisionRatio.Val;
			
			//DebugWindow.SectionWriteVars( "szsq", szsq, "cszsq", cszsq );
			
			List<Vec3f> nearfar = ViewUser.Unproject( sx, sy );
			Vec3f near = nearfar[0];
			Vec3f far = nearfar[1];
			Vec3f dir = Vec3f.Normalize( far - near );
			
			// drop camera to dir
			Vec3f coord = ViewUser.GetPosition() - near;
			float dist = dir % coord;
			Vec3f a = dir * dist;
			
			// replace near with camera-projected-to-dir
			near = near + a;
			
			float mindist = float.PositiveInfinity;
			
			Vec3f? cn = Highlight.GetColor( BrushColor.Val );
			Vec4f hc;
			if( cn != null ) { Vec3f c = (Vec3f) cn; hc = new Vec4f( c.x, c.y, c.z, 1.0f ); }
			else hc = new Vec4f( 0.0f, 0.0f, 0.0f, 0.0f );
			
			/*for( int i = 0; i < viewable.nVerts; i++ )
			{
				coord = viewable.Verts[i] - near;
				dist = dir % coord;
				a = dir * dist;
				float dsq = (a - coord).LengthSqr;
				if( dsq > cszsq ) continue;
				if( dist < mindist && dist > 0 ) mindist = dist;
			}
			
			Vec3f highlightcenter = dir * mindist + near;
			
			//DebugWindow.SectionWriteVars( "mindist", mindist, "highlightcenter", highlightcenter.ToStringFormatted() );
			
			for( int i = 0; i < viewable.nVerts; i++ )
			{
				Vec3f diff = viewable.Verts[i] - highlightcenter;
				if( diff.LengthSqr <= szsq )
					viewable.Colors[0][i] = hc;
			}*/
			
			RefreshControl();
		}
		
		#endregion
	}
}