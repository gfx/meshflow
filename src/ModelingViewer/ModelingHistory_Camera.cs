using System;
using System.Collections.Generic;

namespace ModelingViewer
{
	public partial class ModelingHistory
	{
		public CameraProperties[] GetCameras( Cluster cluster, float sigmat, float sigmax, float epsilon, bool fromt0, float wtar, float wrot, float wdis )
		{
			List<Cluster> clusters = CurrentLevel.Val.GetClusters();
			int ind = -1;
			
			CameraProperties[][] camsartistbest = CurrentLevel.Val.GetCameras();
			
			if( camsartistbest.Length == 0 ) throw new Exception( "failed sanity check" );
			
			List<CameraProperties> lstartists = new List<CameraProperties>();
			List<CameraProperties> lstbestviews = new List<CameraProperties>();
			for( int i = 0; i < camsartistbest.Length; i++ )
			{
				if( Filters.IsFiltered( clusters[i] ) ) continue;
				if( cluster == clusters[i] ) ind = lstartists.Count;
				lstartists.Add( camsartistbest[i][0] );
				lstbestviews.Add( camsartistbest[i][1] );
			}
			
			if( ind == -1 ) { System.Console.WriteLine( "ModelingHistory.GetCameras: Could not find cluster" ); ind = 0; }
			
			CameraProperties[] artists = lstartists.ToArray();
			CameraProperties[] bestviews = lstbestviews.ToArray();
			
			CameraProperties artistgauss = CameraProperties.SmoothGaussian( artists, ind, sigmat );
			artistgauss.Name = artists[ind].Name + " + Gaussian Smoothing";
			
			CameraProperties artistbilat = CameraProperties.SmoothBilateral( artists, ind, sigmat, sigmax );
			artistbilat.Name = artists[ind].Name + " + Bilateral Smoothing";
			
			CameraProperties artistbinary = CameraProperties.SmoothBinary( artists, ind, epsilon, fromt0, wtar, wrot, wdis );
			artistbinary.Name = artists[ind].Name + " + Binary Smoothing";
			
			CameraProperties bestviewgauss = CameraProperties.SmoothGaussian( bestviews, ind, sigmat );
			bestviewgauss.Name = bestviews[ind].Name + " + Gaussian Smoothing";
			
			CameraProperties bestviewbilat = CameraProperties.SmoothBilateral( bestviews, ind, sigmat, sigmax );
			bestviewbilat.Name = bestviews[ind].Name + " + Gaussian Smoothing";
			
			CameraProperties bestviewbinary = CameraProperties.SmoothBinary( bestviews, ind, epsilon, fromt0, wtar, wrot, wdis );
			bestviewbinary.Name = bestviews[ind].Name + " + Binary Smoothing";
			
			return new CameraProperties[] {
				artists[ind], bestviews[ind],
				artistgauss, artistbilat, artistbinary,
				bestviewgauss, bestviewbilat, bestviewbinary
			};
		}
	}
}

