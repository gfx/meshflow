using System;
using OpenTK;
//using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

// inspired by TexLib, /Olof Bjarnason (MIT-License)
// http://paste-it.net/public/h4f2876/

namespace ModelingViewer
{
	public enum GLTextPosition { LeftTop, CenterTop, RightTop, LeftCenter, CenterCenter, RightCenter, LeftBottom, CenterBottom, RightBottom }
	
	public class GLText : IDisposable
	{
		private int fonttexid;
		private int charwidth;
		private int charheight;
		
		private int vw, vh;
		
		public GLText( string fontname )
		{
			Bitmap bmp = MiscFileIO.LoadFontResource( fontname );
			fonttexid = CreateTextureFromBitmap( bmp );
			
			charwidth = bmp.Width / 95;
			charheight = bmp.Height;
		}
		
		public void Dispose()
		{
			GL.DeleteTexture( fonttexid );
		}
		
		public double GetTop() { return (double)vh / 2.0; }
		public double GetBottom() { return -(double)vh / 2.0; }
		public double GetLeft() { return -(double)vw / 2.0; }
		public double GetRight() { return (double)vw / 2.0; }
		
		public void WriteString( double x, ref double y, string text, GLTextPosition pos )
		{
			WriteString( x, y, text, pos );
			y -= GetHeight( text );
		}
		
		public void WriteString( double x, double y, string text, GLTextPosition pos )
		{
			double width = GetWidth( text );
			double height = GetHeight( text );
			
			if( pos == GLTextPosition.LeftTop || pos == GLTextPosition.LeftCenter || pos == GLTextPosition.LeftBottom ) x -= 0;
			if( pos == GLTextPosition.CenterTop || pos == GLTextPosition.CenterCenter || pos == GLTextPosition.CenterBottom ) x -= width / 2.0;
			if( pos == GLTextPosition.RightTop || pos == GLTextPosition.RightCenter || pos == GLTextPosition.RightBottom ) x -= width;
			if( pos == GLTextPosition.LeftTop || pos == GLTextPosition.CenterTop || pos == GLTextPosition.RightTop ) y -= height;
			if( pos == GLTextPosition.LeftCenter || pos == GLTextPosition.CenterCenter || pos == GLTextPosition.RightCenter ) y -= height / 2.0;
			if( pos == GLTextPosition.LeftBottom || pos == GLTextPosition.CenterBottom || pos == GLTextPosition.RightBottom ) y -= 0;
			
			//x -= vw / 2.0;
			//y -= vh / 2.0;
			
			int minf;
			
			GL.GetTexParameter( TextureTarget.Texture2D, GetTextureParameter.TextureMinFilter, out minf );
			GL.TexParameter( TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest );
			GL.Color4( 1.0f, 1.0f, 1.0f, 1.0f );
			GL.BindTexture( TextureTarget.Texture2D, fonttexid );
			
			GL.MatrixMode( MatrixMode.Projection );
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Ortho( -1.0, 1.0, -1.0, 1.0, -1.0, 1.0 );
			
			GL.MatrixMode( MatrixMode.Modelview );
			GL.PushMatrix();
			GL.LoadIdentity();
			GL.Scale( 2.0 / (double) vw, 2.0 / (double) vh, 1.0 );
			GL.Translate( x, y, 0 );
			
			double sx = 0;
			double sy = 0;
			double sw = (double) charwidth;
			double sh = (double) charheight;
			double fx = 0.0;
			double fy = 0.0;
			double fw = 1.0 / 95.0;
			double fh = 1.0;
			
			GL.Begin( BeginMode.Quads );
			foreach( char c in text )
			{
				int i = (int)c - 32;
				fx = (double)i * fw;
				fy = 0.0;
				
				GL.TexCoord2( fx, fy + fh );
				GL.Vertex2( sx, sy );
				GL.TexCoord2( fx + fw, fy + fh );
				GL.Vertex2( sx + sw, sy );
				GL.TexCoord2( fx + fw, fy );
				GL.Vertex2( sx + sw, sy + sh );
				GL.TexCoord2( fx, fy );
				GL.Vertex2( sx, sy + sh );
				
				sx += sw;
			}
			GL.End();
			
			GL.PopMatrix();
			
			GL.MatrixMode( MatrixMode.Projection );
			GL.PopMatrix();
			
			GL.MatrixMode( MatrixMode.Modelview );
			
			GL.TexParameter( TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, minf );
		}
		
		public double GetWidth( string text )
		{
			return text.Length * charwidth;
		}
		
		public double GetHeight( string text )
		{
			return charheight;
		}
		
		public void InitTexturing( int vw, int vh )
		{
			//GL.Disable( EnableCap.CullFace );
			GL.Enable( EnableCap.Texture2D );
			GL.Enable( EnableCap.Blend );
			GL.BlendFunc( BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha );
			GL.PixelStore( PixelStoreParameter.UnpackAlignment, 1 );
			GL.TexParameter( TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest );
			GL.Disable( EnableCap.Lighting );
			
			this.vw = vw;
			this.vh = vh;
		}
		
		public int CreateTextureFromBitmap( Bitmap bmp )
		{
			BitmapData data = bmp.LockBits(
				new Rectangle(0, 0, bmp.Width, bmp.Height),
				ImageLockMode.ReadOnly,
				System.Drawing.Imaging.PixelFormat.Format32bppArgb
			);
			
			int tex = GL.GenTexture();
			GL.BindTexture( TextureTarget.Texture2D, tex );
			GL.TexImage2D(
				TextureTarget.Texture2D,
				0,
				PixelInternalFormat.Rgba,
				data.Width, data.Height,
				0,
				OpenTK.Graphics.OpenGL.PixelFormat.Bgra,
				PixelType.UnsignedByte,
				data.Scan0
			);
			
			bmp.UnlockBits(data);
			
			return tex;
		}
	}
}
