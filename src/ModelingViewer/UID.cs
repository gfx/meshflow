using System;
namespace ModelingViewer
{
	public struct UID : IEquatable<UID>
	{
		private string spre;
		private ulong id;
		
		public UID( string spre ) { this.id = 0; this.spre = spre; }
		
		public ulong Val { get { return id; } }
		
		public UID GetNew() { UID nid = new UID(); nid.id = id++; return nid; }
		
		public bool Equals( UID uid ) { return id == uid.id; }
		public override string ToString() { return spre + "UID" + id.ToString(); }
		public TypeCode GetTypeCode() { return id.GetTypeCode(); }
	}
	
	public static class GlobalUID { public static UID GUID = new UID( "G" ); }
}

