using System;
using System.Collections.Generic;
using System.Linq;
using Common.Libs.VMath;
using Common.Libs.MiscFunctions;

namespace ModelingViewer
{
	// a semi-flattened collection of coordinates and corresponding colors
	public class FlattenedViewable
	{
		public int nVerts;
		public int GroupSize;
		public Vec3f[] Verts = null;
		public Vec3f[] Colors = null;
		public FlattenedViewable( Vec3f[] lstVerts, Vec3f[] lstColors, int GroupSize) {
			nVerts = lstVerts.Length;
			Verts = lstVerts;
			Colors = lstColors;
			this.GroupSize = GroupSize;
		}
		public FlattenedViewable( int nVerts, int GroupSize )
		{
			this.nVerts = nVerts;
			this.GroupSize = GroupSize;
			Verts = new Vec3f[nVerts];
			Colors = new Vec3f[nVerts];
		}
	}
	
	public class FlattenedViewableAlpha
	{
		public int nVerts;
		public int GroupSize;
		public Vec3f[] Verts = null;
		public Vec4f[] Colors = null;
		public FlattenedViewableAlpha( Vec3f[] lstVerts, Vec4f[] lstColors, int GroupSize) {
			nVerts = lstVerts.Length;
			Verts = lstVerts;
			Colors = lstColors;
			this.GroupSize = GroupSize;
		}
		public FlattenedViewableAlpha( int nVerts, int GroupSize )
		{
			this.nVerts = nVerts;
			this.GroupSize = GroupSize;
			Verts = new Vec3f[nVerts];
			Colors = new Vec4f[nVerts];
		}
	}
	
	/*public class IndexedViewableAlpha
	{
		public int nVerts;
		public int[] VertUIDs = null;
		public Vec3f[] Verts = null;
		public Vec4f[][] Colors = null;
		public bool[] Selected = null;
		
		public int[] GroupSizes;
		public int[][] Indices = null;
		public float[] PointSizes = null;
		public float[] LineWidths = null;
		
		public IndexedViewableAlpha( Vec3f[] verts, Vec4f[][] colors, int[][] indices, float[] pointsizes, float[] linewidths, int[] groupsizes )
			: this( verts, colors, indices, pointsizes, linewidths, groupsizes, null, null ) { }
		
		public IndexedViewableAlpha( Vec3f[] verts, Vec4f[][] colors, int[][] indices, float[] pointsizes, float[] linewidths, int[] groupsizes, int[] vertuids, bool[] selected )
		{
			this.nVerts = verts.Length;
			this.Verts = verts;
			this.Colors = colors;
			this.GroupSizes = groupsizes;
			this.Indices = indices;
			this.PointSizes = pointsizes;
			this.LineWidths = linewidths;
			
			if( vertuids == null ) {
				this.VertUIDs = new int[nVerts];
				for( int i = 0; i < nVerts; i++ ) VertUIDs[i] = i;
			}
			this.VertUIDs = vertuids;
			
			if( selected == null ) selected = new bool[nVerts];
			this.Selected = selected;
		}
		
		public static IndexedViewableAlpha operator-( IndexedViewableAlpha v0, IndexedViewableAlpha v1 )
		{
			var diffuids = v0.VertUIDs.Where( (int uid) => !v1.VertUIDs.Contains( uid ) );
			var diffvertinds = diffuids.Select( (int uid) => v0.VertUIDs.IndexOf( uid ) );
			bool[] diff = new bool[v0.nVerts];
			diffvertinds.Each( delegate(int i) { diff[i] = true; } );
			
			List<List<int>> groups = new List<List<int>>();
			for( int i = 0; i < v0.Indices.Length; i++ )
			{
				List<int> ngroups = new List<int>();
				int sz = v0.GroupSizes[i];
				int[] grps = v0.Indices[i];
				for( int j = 0; j < grps.Length; j += sz )
				{
					bool del = false;
					for( int k = 0; k < sz; k++ ) if( diff[grps[j+k]] ) del = true;
					if( !del ) continue;
					for( int k = 0; k < sz; k++ ) ngroups.Add( grps[j+k] );
				}
				groups.Add( ngroups );
			}
			int[][] agroups = groups.Select( (List<int> grps) => grps.ToArray() ).ToArray();
			return new IndexedViewableAlpha( v0.Verts, v0.Colors, agroups, v0.PointSizes, v0.LineWidths, v0.GroupSizes, v0.VertUIDs, v0.Selected );
		}
		
		public static IndexedViewableAlpha operator%( IndexedViewableAlpha v0, IndexedViewableAlpha v1 )
		{
			var sameuids = v0.VertUIDs.Intersect( v1.VertUIDs );
			var samevertinds = sameuids.Select( (int uid) => v0.VertUIDs.IndexOf( uid ) );
			bool[] same = new bool[v0.nVerts];
			samevertinds.Each( delegate(int i) { same[i] = true; } );
			
			List<List<int>> groups = new List<List<int>>();
			for( int i = 0; i < v0.Indices.Length; i++ )
			{
				List<int> ngroups = new List<int>();
				int sz = v0.GroupSizes[i];
				int[] grps = v0.Indices[i];
				for( int j = 0; j < grps.Length; j += sz )
				{
					bool incl = true;
					for( int k = 0; k < sz; k++ ) if( !same[grps[j+k]] ) incl = false;
					if( !incl ) continue;
					for( int k = 0; k < sz; k++ ) ngroups.Add( grps[j+k] );
				}
				groups.Add( ngroups );
			}
			int[][] agroups = groups.Select( (List<int> grps) => grps.ToArray() ).ToArray();
			return new IndexedViewableAlpha( v0.Verts, v0.Colors, agroups, v0.PointSizes, v0.LineWidths, v0.GroupSizes, v0.VertUIDs, v0.Selected );
		}
		
		public static IndexedViewableAlpha operator+( IndexedViewableAlpha v0, IndexedViewableAlpha v1 )
		{
			// FIXME: vertuids may contain duplicates, but their corresponding verts may be different!!!
			
			Vec4f nocolor = new Vec4f( 0.0f, 0.0f, 0.0f, 0.0f );
			Vec4f[] nocolors0 = Enumerable.Repeat( nocolor, v0.nVerts ).ToArray();
			Vec4f[] nocolors1 = Enumerable.Repeat( nocolor, v1.nVerts ).ToArray();
			
			List<Vec3f> verts = new List<Vec3f>( v0.Verts );
			verts.AddRange( v1.Verts );
			
			List<int> vertuids = new List<int>( v0.VertUIDs );
			vertuids.AddRange( v1.VertUIDs );
			
			List<bool> selected = new List<bool>( v0.Selected );
			selected.AddRange( v1.Selected );
			
			List<Vec4f[]> colors = new List<Vec4f[]>();
			for( int i = 0; i < v0.Colors.Count(); i++ )
				colors.Add( new List<Vec4f>( v0.Colors[i] ).AddReturn( nocolors1 ).ToArray() );
			for( int i = 0; i < v1.Colors.Count(); i++ )
				colors.Add( new List<Vec4f>( nocolors0 ).AddReturn( v1.Colors[i] ).ToArray() );
			
			List<int[]> groups = v0.Indices.ToList();
			groups.AddRange( v1.Indices.Select( (int[] grps) => grps.Select( (int i) => i + v0.nVerts ).ToArray() ).ToArray() );
			
			List<float> pointsizes = new List<float>( v0.PointSizes );
			pointsizes.AddRange( v1.PointSizes );
			
			List<float> linewidths = new List<float>( v0.LineWidths );
			linewidths.AddRange( v1.LineWidths );
			
			List<int> groupsizes = new List<int>( v0.GroupSizes );
			groupsizes.AddRange( v1.GroupSizes );
			
			return new IndexedViewableAlpha( verts.ToArray(), colors.ToArray(), groups.ToArray(), pointsizes.ToArray(), linewidths.ToArray(), groupsizes.ToArray(), vertuids.ToArray(), selected.ToArray() );
		}
		
		public void RecolorGroups( Func<int,int[],Vec4f[]> colorfunc )
		{
			for( int ig = 0; ig < Indices.Length; ig++ )
			{
				int sz = GroupSizes[ig];
				int[] grps = Indices[ig];
				int[] grp = new int[sz];
				for( int i = 0, ind = 0; i < grps.Length; i += sz, ind++ )
				{
					for( int j = 0; j < sz; j++ ) grp[j] = grps[i+j];
					Vec4f[] c = colorfunc( ind, grp );
					if( c != null ) for( int j = 0; j < sz; j++ ) Colors[ig][i+j] = c[j];
				}
			}
		}
		
		public void RecolorGroups( Func<int,int[],Vec4f?> colorfunc )
		{
			for( int ig = 0; ig < Indices.Length; ig++ )
			{
				int sz = GroupSizes[ig];
				int[] grps = Indices[ig];
				int[] grp = new int[sz];
				for( int i = 0, ind = 0; i < grps.Length; i += sz, ind++ )
				{
					for( int j = 0; j < sz; j++ ) grp[j] = grps[i+j];
					Vec4f? c = colorfunc( ind, grp );
					if( c != null ) for( int j = 0; j < sz; j++ ) Colors[ig][grps[i+j]] = (Vec4f)c;
				}
			}
		}
	}*/
	
}

