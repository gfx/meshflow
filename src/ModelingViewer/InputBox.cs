using System;
using System.Drawing;
using System.Windows.Forms;

namespace ModelingViewer
{
	public static class InputBox
	{
		// http://www.csharp-examples.net/inputbox/
		public static DialogResult Show( string title, string prompt, ref string text )
		{
			Form frm = new Form();
			Label lbl = new Label();
			TextBox txt = new TextBox();
			Button btnOK = new Button();
			Button btnCancel = new Button();
			
			frm.Text = title;
			frm.Icon = MiscFileIO.LoadIconResource( "model.ico" );
			lbl.Text = prompt;
			txt.Text = text;
			
			btnOK.Text = "OK";
			btnOK.DialogResult = DialogResult.OK;
			btnCancel.Text = "Cancel";
			btnCancel.DialogResult = DialogResult.Cancel;
			
			lbl.SetBounds( 9, 20, 372, 13 );
			txt.SetBounds( 12, 36, 372, 20 );
			btnOK.SetBounds( 228, 72, 75, 23 );
			btnCancel.SetBounds( 309, 72, 75, 23 );
			
			lbl.AutoSize = true;
			txt.Anchor = txt.Anchor | AnchorStyles.Right;
			btnOK.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			btnCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
			
			frm.ClientSize = new Size( 396, 107 );
			frm.Controls.AddRange( new Control[] { lbl, txt, btnOK, btnCancel } );
			frm.ClientSize = new Size( Math.Max( 300, lbl.Right + 10 ), frm.ClientSize.Height );
			frm.FormBorderStyle = FormBorderStyle.FixedToolWindow; //.FixedDialog;
			frm.StartPosition = FormStartPosition.CenterParent;
			frm.MinimizeBox = false;
			frm.MaximizeBox = false;
			frm.ControlBox = false;
			frm.AcceptButton = btnOK;
			frm.CancelButton = btnCancel;
			
			DialogResult result = frm.ShowDialog();
			text = txt.Text;
			return result;
		}
		
		public static DialogResult Show( string title, string prompt, ref float val )
		{
			string v = "" + val;
			DialogResult r = Show( title, prompt, ref v );
			if( r == DialogResult.Cancel ) return r;
			val = float.Parse( v );
			return r;
		}
		public static DialogResult Show( string title, string prompt, ref int val )
		{
			string v = "" + val;
			DialogResult r = Show( title, prompt, ref v );
			if( r == DialogResult.Cancel ) return r;
			val = int.Parse( v );
			return r;
		}
		
		public static bool GetPropertyFloat( string title, Property<float> prop, bool canbeneg, bool canbezero, bool canbepos )
		{
			float nst = prop.Val;
			string errormsg = "Must specify a ";
			
			if( canbeneg && canbezero && canbepos ) errormsg += "";
			if( !canbeneg && canbezero && canbepos ) errormsg += "non-negative";
			if( !canbeneg && !canbezero && canbepos ) errormsg += "positive";
			if( canbeneg && !canbezero && !canbepos ) errormsg += "negative";
			if( canbeneg && canbezero && !canbepos ) errormsg += "non-positive";
			if( canbeneg && !canbezero && canbepos ) errormsg += "non-zero";
			if( !canbeneg && canbezero && !canbepos ) throw new ArgumentException( "sanity check fail" );
			if( !canbeneg && !canbezero && !canbepos ) throw new ArgumentException( "sanity check fail" );
			
			errormsg = " float";
			if( !InputBox.GetFloat( title, prop.Name, errormsg, ref nst, canbeneg, canbezero, canbepos ) ) return false;
			prop.Set( nst );
			return true;
		}
		
		public static bool GetFloat( string title, string prompt, string errormsg, ref float floatval )
		{
			return GetFloat( title, prompt, errormsg, ref floatval, true, true, true );
		}
		
		public static bool GetFloat( string title, string prompt, string errormsg, ref float floatval, bool canbeneg, bool canbezero, bool canbepos )
		{
			float nfv;
			string s = "" + floatval;
			if( InputBox.Show( title, prompt, ref s ) == DialogResult.Cancel ) return false;
			if( !Single.TryParse( s, out nfv ) ) { MessageBox.Show( errormsg ); return false; }
			if( ( !canbeneg && nfv < 0.0f ) || ( !canbezero && nfv == 0.0f ) || ( !canbepos && nfv > 0.0f ) ) { MessageBox.Show( errormsg ); return false; }
			floatval = nfv;
			return true;
		}
	}
}

