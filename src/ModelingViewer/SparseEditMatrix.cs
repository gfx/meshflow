using System;
using System.Collections.Generic;
using System.Linq;
using Common.Libs.MiscFunctions;
using Common.Libs.VMath;

namespace ModelingViewer
{
	public class SparseEditMatrix<T> where T:IEquatable<T>
	{
		protected int width, height;
		
		protected List<int>[] assignedindices;
		protected List<T>[] sparsematrix;
		
		public T NullValue { get; set; }
		
		public SparseEditMatrix( int height, int width )
		{
			this.width = width;
			this.height = height;
			assignedindices = new List<int>[height];
			sparsematrix = new List<T>[height];
		}
		public SparseEditMatrix( int height, int width, T nullvalue ) : this( height, width )
		{
			this.NullValue = nullvalue;
		}
		
		public T this[ int j, int i ]
		{
			get
			{
				if( i < 0 || j < 0 || i >= width || j >= height ) throw new ArgumentException( "Out of range" );
				int prevind = GetPrevEditIndex( j, i );
				if( prevind == -1 ) return NullValue;
				return sparsematrix[j][prevind];
			}
			set
			{
				// todo: handle if value = null!!!!!
				int prevind = GetPrevEditIndex( j, i );
				
				if( assignedindices[j][prevind] != i ) {								// new entry
					assignedindices[j].Add( i );
					sparsematrix[j].Add( value );
				} else {																// overwriting
					sparsematrix[j][prevind] = value;
				}
			}
		}
		
		public void UnsetAt( int j, int i )
		{
			int prevind = GetPrevEditIndex(j,i);
			if( assignedindices[j][prevind] != i ) return;								// not set
			assignedindices[j].RemoveAt(prevind);
			sparsematrix[j].RemoveAt(prevind);
		}
		
		public bool IsSet( int j, int i ) { return (GetPrevEditIndex(j,i) == i); }
		
		public T[] GetFilteredColumn( int[] ajs, int i ) { return ajs.Select( (int j) => this[j,i] ).ToArray(); }
		public T[] GetFilteredRow( int j, int[] ais ) { return ais.Select( (int i) => this[j,i] ).ToArray(); }
		
		protected int GetPrevEditIndex( int j, int i )
		{
			List<int> indices = assignedindices[j];
			if( indices == null ) return -1;
			
			int prevind = -1;
			int largestediti = -1;
			indices.Each( delegate( int editi, int ind ) {
				if( editi <= i && editi > largestediti ) { prevind = ind; largestediti = editi; }
			} );
			return prevind;
		}
		
		protected int GetNextEditIndex( int j, int i )
		{
			List<int> indices = assignedindices[j];
			if( indices == null ) return -1;
			
			int nextind = -1;
			int smallestediti = -1;
			indices.Each( delegate( int editi, int ind ) {
				if( editi >= i && editi < smallestediti ) { nextind = ind; smallestediti = editi; }
			} );
			return nextind;
		}
	}
}

